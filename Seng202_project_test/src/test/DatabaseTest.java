package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

import exceptions.CommentNotFoundException;
import exceptions.EventNotFoundException;
import exceptions.InvalidUsernameException;
import exceptions.UserNotFoundException;

import requests.CreateCommentReq;
import requests.CreateEventReq;
import requests.CreateMeetingReq;
import requests.CreateUserReq;
import requests.LoginReq;
import requests.UpdateEventReq;
import requests.UpdateInitiatorReq;
import requests.UpdateUserReq;
import server.Database;
import shared.Comment;
import shared.Event;
import shared.Meeting;
import shared.Schedule;
import shared.User;

 
/**
 * JUnit test class for testing methods in the database
 * @author Daniel Hope
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseTest {
	
	public static String databaseBackupPath = "databaseBackup/test.db";
	public static String databasePath = "databaseObjects/test.db";
	
	
	/**
	 * Copies a backup of the database file over the current file and prepares the 
	 * database for testing.
	 */
	@BeforeClass
	public static void initDatabase() {
		try {
			Files.copy(Paths.get(databaseBackupPath),
					Paths.get(databasePath),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			System.err.println(">> Failed to copy backup database");
			e1.printStackTrace();
		}

		try {
			Database.initiateDatabase(databasePath);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Closes the database after testing is finished
	 */
	@AfterClass
	public static void closeDatabase() {
		Database.closeDatabase();
	}
	
	
	/**
	 * Checks that the database initialised correctly.
	 */
	@Test
	public void test01Initalise() {
		assertEquals(7, Database.getNextEventID());
		assertEquals(5, Database.getNextCommentID());
}
	
	
/**
	 * Checks that the get all users function returns the username, firstname
 * and lastname of all users correctly
	 */
	@Test
	public void test02GetAllUsers() {
		ArrayList<String[]> expectedUsers = new ArrayList<String[]>();
		expectedUsers.add(new String[] { "yousirname", "Daniel", "Hope" });
		expectedUsers.add(new String[] { "JD", "Jay", "Dee" });
		expectedUsers.add(new String[] { "Tester1", "Montgomery", "Anderson" });
		expectedUsers.add(new String[] { "Tester2", "Yolo", "Swaggins" });
		expectedUsers.add(new String[] { "Tester3", "Raspberry", "Coke" });
		try {
			ArrayList<String[]> testUsers = Database.getUserList();
			if (expectedUsers.size() != testUsers.size()) {
				fail("ArrayLists not equal size");
			}
			for (int i = 0; i < 5; i++) {
				if (!Arrays.equals(expectedUsers.get(i), testUsers.get(i))) {
					fail("String[] at " + Integer.toString(i) + " is not equal");
				}
			}
			assert (true);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage()
					+ " @getUserList");
			fail("Failed to get users from database");
		}
	}
	
	
	/**
	 * Checks the comment sorting
	 */
	@Test
	public void test03SortComments() {
		ArrayList<Comment> expectedList = new ArrayList<Comment>();
		Calendar timeStamp = Calendar.getInstance();
		
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379761200000L);
		expectedList.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379757600000L);
		expectedList.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379754000000L);
		expectedList.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379750400000L);
		expectedList.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379746800000L);
		expectedList.add(new Comment(0, 0, "owner", timeStamp, "comment"));

		ArrayList<Comment> toBeSorted = new ArrayList<Comment>();
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379757600000L);
		toBeSorted.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379746800000L);
		toBeSorted.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379761200000L);
		toBeSorted.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379754000000L);
		toBeSorted.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379750400000L);
		toBeSorted.add(new Comment(0, 0, "owner", timeStamp, "comment"));
		
		assertEquals(expectedList, Database.sortComments(toBeSorted));

	}

	
	/**
	 * Checks that a comment is retrieved properly from the database
	 */
	@Test
	public void test04GetComment() {
		Calendar timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1380744000000L);
		Comment expectedComment = new Comment(1, 4, "JD", timeStamp,
				"pfffft, as if im going to go to that");
		try {
			Comment testComment = Database.getComment(1);
			assertEquals(expectedComment.toString(), testComment.toString());
		} catch (CommentNotFoundException e) {
			fail();
		}
	}

	
	/**
	 * Adds a comment to the database and the checks if it is retrieved when the
	 * meeting it is attached to is retrieved from the database.
	 */
	@Test
	public void test05AddComment() {
		Calendar timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379739600000L);
		Comment insertComment = new Comment(-1, 4, "yousirname", timeStamp,
				"testComment");
		Database.addComment(new CreateCommentReq(insertComment));
		Comment expectedComment = new Comment(5, 4, "yousirname", timeStamp,
				"testComment");
		try {
			Meeting testMeeting = (Meeting) Database.getEvent(4);
			int numComments = testMeeting.getComments().size();
			Boolean found = false;
			int i = 0;
			Comment comment;
			while (!found && i < numComments) {
				comment = testMeeting.getComments().get(i);
				if (comment.getID() == 5) {
					found = true;
					assertEquals(expectedComment, comment);
				}
				i += 1;
			}

			if (!found) {
				fail("Comment could not be found in the associated meeting");
			}
		} catch (EventNotFoundException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage()
					+ " @getEvent(Meeting)");
		}
	}


	/**
	 * Tests if the database can delete the comment created in test05
	 */
	@Test
	public void test06DeleteComment() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(4);
			if (expectedMeeting.getComments().get(0).getID() == 2) {
				expectedMeeting.getComments().remove(0);
			} else {
				expectedMeeting.getComments().remove(1);
			}
			
			Database.deleteComment(2);
			Meeting testMeeting = (Meeting) Database.getEvent(4);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			fail("Could not fine meeting to use for test");
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @deleteComment");
			fail();
		}
	}
	
	
	/**
	 * Tests getEvent() for a event request
	 */
	@Test
	public void test07GetEvent() {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1380859200000L);
		Event expectedEvent = new Event(1, false, "Dinner", "Captain Bens",
				"Get a feed", 30, startTime, "yousirname");
		try {
			Event testEvent = Database.getEvent(1);
			assertEquals(expectedEvent, testEvent);
		} catch (EventNotFoundException e) {
			fail();
		}
	}
	
	
	/**
	 * Tests adding a new private event to the database and that the database
	 * increments the nextEventID
	 */
	@Test
	public void test08AddEvent() {
		int eventNumber = Database.getNextEventID();
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1379797200000L);
		Event expectedEvent = new Event(eventNumber, false, "TestEvent",
				"location", "Description", 30, startTime, "yousirname");
		try {
			Database.addEvent(new CreateEventReq(expectedEvent));
			Event testEvent = Database.getEvent(eventNumber);
			assertEquals(expectedEvent, testEvent);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage()
					+ " @addEvent");
			fail("Could not add a new private event to the database");
		} catch (EventNotFoundException e) {
			fail("Could not find event after being added to the database");
		}
	}
	
	
	/**
	 * Tests that an events is succesfully modified in the database.
	 */
	@Test
	public void test09ModifyEvent() {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1379797200000L);
		Event expectedEvent = new Event(5, false, "newName", "newLocation",
				"newDescription", 30, startTime, "yousirname");
		try {
			Database.modifyEvent(new UpdateEventReq(expectedEvent));
			Event testEvent = Database.getEvent(5);
			assertEquals(expectedEvent, testEvent);
		} catch (EventNotFoundException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}

	}
	
	
	/**
	 * Tests delete event by retrieving an event from the database then deleting it
	 * then trying to retrieve it again.
	 */
	@Test
	public void test10DeleteEvent() {
		try {
			Database.getEvent(7);     //checks that the event exists
		} catch (EventNotFoundException e) {
			fail("Event was not found before deletion");
		}	
		try {
			Database.deleteEvent(7);
		} catch (SQLException e) {    //delete it
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @DeleteEvent");
			fail();
		}
		try {
			Database.getEvent(7);
			fail("Event was found in the database after it was deleted");
		} catch (EventNotFoundException e) {
			assert(true);
		}
	}
	
	
	/**
	 * Tests getEvent() for a meeting request
	 */
	@Test
	public void test11GetMeeting() {
		ArrayList<String> attendees = new ArrayList<String>();
		attendees.add("JD");
		attendees.add("Tester1");
		attendees.add("Tester2");
		attendees.add("Tester3");
		ArrayList<Boolean> accepted = new ArrayList<Boolean>();
		accepted.add(false);
		accepted.add(false);
		accepted.add(false);
		accepted.add(false);
		ArrayList<Comment> comments = new ArrayList<Comment>();
		Calendar timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1380744000000L);
		comments.add(new Comment(1, 4, "JD", timeStamp,
				"pfffft, as if im going to go to that"));

		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1380762000000L);
		Meeting expectedMeeting = new Meeting(4, true, "Team meeting", "Lab 3",
				"A meeting to discuss stuff", 180, startTime, "yousirname",
				attendees, accepted, comments);

		Meeting testMeeting;
		try {
			testMeeting = (Meeting) Database.getEvent(4);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			fail("Meeting not found");
		}
	}
	
	
	/**
	 * Tests if the database can correctly process a create meeting request
	 */
	@Test
	public void test12CreateMeeting() {
		int eventNumber = Database.getNextEventID();
		ArrayList<String> attendees = new ArrayList<String>();
		ArrayList<Boolean> accepted = new ArrayList<Boolean>();
		attendees.add("JD");
		accepted.add(false);
		ArrayList<Comment> comments = new ArrayList<Comment>();
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1379743200000L);
		Meeting inputMeeting = new Meeting(-1, true, "title", "location",
				"description", 60, startTime, "yousirname", attendees,
				accepted, comments);
		Meeting expectedMeeting = new Meeting(eventNumber, true, "title", "location",
				"description", 60, startTime, "yousirname", attendees,
				accepted, comments);
		Database.createNewMeeting(new CreateMeetingReq(inputMeeting));
		try {
			Meeting testMeeting = (Meeting) Database.getEvent(8);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			fail("Could not find meeting after creating it");
		}	
	}
	
	
	/**
	 * Tests the modify meeting method in the databse.
	 */
	@Test
	public void test13ModifyMeeting() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(8);
			expectedMeeting.setDescription("edited");
			expectedMeeting.setName("newName");
			expectedMeeting.setLocation("newLocation");
			Database.modifyMeeting(expectedMeeting);
			Meeting testMeeting = (Meeting) Database.getEvent(8);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @getEvent(Meeting)");
			fail();
		}
	}

	
	/**
	 * Tests that the database can succesfully update the acceptance_table
	 */
	@Test
	public void test14AcceptMeeting() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(8);
			Boolean found = false;
			int length = expectedMeeting.getAttendees().size();
			int i = 0;
			while (i < length && !found) {
				if (expectedMeeting.getAttendees().get(i).equals("JD")) {
					expectedMeeting.getAccepted().set(i, true);
					found = true;
				}
				i += 1;
			}

			if (!found) {
				fail("Could not find attendee in the meeting attendees");
			}

			Database.acceptMeeting(6, "JD");
			Meeting testMeeting = (Meeting) Database.getEvent(8);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			fail("Could not find event");
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage()
					+ " @getEvent(meeting)");
			fail();
		}
	}
	
	
	/**
	 * Tests if the database correctly removes a user from a meeting.
	 */
	@Test
	public void test15RejectMeeting() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(8);
			Boolean found = false;
			int length = expectedMeeting.getAttendees().size();
			int i = 0;
			while (i < length && !found) {
				if (expectedMeeting.getAttendees().get(i).equals("JD")) {
					expectedMeeting.getAttendees().remove(i);
					expectedMeeting.getAccepted().remove(i);
					found = true;
				}
				i += 1;
			}

			if (!found) {
				fail("Could not find attendee in the meeting attendees");
			}

			Database.rejectMeeting(6, "JD");
			Meeting testMeeting = (Meeting) Database.getEvent(8);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			fail("Could not find event");
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage()
					+ " @getEvent(meeting)");
			fail();
		}
	}
	
	
	/**
	 * Tests if the database can add a user to a meeting.
	 */
	@Test
	public void test16AddUserMeeting() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(8);
			expectedMeeting.getAttendees().add("Tester1");
			expectedMeeting.getAccepted().add(false);
			ArrayList<String> users = new ArrayList<String>();
			users.add("Tester1");
			Database.addUsersToEvents(8, users);
			Meeting testMeeting = (Meeting) Database.getEvent(8);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			fail("Event not found");
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @addUsersToEvents");
			fail();
		}
	}
	
	
	/**
	 * Tests that the database successfully swap the initiator on a meeting
	 */
	@Test
	public void test17ChangeInitiator() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(8);
			expectedMeeting.changeInitiator("Tester1");
			UpdateInitiatorReq request = new UpdateInitiatorReq(8, "yousirname", "Tester1");
			Meeting testMeeting = (Meeting) Database.updateInitiator(request);
			assertEquals(testMeeting, expectedMeeting);
		} catch (EventNotFoundException e) {
			fail("Event 8 not found in the database");
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			fail();
		}

	}
	
	
	/**
	 * Tests delete meeting by retrieving an meeting from the database then deleting it
	 * then trying to retrieve it again.
	 */
	@Test
	public void test18DeleteMeeting() {
		try {
			Database.getEvent(8);     //checks that the event exists
		} catch (EventNotFoundException e) {
			fail("Event was not found before deletion");
		}	
		try {
			Database.deleteMeeting(8);
		} catch (SQLException e) {    //delete it
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @DeleteEvent");
			fail();
		}
		try {
			Database.getEvent(8);
			fail("Event was found in the database after it was deleted");
		} catch (EventNotFoundException e) {
			assert(true);
		}
	}
	
	
	/**
	 * Tests that the database returns a correct schedule
	 */
	@Test
	public void test19BuildSchedule() {
		ArrayList<Event> events = new ArrayList<Event>();
		ArrayList<Boolean> accepted = new ArrayList<Boolean>();
		try {
			events.add(Database.getEvent(1));
			events.add(Database.getEvent(2));
			events.add(Database.getEvent(3));
			events.add(Database.getEvent(4));
			events.add(Database.getEvent(5));
			events.add(Database.getEvent(6));
			accepted.add(true);
			accepted.add(true);
			accepted.add(true);
			accepted.add(true);
			accepted.add(true);
			accepted.add(true);
			Schedule expectedSchedule = new Schedule(events, accepted);
			Schedule testSchedule = Database.buildSchedule("yousirname");
			assertEquals(expectedSchedule, testSchedule);
		} catch (EventNotFoundException e) {
			fail("Could not get events from database");
		}
	}
	
	
	/**
	 * Tests if the database correctly returns a user.
	 */
	@Test
	public void test20GetUser() {
		Schedule schedule = Database.buildSchedule("yousirname");
		User expectedUser = new User("yousirname", "Daniel", "Hope",
				"Most awesomest person evarrr",
				"Head Cheif Exective Technicion Officer", "Lab 2",
				"email@domian.com", "027 777 7777", schedule);
		try {
			User testUser = Database.getUser("yousirname");
			assertEquals(expectedUser, testUser);
		} catch (UserNotFoundException e) {
			fail("User yousirname not found");
		}
	}
	
	
	/**
	 * Checks that the server correctly identifies correct logon details
	 */
	@Test
	public void test21Login() {
		User expectedUser;
		User testUser;
		try { //Tries a passing case
			char[] password = { 'p', 'a', 'r', 's', 'e', 'w', 'o', 'r', 'd' };
			expectedUser = Database.getUser("yousirname");
			testUser = Database
					.login(new LoginReq("yousirname", password));
			assertEquals(expectedUser, testUser);
		} catch (UserNotFoundException e) {
			fail();
		}
		
		try {//Tries a failing case
			char[] password = {'p'};
			expectedUser = Database.getUser("yousirname");
			testUser = Database.login(new LoginReq("yousirname", password));
			fail("Login should have failed");
		} catch (UserNotFoundException e) {
			//pass
		}
	}
	
	
	/**
	 * Checks that the database can insert a new user into the user table
	 */
	@Test
	public void test22CreateUser() {
		Schedule expectedSchedule = new Schedule(new ArrayList<Event>(), new ArrayList<Boolean>());
		User expectedUser = new User("Test", "not set", "not set",
				"bio not set", "positon not set", "room not set",
				"email not set", "phone not set", expectedSchedule);
		char[] password = new char[] {'a','b','c','d','e'};
		CreateUserReq newUser = new CreateUserReq(expectedUser, password);
		
		try {
			Database.addUser(newUser);
			User testUser = Database.getUser("Test");
			assertEquals(expectedUser, testUser);
		} catch (UserNotFoundException | InvalidUsernameException e) {
			fail("User not found in database");
		}

	}
	
	
	/**
	 * checks that a user is able to update their personal info
	 */
	@Test
	public void test23UpdateUser() {
		Schedule expectedSchedule = new Schedule(new ArrayList<Event>(),
				new ArrayList<Boolean>());
		User expectedUser = new User("Test", "first name", "last name",
				"new bio", "new position", "room 222", "email@eamil.email",
				"123456789", expectedSchedule);
		User testUser;
		try {
			char[] newPassword = new char[] {};
			testUser = Database.updateUser(new UpdateUserReq(expectedUser, newPassword));
			assertEquals(expectedUser, testUser);
		} catch (SQLException | UserNotFoundException e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	
	/**
	 * Checks that the getAttendees method in the database returns the right users;
	 */
	@Test
	public void test24GetAttendees() {
		ArrayList<String> expectedUsers = new ArrayList<String>();
		
		expectedUsers.add("JD");
		expectedUsers.add("Tester1");
		expectedUsers.add("Tester2");
		expectedUsers.add("Tester3");
		expectedUsers.add("yousirname");
		ArrayList<String> testUsers;
		try {
			testUsers = Database.getAttendees(4);
			assertEquals(expectedUsers, testUsers);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @getAttendees");
			fail();
		}
		
	}
	
	
	/**
	 * Checks the final nextId's of the database to make sure that it is 
	 * incrementing the count as new objecs are added.
	 */
	@Test
	public void test25FinalState() {
		assertEquals(9, Database.getNextEventID());
		assertEquals(6, Database.getNextCommentID());
	}
}
