package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import shared.Event;
import shared.Schedule;
import shared.User;
  
public class UserTest {
	public String username;
	public String firstName;
	public String lastName;
	public String bio;
	public String position;
	public String room;
	public Schedule schedule;
	public User user;
    
    @Before
    public void setup() {
    	username = "testuser";
    	firstName = "Test";
    	lastName = "User";
    	bio = "No info";
    	position = "None";
    	room = "Nowhere";
    	String email = "nothing@nothing.com";
    	String phone = "0270000000";
    	
    	ArrayList<Event> events = new ArrayList<Event>();
    	ArrayList<Boolean> accepts = new ArrayList<Boolean>();
    	schedule = new Schedule(events, accepts);
    	user = new User(username, firstName, lastName, bio, position, room, email, phone, schedule);
    }
    
    @Test
    public void testGetBio(){
    	assertEquals(user.getBio(), bio);
    }
    
    @Test
    public void testGetName(){
    	assertEquals(user.getName(), firstName + " " + lastName);
    }
    
    @Test
    public void testGetPosition(){
    	assertEquals(user.getPosition(), position);
    }
    
    @Test
    public void testGetSchedule(){
    	assertEquals(user.getSchedule(), schedule);
    }
    
    @Test
    public void testGetUsername(){
    	assertEquals(user.getUsername(), username);
    }
    
    @Test
    public void testSetBio(){
    	String newBio = "More info";
		user.setBio(newBio);
    	assertEquals(user.getBio(), newBio);
    }
    
    @Test
    public void testSetFirstName(){
    	String newFName = "Firstname";
		user.setFirstName(newFName);
    	assertEquals(user.getName(), newFName + " " + lastName);
    }
    
    @Test
    public void testSetLastName(){
    	String newLName = "Lastname";
		user.setLastName(newLName);
    	assertEquals(user.getName(), firstName + " " + newLName);
    }
    
    @Test
    public void testSetName(){
		String newFirstName = "Different";
		String newLastName = "Name";
		user.setName(newFirstName, newLastName);
    	assertEquals(user.getName(), newFirstName + " " + newLastName);
    }
}
