package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import exceptions.*;

import requests.*;
import server.Database;
import server.DatabaseRequest;
import server.RequestHandler;
import server.ServiceThread;
import shared.*;
 

/**
 * Used to test the stability and functionality of RequestHandler.java
 * @author Daniel Hope
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RequestHandlerTest {
	
	private static String databaseBackupPath = "databaseBackup/test.db";
	private static String databasePath = "databaseObjects/test.db";
	private static RequestHandler RH = new RequestHandler();
	private static Thread RHThread =  new Thread(RH);
	private ServiceThread STyousirname =  new ServiceThread(null, RH);
	private ServiceThread STJD =  new ServiceThread(null, RH);
	private ServiceThread STTester1 =  new ServiceThread(null, RH);
	private int testEventNum = 7;
	private int testMeetingNum = 8;
	
	
/*	
case "CheckAvailabilityReq": doCheckAvailabliltyReq(request); break;
//case "CreateCommentReq": doCreateComment(request); break;
//case "CreateEventReq": doCreateEventReq(request); break;
//case "CreateMeetingReq": doCreateMeetingReq(request); break;
//case "CreateUserReq": doCreateUserReq(request); break;
//case "DeleteEventReq": doDeleteEventReq(request); break;
//case "EditAcceptanceReq": doEditAcceptanceReq(request); break;
//case "GetAllUsersReq": doGetAllUsersReq(request); break;
//case "GetUserReq": doGetUser(request); break;
//case "LoginReq": doLoginReq(request); break;
//case "LogoutReq": doLogoutReq(request); break;
//case "RemoveAttendeeReq": doRemoveAttendeeReq(request); break;
case "ScheduleMeetingReq": doScheduleMeetingReq(request); break;
//case "UpdateEventReq": doUpdateEventReq(request); break;
//case "UpdateInitiatorReq": doUpdateInitiatorReq(request); break;
//case "UpdateMeetingReq": doUpdateMeetingReq(request); break; //TODO delete
//case "UpdateUserReq": doUpdateUserReq(request); break;
*/
	
	
	/**
	 * Initialises the database.
	 */
	@BeforeClass
	public static void initDatabase() {
		//copy over database to get a clean one
		try {
			Files.copy(Paths.get(databaseBackupPath),
					Paths.get(databasePath),
					StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			System.err.println(">> Failed to copy backup database");
			e1.printStackTrace();
		}
		
		//setup database
		try {
			Database.initiateDatabase(databasePath);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//Start request handler thread
		RHThread.start();
	}
	
	
	/**
	 * Closes the database after testing is done
	 */
	@AfterClass
	public static void closeDatabase() {
		Database.closeDatabase();
	}
	
	
	/**
	 * Tests that the user class was recieved from the database on successful login.
	 * Checks that incorrect login details fails to login.
	 * Also checks that only the user that successfully logged in
	 * was added to the connectedUsers list.
	 */
	@Test
	public void test01LoginReq() {
		User expectedUser;
		User testUser;
		//test correct login yousirname
		String username = "yousirname";
		char[] password = {'p','a','r','s','e','w','o','r','d'};
		RH.addRequest(new DatabaseRequest(new LoginReq(username, password), STyousirname));
		 
		try {
			expectedUser = Database.getUser("yousirname");
			testUser = (User) STyousirname.getInput();
			assertEquals(testUser, expectedUser);
		} catch (UserNotFoundException e) {
			fail("Could not get user from database");
		}
		
		//test fail login
		username = "JD";
		password = new char[] {'p'};
		RH.addRequest(new DatabaseRequest(new LoginReq(username, password), STyousirname));
		
		try {
			Object testResult = STyousirname.getInput();
			assertEquals(testResult.getClass(), ServerError.class);
			ServerError result = (ServerError) testResult;
			assertEquals(result.exception.getClass(), LoginErrorException.class);
		} catch (ClassCastException e) {
			fail("Did not recieve LoginErrorException when using invalid credentials");
		}
		
		
		//login JD and Tester1
		username = "JD";
		password = new char[] {'a', 'f', 'k'};
		RH.addRequest(new DatabaseRequest(new LoginReq(username, password), STJD));
		
		username = "Tester1";
		password = new char[] {'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};
		RH.addRequest(new DatabaseRequest(new LoginReq(username, password), STTester1));
		
		//Check for successfull logins
		try {
			expectedUser = Database.getUser("JD");
			testUser = (User) STJD.getInput();
			assertEquals(testUser, expectedUser);
		} catch (UserNotFoundException e) {
			fail("Could not get user from database");
		}
		
		try {
			expectedUser = Database.getUser("Tester1");
			testUser = (User) STTester1.getInput();
			assertEquals(testUser, expectedUser);
		} catch (UserNotFoundException e) {
			fail("Could not get user from database");
		}
		
		//check connectedUsers
		ArrayList<String> expectedUsers = new ArrayList<String>();
		expectedUsers.add("yousirname");
		expectedUsers.add("JD");
		expectedUsers.add("Tester1");
		assertEquals(RH.getConnectedUsers(), expectedUsers);
		
	}
	
	
	@Test
	public void test02GetAllUsersReq() {
		ArrayList<String[]> expectedUsers = new ArrayList<String[]>();
		expectedUsers.add(new String[] { "yousirname", "Daniel", "Hope" });
		expectedUsers.add(new String[] { "JD", "Jay", "Dee" });
		expectedUsers.add(new String[] { "Tester1", "Montgomery", "Anderson" });
		expectedUsers.add(new String[] { "Tester2", "Yolo", "Swaggins" });
		expectedUsers.add(new String[] { "Tester3", "Raspberry", "Coke" });
		RH.addRequest(new DatabaseRequest(new GetAllUsersReq(), STyousirname));
		
		try {
			@SuppressWarnings("unchecked")
			ArrayList<String[]> result = (ArrayList<String[]>) STyousirname.input.take();
			if (expectedUsers.size() != result.size()) {
				fail("ArrayLists not equal size");
			}
			for (int i = 0; i < 5; i++) {
				if (!Arrays.equals(expectedUsers.get(i), result.get(i))) {
					fail("expected " + expectedUsers.get(i) + " :: got " + result.get(i));
				}
			}
		} catch (InterruptedException | ClassCastException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void test03CreateCommentReq() {
		Calendar timeStamp = Calendar.getInstance();
		timeStamp.setTimeInMillis(1379739600000L);
		Comment insertComment = new Comment(5, 4, "yousirname", timeStamp, "testComment");
		CreateCommentReq req = new CreateCommentReq(insertComment);
		DatabaseRequest request = new DatabaseRequest(req, STyousirname);
		RH.addRequest(request);
		try {
			Comment result = (Comment) STyousirname.input.take();
			assertEquals(insertComment, result);
		} catch (InterruptedException | ClassCastException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void test04CreateEventReq() {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1379797200000L);
		Event expectedEvent = new Event(testEventNum, false, "TestEvent",
				"location", "Description", 30, startTime, "yousirname");
		try {
			DatabaseRequest request = new DatabaseRequest(new CreateEventReq(expectedEvent), STyousirname);
			RH.addRequest(request);
			Event testEvent = (Event) STyousirname.input.take();
			assertEquals(expectedEvent, testEvent);
		} catch (InterruptedException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage()
					+ " @addEvent");
			fail("Could not add a new private event to the database");
		}
	}
	
	
	@Test
	public void test05UpdateEventReq() {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1379797200000L);
		Event expectedEvent = new Event(testEventNum, false, "newName", "newLocation",
				"newDescription", 30, startTime, "yousirname");
		
		try {
			DatabaseRequest request = new DatabaseRequest(new UpdateEventReq(expectedEvent), STyousirname);
			RH.addRequest(request);
			Event testEvent = (Event) STyousirname.input.take();
			assertEquals(expectedEvent, testEvent);
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void test06DeleteEventReq() {
		//Check the event exists in the database before deleting it.
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1379797200000L);
		Event expectedEvent = new Event(testEventNum, false, "newName", "newLocation",
				"newDescription", 30, startTime, "yousirname");
		try {
			Event currentEvent = Database.getEvent(testEventNum);
			assertEquals(expectedEvent, currentEvent);
		} catch (EventNotFoundException e) {
			fail("Event not found in database, Could not test deleteEvent");
		}
		
		//Delete the event
		DatabaseRequest request = new DatabaseRequest(new DeleteEventReq(testEventNum, false), STyousirname);
		RH.addRequest(request);
		try {
			Thread.sleep(10); //Wait for the request to be completed
		} catch (InterruptedException e) {}
		
		//Check that the event has been deleted
		try {
			Database.getEvent(testEventNum);
			fail("Event was not deleted");
		} catch (EventNotFoundException e) {
			//Event was found
		}
	}
	
	
	@Test
	public void test07ScheduleMeetingReq() {
		ArrayList<ArrayList<String[]>> expectedResult = new ArrayList<ArrayList<String[]>>();
		ArrayList<String[]> timeslot;
		String[] users;
		//1380757500000 60
		timeslot = new ArrayList<String[]>(); //eveyone free
		
		expectedResult.add(timeslot);
		
		timeslot = new ArrayList<String[]>(); //everyone free
		
		expectedResult.add(timeslot);
		
		timeslot = new ArrayList<String[]>(); //everyone busy from event 4 (team meeting)
		users = new String[] {"yousirname", "Daniel", "Hope"};
		timeslot.add(users);
		users = new String[] {"JD", "Jay", "Dee"};
		timeslot.add(users);
		users = new String[] {"Tester1", "Montgomery", "Anderson"};
		timeslot.add(users);
		expectedResult.add(timeslot);
		//1380757500000L
		//1380766500000L
		ArrayList<String> attendees = new ArrayList<String>();
		attendees.add("yousirname");
		attendees.add("JD");
		attendees.add("Tester1");
		Calendar startTimeFrame = Calendar.getInstance();
		startTimeFrame.setTimeInMillis(1380757500000L);
		Calendar endTimeFrame = Calendar.getInstance();
		endTimeFrame.setTimeInMillis(1380762900000L);
		ScheduleMeetingReq schedulingRequest = new ScheduleMeetingReq(startTimeFrame, endTimeFrame, 60,  attendees);
		DatabaseRequest request = new DatabaseRequest(schedulingRequest, STyousirname);
		RH.addRequest(request);
		try {
			@SuppressWarnings("unchecked")
			ArrayList<ArrayList<String[]>> result = (ArrayList<ArrayList<String[]>>) STyousirname.input.take();
			//check the result
			int outerSize;
			int middleSize;
			int innerSize;
			try {
				outerSize = result.size();
				for (int i = 0; i < outerSize; i++) {
					middleSize = result.get(i).size();
					for (int j = 0; j < middleSize; j++) {
						innerSize = result.get(i).get(j).length;
						for (int k = 0; k < innerSize; k++) {
							if (!expectedResult.get(i).get(j)[k].equals(result.get(i).get(j)[k])) {
								fail();
							}
						}
					}
				}
				//Then pass
			} catch (IndexOutOfBoundsException e) {
				fail();
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	
	@Test
	public void test08CreateMeetingReq() {
		ArrayList<String> attendees = new ArrayList<String>();
		ArrayList<Boolean> accepted = new ArrayList<Boolean>();
		attendees.add("JD");
		attendees.add("Tester1");
		attendees.add("Tester2");
		accepted.add(false);
		accepted.add(false);
		accepted.add(false);
		ArrayList<Comment> comments = new ArrayList<Comment>();
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1379743200000L);
		Meeting inputMeeting = new Meeting(-1, true, "title", "location",
				"description", 60, startTime, "yousirname", attendees,
				accepted, comments);
		
		Meeting expectedMeeting = new Meeting(testMeetingNum, true, "title", "location",
				"description", 60, startTime, "yousirname", attendees,
				accepted, comments);
		
		DatabaseRequest request = new DatabaseRequest(new CreateMeetingReq(inputMeeting), STyousirname);
		RH.addRequest(request);
		
		try {
			Meeting testMeeting = (Meeting) STyousirname.input.take();
			assertEquals(expectedMeeting, testMeeting);
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void test09EditAcceptanceReq() {
		Meeting expectedMeeting;
		DatabaseRequest request;
		boolean result;
		Meeting testMeeting;
		int index;
		try {//Reject meeting
			expectedMeeting = (Meeting) Database.getEvent(testMeetingNum);
			index = expectedMeeting.getAttendees().indexOf("Tester2");
			expectedMeeting.getAttendees().remove(index);
			expectedMeeting.getAccepted().remove(index);
			request = new DatabaseRequest(new EditAcceptanceReq(testMeetingNum, "Tester2", false), STyousirname);
			RH.addRequest(request);
			result = (boolean) STyousirname.input.take();
			assertTrue(result);
			testMeeting = (Meeting) Database.getEvent(testMeetingNum);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			e.printStackTrace();
			fail();
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
		try {//Accept meeting
			expectedMeeting = (Meeting) Database.getEvent(testMeetingNum);
			index = expectedMeeting.getAttendees().indexOf("JD");
			expectedMeeting.getAccepted().set(index, true);
			request = new DatabaseRequest(new EditAcceptanceReq(testMeetingNum, "Tester1", true), STyousirname);
			RH.addRequest(request);
			result = (boolean) STyousirname.input.take();
			assertTrue(result);
			testMeeting = (Meeting) Database.getEvent(testMeetingNum);
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			e.printStackTrace();
			fail();
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void test10UpdateInitiatorReq() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(testMeetingNum);
			expectedMeeting.changeInitiator("JD");
			DatabaseRequest request = new DatabaseRequest(new UpdateInitiatorReq(testMeetingNum, "yousirname", "JD"), STyousirname);
			RH.addRequest(request);
			Meeting testMeeting = (Meeting) STyousirname.input.take();
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			e.printStackTrace();
			fail();
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void test11RemoveAttendeeReq() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(testMeetingNum);
			int index = expectedMeeting.getAttendees().indexOf("Tester1");
			expectedMeeting.getAttendees().remove(index);
			expectedMeeting.getAccepted().remove(index);
			DatabaseRequest request = new DatabaseRequest(new RemoveAttendeeReq(testMeetingNum, "Tester1"), STyousirname);
			RH.addRequest(request);
			Meeting testMeeting = (Meeting) STyousirname.input.take();
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			e.printStackTrace();	
			fail();
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	
	@Test
	public void test12UpdateMeetingReq() {
		try {
			Meeting expectedMeeting = (Meeting) Database.getEvent(testMeetingNum);
			expectedMeeting.setName("EditedName");
			expectedMeeting.setDescription("Edited description");
			expectedMeeting.setLocation("EditedLocation");
			DatabaseRequest request = new DatabaseRequest(new UpdateMeetingReq(expectedMeeting), STyousirname);
			RH.addRequest(request);
			Meeting testMeeting = (Meeting) STyousirname.input.take();
			assertEquals(expectedMeeting, testMeeting);
		} catch (EventNotFoundException e) {
			e.printStackTrace();
			fail("Could not find meeting to edit");
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	
	@Test
	public void test13DeleteEventReq() {
		try {
			Database.getEvent(testMeetingNum);     //checks that the Meeting exists
		} catch (EventNotFoundException e) {
			fail("Event was not found before deletion");
		}	
		
		DatabaseRequest request = new DatabaseRequest(new DeleteEventReq(testMeetingNum, true), STyousirname);	
		RH.addRequest(request);
		try {
			Thread.sleep(10); //Wait for request to be completed
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			fail();
		}
		
		try {
			
			Database.getEvent(testMeetingNum);
			fail("Event was found in the database after it was deleted");
		} catch (EventNotFoundException e) {
			// Test cases passes
		}
	}
	
	
	@Test
	public void test14GetUserReq() {
		//Test with a valid username
		User expectedUser;
		GetUserReq request;
		DatabaseRequest DBReq;
		try {
			expectedUser = Database.getUser("yousirname");
			request = new GetUserReq("yousirname");
			DBReq = new DatabaseRequest(request, STyousirname);
			RH.addRequest(DBReq);
			User testUser = (User) STyousirname.input.take();
			assertEquals(expectedUser, testUser);
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			fail();
		} catch (InterruptedException e) {
			fail();
		}
		
		//Test with bad username
		try {
			request = new GetUserReq("badUsername");
			DBReq = new DatabaseRequest(request, STyousirname);
			RH.addRequest(DBReq);
			ServerError result = (ServerError) STyousirname.input.take();
			assertEquals(UserNotFoundException.class, result.exception.getClass());
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@Test
	public void test15CreateUserReq() {
		Schedule expectedSchedule = new Schedule(new ArrayList<Event>(), new ArrayList<Boolean>());
		User expectedUser = new User("Test", "not set", "not set",
				"bio not set", "positon not set", "room not set",
				"email not set", "phone not set", expectedSchedule);
		char[] password = new char[] {'a','b','c','d','e'};
		ServiceThread STnewUser = new ServiceThread(null, RH);
		DatabaseRequest request = new DatabaseRequest(new CreateUserReq(expectedUser, password), STnewUser);
		
		RH.addRequest(request);
		
		try {
			User testUser = (User) STnewUser.input.take();
			assertEquals(expectedUser, testUser);
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	
	@Test
	public void test16UpdateUserReq() {
		Schedule expectedSchedule = new Schedule(new ArrayList<Event>(),
				new ArrayList<Boolean>());
		User expectedUser = new User("Test", "first name", "last name",
				"new bio", "new position", "room 222", "email@eamil.email",
				"123456789", expectedSchedule);
		

		try {
			char[] newPassword = new char[] {};
			DatabaseRequest request = new DatabaseRequest(new UpdateUserReq(expectedUser, newPassword), STyousirname);
			RH.addRequest(request);
			User testUser = (User) STyousirname.input.take();
			assertEquals(expectedUser, testUser);
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void test17CheckAvailabliltyReq() {
		ArrayList<String[]> expectedResult;
		ArrayList<String[]> result;
		ArrayList<String> users = new ArrayList<String>();
		users.add("yousirname");
		users.add("JD");
		users.add("Tester1");
		
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1380870000000L);
		int length = 60;
		
		
		DatabaseRequest request = new DatabaseRequest(new CheckAvailabilityReq(users, startTime, length, -1), STyousirname);
		RH.addRequest(request);
		try {
			result = (ArrayList<String[]>) STyousirname.input.take();
			expectedResult = new ArrayList<String[]>(); //result should be empty
			assertEquals(expectedResult, result);
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
		
		length = 75;
		request = new DatabaseRequest(new CheckAvailabilityReq(users, startTime, length, -1), STyousirname);
		RH.addRequest(request);
		
		try {
			result = (ArrayList<String[]>) STyousirname.input.take();
			expectedResult = new ArrayList<String[]>();
			expectedResult.add(new String[] {"yousirname", "Daniel", "Hope"});
			//check equality between expectedResult and result
			int size = result.size();
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < 3; j++) {
					if (! result.get(i)[j].equals(expectedResult.get(i)[j])) {
						fail();
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
		
		length = 120;
		startTime.setTimeInMillis(1380762000000L);
		request = new DatabaseRequest(new CheckAvailabilityReq(users, startTime, length, -1), STyousirname);
		RH.addRequest(request);
		
		try {
			result = (ArrayList<String[]>) STyousirname.input.take();
			expectedResult = new ArrayList<String[]>();
			expectedResult.add(new String[] {"yousirname", "Daniel", "Hope"});
			expectedResult.add(new String[] {"JD", "Jay", "Dee"});
			expectedResult.add(new String[] {"Tester1", "Montgomery", "Anderson"});
			int size = result.size();
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < 3; j++) {
					if (! result.get(i)[j].equals(expectedResult.get(i)[j])) {
						fail();
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			fail();
		}
		
	}
	
	
	@Test
	public void test18LogoutReq() {
		//Check the Request Handler has the correct connected Users
		ArrayList<String> expectedUsers = new ArrayList<String>();
		expectedUsers.add("yousirname");
		expectedUsers.add("JD");
		expectedUsers.add("Tester1");
		assertEquals(expectedUsers, RH.getConnectedUsers());
		
		//Logout yousirname
		DatabaseRequest request = new DatabaseRequest(new LogoutReq("yousirname"), STyousirname);
		RH.addRequest(request);
		try { //wait for the request handler to complete the request
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//check that yousirname was removed from the connected users list
		expectedUsers.remove("yousirname");
		assertEquals(expectedUsers, RH.getConnectedUsers());
		
	}
	
}
