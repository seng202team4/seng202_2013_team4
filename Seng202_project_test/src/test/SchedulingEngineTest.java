package test;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import server.SchedulingEngine;
import shared.Event;
import shared.Meeting;
import shared.Schedule;
import shared.TimeConstants;
import shared.User;

/**
 * @author Zack McGrath
 */
public class SchedulingEngineTest implements TimeConstants {
	public static final String DATE_FORMAT_STRING = "EEE dd MMM HH:mm yyyy";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);
	
    // Creating test user 1, TESTER1
	public static String TESTER1_USERNAME = "Tester1";
	public static String TESTER1_FNAME ="Coco";
	public static String TESTER1_LNAME ="Buttersugar";
	public static String TESTER1_BIO = "I liek kittehs.";
	public static String TESTER1_POSITION = "SENG202 tutor";
	public static String TESTER1_ROOM = "The morgue";
	public static String TESTER1_EMAIL ="cocobutter@uclive.ac.nz";
	public static String TESTER1_PHONE = "+64 27 012 3456";
	
	// Creating test user 2, TESTER2
    public static String TESTER2_USERNAME = "Tester2";
    public static String TESTER2_FNAME = "Prof. Chicken";
    public static String TESTER2_LNAME = "Smith";
    public static String TESTER2_BIO = "I disliek kittehs.";
    public static String TESTER2_POSITION = "Lecturer of biochemistry";
    public static String TESTER2_ROOM = "Rutherford 9001";
    public static String TESTER2_EMAIL = "peckthekittehs@bing.org";
    public static String TESTER2_PHONE = "+64 29 010 1010";
    
    // Creating test user 3, TESTER3
    public static String TESTER3_USERNAME = "Tester3";
    public static String TESTER3_FNAME = "Dan dan";
    public static String TESTER3_LNAME = "Hopeless";
    public static String TESTER3_BIO = "Herp didley derp.";
    public static String TESTER3_POSITION = "Town cabbage";
    public static String TESTER3_ROOM = "The morgues floor";
    public static String TESTER3_EMAIL = "DanDanNoHope@hopelesswelps.co.gtfo";
    public static String TESTER3_PHONE = "5138008";
    
    // Creating test user 4, TESTER4
    public static String TESTER4_USERNAME = "Tester4";
    public static String TESTER4_FNAME = "Monkey duck";
    public static String TESTER4_LNAME = "Soup sauce";
    public static String TESTER4_BIO = "I don't even know what I am.";
    public static String TESTER4_POSITION = "Left";
    public static String TESTER4_ROOM = "1304";
    public static String TESTER4_EMAIL = "monkeyDuckSoupSauce@food.co.idk";
    public static String TESTER4_PHONE = "0800 83 83 83";
    
    // Creating test event 1.
    public static int EVENT1_ID = 2;
    public static String EVENT1_NAME = "Test event1";
    public static String EVENT1_LOCATION = "The test spa-house";
    public static String EVENT1_DESCRIPTION = "A test spa-party.";
    public static int EVENT1_DURATION = 30;
    public static String EVENT1_START_STRING = "Mon 20 Oct 16:00 2013";
    public static Calendar EVENT1_START_TIME;
    
    // Creating test meeting 1
    public static int MEETING1_ID = 1;
    public static String MEETING1_NAME = "Test meeting1";
    public static String MEETING1_LOCATION = "The test realm";
    public static String MEETING1_DESCRIPTION = "A test meeting.";
    public static int MEETING1_DURATION = 45;
    public static String MEETING1_START_STRING = "Mon 20 Oct 09:00 2013";
    public static Calendar MEETING1_START_TIME;
    public static ArrayList<String> MEETING1_ATTENDEES =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME, TESTER3_USERNAME, TESTER4_USERNAME));
    
    public static int MEETING2_ID = 3;
    public static String MEETING2_NAME = "Test meeting2";
    public static String MEETING2_LOCATION = "The test dimension";
    public static String MEETING2_DESCRIPTION = "A multi-dimensional test meeting";
    public static int MEETING2_DURATION = 60;
    public static String MEETING2_START_STRING = "Mon 20 Oct 00:00 2013";
    public static Calendar MEETING2_START_TIME;
    public static ArrayList<String> MEETING2_ATTENDEES =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME, TESTER3_USERNAME));
    
	public static String NEW_MEETING_START_STRING = "Mon 20 Oct 00:00 2013";
	public static String NEW_MEETING_END_STRING = "Mon 20 Oct 02:00 2013";
	public static Calendar NEW_MEETING_START_TIME;
	public static Calendar NEW_MEETING_END_TIME;
	
    public static Event EVENT1;
    public static Meeting MEETING1;
    public static Meeting MEETING2;
    
	public static User TESTER1;
	public static User TESTER2;
	public static User TESTER3;
	public static User TESTER4;
	
	@Before
	public void setUp(){
		givenTester1Created();
		givenTester2Created();
		givenTester3Created();
		givenTester4Created();
		
		givenEvent1ObjectCreated();
		givenMeeting1ObjectCreated();
		givenMeeting2ObjectCreated();
	}
	
	
	@Test
	public void testGetBusyUserTriplesForDuration() {
		try {
			NEW_MEETING_START_TIME = Calendar.getInstance();
			NEW_MEETING_END_TIME = Calendar.getInstance();
			NEW_MEETING_START_TIME.setTime(DATE_FORMAT.parse(NEW_MEETING_START_STRING));
			NEW_MEETING_END_TIME.setTime(DATE_FORMAT.parse(NEW_MEETING_END_STRING));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		ArrayList<User> attendees = new ArrayList<>();
		attendees.add(TESTER1);
		attendees.add(TESTER2);
		attendees.add(TESTER3);
		attendees.add(TESTER4);
		
		ArrayList<ArrayList<String[]>> ret = SchedulingEngine.getBusyUserTriplesForDuration(
				attendees, NEW_MEETING_START_TIME, NEW_MEETING_END_TIME, 60);
		
		ArrayList<ArrayList<String>> usersBusy = new ArrayList<>();
		int i = 0;
		ArrayList<String> a = MEETING2_ATTENDEES;
		a.add(TESTER1_USERNAME);
		while (i < MEETING2_DURATION / MIN_PER_TIME_SLOT) {
			System.out.println(a);
			usersBusy.add(a);
			i++;
		}
		while (i < ret.size()) {
			usersBusy.add(new ArrayList<String>());
			i++;
		}

		int[] retArray = new int[ret.size()];
		int[] numUsersBusy = new int[ret.size()];
		for (int j = 0; j < ret.size(); j++) {
			retArray[j] = ret.get(j).size();
			numUsersBusy[j] = usersBusy.get(j).size();
		}
		for (int j = 0; j < ret.size(); j++) {
			assertTrue(retArray[j] == numUsersBusy[j]);
		}
	}


	public void givenTester1Created() {
		TESTER1 = new User(
				TESTER1_USERNAME,
				TESTER1_FNAME,
				TESTER1_LNAME,
				TESTER1_BIO,
				TESTER1_POSITION,
				TESTER1_ROOM,
				TESTER1_EMAIL,
				TESTER1_PHONE,
				new Schedule(
						new ArrayList<Event>(),
                        new ArrayList<Boolean>()));
	}


	public void givenTester2Created() {
		TESTER2 = new User(
				TESTER2_USERNAME,
				TESTER2_FNAME,
				TESTER2_LNAME,
				TESTER2_BIO,
				TESTER2_POSITION,
				TESTER2_ROOM,
				TESTER2_EMAIL,
				TESTER2_PHONE,
				new Schedule(
						new ArrayList<Event>(),
                        new ArrayList<Boolean>()));
	}


	public void givenTester3Created() {
		TESTER3 = new User(
				TESTER3_USERNAME,
				TESTER3_FNAME,
				TESTER3_LNAME,
				TESTER3_BIO,
				TESTER3_POSITION,
				TESTER3_ROOM,
				TESTER3_EMAIL,
				TESTER3_PHONE,
				new Schedule(
						new ArrayList<Event>(),
                        new ArrayList<Boolean>()));
	}


	public void givenTester4Created() {
		TESTER4 = new User(
				TESTER4_USERNAME,
				TESTER4_FNAME,
				TESTER4_LNAME,
				TESTER4_BIO,
				TESTER4_POSITION,
				TESTER4_ROOM,
				TESTER4_EMAIL,
				TESTER4_PHONE,
				new Schedule(
						new ArrayList<Event>(),
                        new ArrayList<Boolean>()));
	}


	public void givenEvent1ObjectCreated() {
		try {
			EVENT1_START_TIME = Calendar.getInstance();
			EVENT1_START_TIME.setTime(DATE_FORMAT.parse(EVENT1_START_STRING));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		EVENT1 = new Event(
                EVENT1_ID,
                false,
                EVENT1_NAME,
                EVENT1_LOCATION,
                EVENT1_DESCRIPTION,
                EVENT1_DURATION,
                EVENT1_START_TIME,
                TESTER1.getUsername()
        );
		
		TESTER1.getSchedule().addEvent(EVENT1, true);
	}


	public void givenMeeting1ObjectCreated() {
		try {
			MEETING1_START_TIME = Calendar.getInstance();
			MEETING1_START_TIME.setTime(DATE_FORMAT.parse(MEETING1_START_STRING));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		ArrayList<Boolean> meeting1_accepted = new ArrayList<>();
		for (int i = 0; i < MEETING2_ATTENDEES.size(); i++)
			meeting1_accepted.add(false);
		
		MEETING1 = new Meeting(
                MEETING1_ID,
                true,
                MEETING1_NAME,
                MEETING1_LOCATION,
                MEETING1_DESCRIPTION,
                MEETING1_DURATION,
                MEETING1_START_TIME,
                TESTER1.getUsername(),
                MEETING2_ATTENDEES,
                meeting1_accepted);
		
		TESTER1.getSchedule().addEvent(MEETING1, true);
		TESTER2.getSchedule().addEvent(MEETING1, true);
		TESTER3.getSchedule().addEvent(MEETING1, true);
		TESTER4.getSchedule().addEvent(MEETING1, true);
	}
	
	
	public void givenMeeting2ObjectCreated() {
		try {
			MEETING2_START_TIME = Calendar.getInstance();
			MEETING2_START_TIME.setTime(DATE_FORMAT.parse(MEETING2_START_STRING));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		ArrayList<Boolean> meeting2_accepted = new ArrayList<>();
		for (int i = 0; i < MEETING2_ATTENDEES.size(); i++)
			meeting2_accepted.add(false);
		
		MEETING2 = new Meeting(
                MEETING2_ID,
                true,
                MEETING2_NAME,
                MEETING2_LOCATION,
                MEETING2_DESCRIPTION,
                MEETING2_DURATION,
                MEETING2_START_TIME,
                TESTER1_USERNAME,
                MEETING2_ATTENDEES,
                meeting2_accepted);
		
		TESTER1.getSchedule().addEvent(MEETING2, true);
		TESTER2.getSchedule().addEvent(MEETING2, true);
		TESTER4.getSchedule().addEvent(MEETING2, true);
	}
		
	
	public boolean checkTripleResult(ArrayList<String[]> expected, ArrayList<String[]> result) {
		int length = expected.size();
		if (length != result.size()) {
			return false;
		}
		
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < 3; j++) {
				if (! expected.get(i)[j].equals(result.get(i)[j])) {
					return false;
				}
			}
		}
		return true;
	}
	
	
	/*
	@Test
	public void test1() {
		ArrayList<String[]> expectedResult;
		ArrayList<String[]> result;
		
		ArrayList<String> users = new ArrayList<String>();
		users.add("yousirname");
		users.add("JD");
		users.add("Tester1");
		
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(1380870000000L);
		int length = 60;
		expectedResult = new ArrayList<String[]>(); //result should be empty
		result = SchedulingEngine.getUserTriplesBusyAt(attendees, start);
		assertTrue(checkTripleResult(expectedResult, result));
		
		

		length = 75;
		expectedResult = new ArrayList<String[]>();
		expectedResult.add(new String[] {"yousirname", "Daniel", "Hope"});
			//check equality between expectedResult and result
		int size = result.size();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 3; j++) {
				if (! result.get(i)[j].equals(expectedResult.get(i)[j])) {
					fail();
				}
			}
		}
		
		
		
		length = 120;
		startTime.setTimeInMillis(1380762000000L);
		
		
		expectedResult = new ArrayList<String[]>();
		expectedResult.add(new String[] {"yousirname", "Daniel", "Hope"});
		expectedResult.add(new String[] {"JD", "Jay", "Dee"});
		expectedResult.add(new String[] {"Tester1", "Montgomery", "Anderson"});
		size = result.size();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 3; j++) {
				if (! result.get(i)[j].equals(expectedResult.get(i)[j])) {
					fail();
				}
			}
		}
	}*/
}
