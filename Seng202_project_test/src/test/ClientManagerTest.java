package test;

 
import client.ClientManager;
import client.ClientNetworkingManager;
import exceptions.InvalidTimeslotException;

import exceptions.InvalidUsernameException;
import exceptions.LoginErrorException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import shared.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import static org.mockito.Mockito.*;
 
/**
 * Behavioural driven development test cases for the clientManager. Tests the majority of the dynamic methods, the
 * majority of missing test coverage is unnecessary.
 * @author Montgomery Anderson
 */
public class ClientManagerTest {

    public static ClientNetworkingManager mockNetworkingManager;
    public static boolean loggedIn = false;
    public static boolean correctFormat = false;
    public static Exception exceptionThrown;
    public static ArrayList<Event> todaysEvents;
    public static ArrayList<String[]> userTriples;

    public static final String DATE_FORMAT_STRING = "EEE dd MMM HH:mm yyyy";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);

    // Creating test user 1, TESTER1
    public static User TESTER1;
    public static String TESTER1_USERNAME = "Tester1";
    public static String TESTER1_FNAME ="Coco";
    public static String TESTER1_LNAME ="Buttersugar";
    public static String TESTER1_BIO = "I liek kittehs.";
    public static String TESTER1_POSITION = "SENG202 tutor";
    public static String TESTER1_ROOM = "The morgue";
    public static String TESTER1_EMAIL ="cocobutter@uclive.ac.nz";
    public static String TESTER1_PHONE = "+64 27 012 3456";
    // Creating test user 1's password, TESTER1_PASSWORD
    public static char[] TESTER1_PASSWORD = {'p','a','s','s','w','o','r','d'};
    // Creating a 'incorrect' arbitrary password, INCORRECT_PASSWORD, i.e. forgot one 's'
    public static char[] INCORRECT_PASSWORD = {'p','a','s','w','o','r','d'};

    // Creating test user 2, TESTER2
    public static User TESTER2;
    public static String TESTER2_USERNAME = "Tester2";
    public static String TESTER2_FNAME = "Prof. Chicken";
    public static String TESTER2_LNAME = "Smith";
    public static String TESTER2_BIO = "I disliek kittehs.";
    public static String TESTER2_POSITION = "Lecturer of biochemistry";
    public static String TESTER2_ROOM = "Rutherford 9001";
    public static String TESTER2_EMAIL = "peckthekittehs@bing.org";
    public static String TESTER2_PHONE = "+64 29 010 1010";
    // Creating test user 2's password, TESTER2_PASSWORD
    public static char[] TESTER2_PASSWORD = {'p','a','r','s','e','w','o','r','d'};

    // Creating test user 3, TESTER3
    public static User TESTER3;
    public static String TESTER3_USERNAME = "Tester3";
    public static String TESTER3_FNAME = "Dan dan";
    public static String TESTER3_LNAME = "Hopeless";
    public static String TESTER3_BIO = "Herp didley derp.";
    public static String TESTER3_POSITION = "Town cabbage";
    public static String TESTER3_ROOM = "The morgues floor";
    public static String TESTER3_EMAIL = "DanDanNoHope@hopelesswelps.co.gtfo";
    public static String TESTER3_PHONE = "5138008";
    // Creating test user 3's password, TESTER3_PASSWORD
    public static char[] TESTER3_PASSWORD = {'b','o','y','s'};

    // Creating test user 4, TESTER4
    public static User TESTER4;
    public static String TESTER4_USERNAME = "Tester4";
    public static String TESTER4_FNAME = "Monkey duck";
    public static String TESTER4_LNAME = "Soup sauce";
    public static String TESTER4_BIO = "I don't even know what I am.";
    public static String TESTER4_POSITION = "Left";
    public static String TESTER4_ROOM = "1304";
    public static String TESTER4_EMAIL = "monkeyDuckSoupSauce@food.co.idk";
    public static String TESTER4_PHONE = "0800 83 83 83";
    public static char[] TESTER4_PASSWORD = {'m','a','y','o'};

    public static int MEETING1_ID = 1;
    public static String MEETING1_NAME = "Test meeting1";
    public static String MEETING1_LOCATION = "The test realm";
    public static String MEETING1_DESCRIPTION = "A test meeting.";
    public static int MEETING1_DURATION = 45;
    public static String MEETING1_START_STRING = "Mon 20 Oct 09:00 2013";
    public static Calendar MEETING1_START_TIME;
    public static ArrayList<String> MEETING1_ATTENDEES =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME));
    public static Meeting MEETING1;

    public static int EVENT1_ID = 2;
    public static String EVENT1_NAME = "Test event1";
    public static String EVENT1_LOCATION = "The test spa-house";
    public static String EVENT1_DESCRIPTION = "A test spa-party.";
    public static int EVENT1_DURATION = 30;
    public static String EVENT1_START_STRING = "Mon 20 Oct 16:00 2013";
    public static Calendar EVENT1_START_TIME;
    public static Event EVENT1;

    public static int MEETING2_ID = 3;
    public static String MEETING2_NAME = "Test meeting2";
    public static String MEETING2_NEW_NAME = "Updated Test meeting2";
    public static String MEETING2_LOCATION = "The test dimension";
    public static String MEETING2_NEW_LOCATION = "Test dimension 2.0";
    public static String MEETING2_DESCRIPTION = "A multi-dimensional test meeting";
    public static String MEETING2_NEW_DESCRIPTION = "A maxi-dimensional updated test l33ting";
    public static int MEETING2_DURATION = 60;
    public static String MEETING2_START_STRING = EVENT1_START_STRING;
    public static Calendar MEETING2_START_TIME;
    public static ArrayList<String> MEETING2_ATTENDEES =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME));
    public static ArrayList<String> MEETING2_NEW_ATTENDEES_1 =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME, TESTER3_USERNAME));
    public static ArrayList<String> MEETING2_NEW_ATTENDEES_2 =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME, TESTER4_USERNAME));
    public static Meeting MEETING2;

    public static int EVENT2_ID = 4;
    public static String EVENT2_NAME = "Test clash with meeting";
    public static String EVENT2_NEW_NAME = "New clash test meeting ;)";
    public static String EVENT2_LOCATION = "My drink bottle";
    public static String EVENT2_NEW_LOCATION = "My lift+ can";
    public static String EVENT2_DESCRIPTION = "An event to test meeting 1 cannot be scheduled in a clash.";
    public static String EVENT2_NEW_DESCRIPTION = "A new updated event2, (daniels are not allowed).";
    public static int EVENT2_DURATION = 90;
    public static int EVENT2_NEW_DURATION = 60;
    public static String EVENT2_START_STRING = MEETING1_START_STRING;
    public static Calendar EVENT2_START_TIME;
    public static String EVENT2_NEW_START_STRING = EVENT1_START_STRING;
    public static Calendar EVENT2_NEW_START_TIME;
    public static Event EVENT2;

    public static int COMMENT1_ID = 1;
    public static int COMMENT1_MEETING_ID = MEETING2_ID;
    public static String COMMENT1_OWNER = TESTER1_USERNAME;
    public static Calendar COMMENT1_TIMESTAMP;
    public static String COMMENT1_COMMENT = "Yolo swag";
    public static Comment COMMENT1 =
            new Comment(COMMENT1_ID, COMMENT1_MEETING_ID, COMMENT1_OWNER, COMMENT1_TIMESTAMP, COMMENT1_COMMENT);

    public static int TEST_WINDOW_DURATION = 60;


    @Before
    public void givenLaunched() {
        // reset old data
        ClientManager.reset();
        loggedIn = false;
        correctFormat = false;
        todaysEvents = null;
        userTriples = null;
        exceptionThrown = null;
        MEETING1 = null;
        EVENT1 = null;
        MEETING2 = null;
        EVENT2 = null;
        TESTER1 = null;
        TESTER2 = null;
        TESTER3 = null;
        TESTER4 = null;
        mockNetworkingManager = null;
        // launched the clientManager if not already launched.
        ClientManager.getClientManager();
    }

    /**
     * Simulates having test user 1 registered on the database.
     * @throws IOException      If this is thrown there has been an error, then fail.
     */
    public void givenTester1Registered() {
        TESTER1 = new User(
                TESTER1_USERNAME,
                TESTER1_FNAME,
                TESTER1_LNAME,
                TESTER1_BIO,
                TESTER1_POSITION,
                TESTER1_ROOM,
                TESTER1_EMAIL,
                TESTER1_PHONE,
                new Schedule(
                        new ArrayList<Event>(),
                        new ArrayList<Boolean>()
                )
        );
        try {
            // simulate it being registered on the database
            when(mockNetworkingManager.getUser(TESTER1.getUsername())).thenReturn(TESTER1);
            when(mockNetworkingManager.login(TESTER1.getUsername(), TESTER1_PASSWORD)).thenReturn(TESTER1);
            User FRESH_TESTER1 = new User(TESTER1_USERNAME, TESTER1_FNAME, TESTER1_LNAME, TESTER1_BIO, TESTER1_POSITION,
                    TESTER1_ROOM, TESTER1_EMAIL, TESTER1_PHONE, new Schedule(new ArrayList<Event>(),
                    new ArrayList<Boolean>()));
            when(mockNetworkingManager.registerUser(FRESH_TESTER1, TESTER1_PASSWORD)).thenThrow(new InvalidUsernameException());
        } catch(Exception e) {
            exceptionThrown = e;
        }
    }

    public void givenTester2Registered() {
        TESTER2 = new User(
                TESTER2_USERNAME,
                TESTER2_FNAME,
                TESTER2_LNAME,
                TESTER2_BIO,
                TESTER2_POSITION,
                TESTER2_ROOM,
                TESTER2_EMAIL,
                TESTER2_PHONE,
                new Schedule(
                        new ArrayList<Event>(),
                        new ArrayList<Boolean>()
                )
        );
        try {
            // simulate it being registered on the database
            when(mockNetworkingManager.getUser(TESTER2_USERNAME)).thenReturn(TESTER2);
            when(mockNetworkingManager.login(TESTER2_USERNAME,TESTER2_PASSWORD)).thenReturn(TESTER2);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void givenTester3Registered() {
        TESTER4 = new User(
                TESTER3_USERNAME,
                TESTER3_FNAME,
                TESTER3_LNAME,
                TESTER3_BIO,
                TESTER3_POSITION,
                TESTER3_ROOM,
                TESTER3_EMAIL,
                TESTER3_PHONE,
                new Schedule(
                        new ArrayList<Event>(),
                        new ArrayList<Boolean>()
                )
        );
        try {
            // simulate it being registered on the database
            when(mockNetworkingManager.getUser(TESTER3_USERNAME)).thenReturn(TESTER3);
            when(mockNetworkingManager.login(TESTER3_USERNAME,TESTER3_PASSWORD)).thenReturn(TESTER3);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void givenTester4Registered() {
        TESTER4 = new User(
                TESTER4_USERNAME,
                TESTER4_FNAME,
                TESTER4_LNAME,
                TESTER4_BIO,
                TESTER4_POSITION,
                TESTER4_ROOM,
                TESTER4_EMAIL,
                TESTER4_PHONE,
                new Schedule(
                        new ArrayList<Event>(),
                        new ArrayList<Boolean>()
                )
        );
        try {
            // simulate it being registered on the database
            when(mockNetworkingManager.getUser(TESTER4_USERNAME)).thenReturn(TESTER4);
            when(mockNetworkingManager.login(TESTER4_USERNAME,TESTER4_PASSWORD)).thenReturn(TESTER4);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void givenMeeting1ObjectCreated() {
        // create a calendar object to set the start Time
        try {
            MEETING1_START_TIME = Calendar.getInstance();
            MEETING1_START_TIME.setTime(DATE_FORMAT.parse(MEETING1_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the meeting object
        MEETING1 = new Meeting(
                MEETING1_ID,
                true,
                MEETING1_NAME,
                MEETING1_LOCATION,
                MEETING1_DESCRIPTION,
                MEETING1_DURATION,
                MEETING1_START_TIME,
                TESTER1.getUsername(),
                new ArrayList<>(Arrays.asList(TESTER2_USERNAME)),
                new ArrayList<>(Arrays.asList(false))
        );
        // add it to TESTER2's schedule
        TESTER2.getSchedule().addEvent(MEETING1, false);
        try {
            when(mockNetworkingManager.requestMeetingCreation(
                    MEETING1_NAME, MEETING1_LOCATION, MEETING1_DESCRIPTION, MEETING1_DURATION,
                    MEETING1_START_TIME, TESTER1_USERNAME, new ArrayList<>(Arrays.asList(TESTER2_USERNAME))
            )).thenReturn(MEETING1);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void givenEvent1ObjectCreated() {
        // create a calendar object to set the start Time
        try {
            EVENT1_START_TIME = Calendar.getInstance();
            EVENT1_START_TIME.setTime(DATE_FORMAT.parse(EVENT1_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the event object
        EVENT1 = new Event(
                EVENT1_ID,
                false,
                EVENT1_NAME,
                EVENT1_LOCATION,
                EVENT1_DESCRIPTION,
                EVENT1_DURATION,
                EVENT1_START_TIME,
                TESTER1.getUsername()
        );
        // add to TESTER1's schedule TODO
        //TESTER1.getSchedule().addEvent(EVENT1, true);
        try {
            when(mockNetworkingManager.requestEventCreation(
                    EVENT1_NAME, EVENT1_LOCATION, EVENT1_DESCRIPTION, EVENT1_DURATION,
                    EVENT1_START_TIME, TESTER1_USERNAME
            )).thenReturn(EVENT1);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void givenMeeting2Created() {
        // create a calendar object to set the start Time
        try {
            MEETING2_START_TIME = Calendar.getInstance();
            MEETING2_START_TIME.setTime(DATE_FORMAT.parse(MEETING2_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the meeting object
        MEETING2 = new Meeting(
                MEETING2_ID,
                true,
                MEETING2_NAME,
                MEETING2_LOCATION,
                MEETING2_DESCRIPTION,
                MEETING2_DURATION,
                MEETING2_START_TIME,
                TESTER1_USERNAME,
                new ArrayList<String>(),
                new ArrayList<Boolean>()
        );
        // add to TESTER1's schedule
        TESTER1.getSchedule().addEvent(MEETING2, true);
        // add the attendees - also adds it to the attendees schedule
        MEETING2.addAttendees(new ArrayList<>(Arrays.asList(TESTER2)));
        try {
            Meeting updatedMeeting2 = new Meeting(MEETING2_ID, true, MEETING2_NEW_NAME, MEETING2_NEW_LOCATION,
                    MEETING2_NEW_DESCRIPTION, MEETING2_DURATION, MEETING2_START_TIME, TESTER1_USERNAME,
                    MEETING2_NEW_ATTENDEES_1, MEETING2.getAccepted(), MEETING2.getComments());
            when(mockNetworkingManager.updateMeeting(updatedMeeting2)).thenReturn(updatedMeeting2);
            // mock changing attendees to MEETING2_NEW_ATTENDEES_!
            ArrayList<String> addedAttendees1 = new ArrayList<>();
            for(String attendee : MEETING2_NEW_ATTENDEES_1) {
                if(! MEETING2_ATTENDEES.contains(attendee)) {
                    addedAttendees1.add(attendee);
                }
            }
            when(mockNetworkingManager.checkAvailable(addedAttendees1, MEETING2_START_TIME, MEETING2_DURATION, MEETING2_ID))
                    .thenReturn(new ArrayList<String[]>());
            // mock changing attendees to MEETING2_NEW_ATTENDEES_2
            ArrayList<String> addedAttendees2 = new ArrayList<>();
            for(String attendee : MEETING2_NEW_ATTENDEES_2) {
                if(! MEETING2_ATTENDEES.contains(attendee)) {
                    addedAttendees2.add(attendee);
                }
            }
            String[] triple = new String[] {TESTER4.getUsername(), TESTER4.getFirstname(), TESTER4.getLastName()};
            ArrayList<String[]> unavailableAttendees = new ArrayList<>();
            unavailableAttendees.add(triple);
            when(mockNetworkingManager.checkAvailable(addedAttendees2, MEETING2_START_TIME, MEETING2_DURATION, MEETING2_ID))
                    .thenReturn(new ArrayList<>(unavailableAttendees));
            // mock commenting
            when(mockNetworkingManager.createComment(new Comment(-1, MEETING2_ID, COMMENT1_OWNER, (Calendar)any(),
                    COMMENT1_COMMENT))).thenReturn(COMMENT1);
        } catch (Exception e) {
            exceptionThrown = e;
        }
    }

    public void givenEvent2CreatedByTester1() {
        // create a calendar object to set the start Time
        try {
            EVENT2_START_TIME = Calendar.getInstance();
            EVENT2_START_TIME.setTime(DATE_FORMAT.parse(EVENT2_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the event object
        EVENT2 = new Event(
                EVENT2_ID,
                false,
                EVENT2_NAME,
                EVENT2_LOCATION,
                EVENT2_DESCRIPTION,
                EVENT2_DURATION,
                EVENT2_START_TIME,
                TESTER1_USERNAME
        );
        // add to TESTER1's schedule
        TESTER1.getSchedule().addEvent(EVENT2, true);
        try {
            when(mockNetworkingManager.requestMeetingCreation(MEETING1_NAME, MEETING1_LOCATION, MEETING1_DESCRIPTION,
                    MEETING1_DURATION, MEETING1_START_TIME, TESTER1_USERNAME, MEETING1_ATTENDEES)).thenThrow(
                    new InvalidTimeslotException()
            );
        } catch(IOException | InvalidTimeslotException e) {
            exceptionThrown = e;
        }
    }

    public void givenEvent2CreatedByTester4() {
        // create a calendar object to set the start Time
        try {
            EVENT2_START_TIME = Calendar.getInstance();
            EVENT2_START_TIME.setTime(DATE_FORMAT.parse(EVENT2_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the event object
        EVENT2 = new Event(
                EVENT2_ID,
                false,
                EVENT2_NAME,
                EVENT2_LOCATION,
                EVENT2_DESCRIPTION,
                EVENT2_DURATION,
                EVENT2_START_TIME,
                TESTER4_USERNAME
        );
        // add to TESTER1's schedule
        TESTER4.getSchedule().addEvent(EVENT2, true);
        try {
            when(mockNetworkingManager.requestMeetingCreation(MEETING1_NAME, MEETING1_LOCATION, MEETING1_DESCRIPTION,
                    MEETING1_DURATION, MEETING1_START_TIME, TESTER1_USERNAME, MEETING1_ATTENDEES)).thenThrow(
                    new InvalidTimeslotException()
            );
        } catch(IOException | InvalidTimeslotException e) {
            exceptionThrown = e;
        }
    }

    public void givenTester3ObjectCreated() {
        TESTER3 = new User(
                TESTER3_USERNAME,
                TESTER3_FNAME,
                TESTER3_LNAME,
                TESTER3_BIO,
                TESTER3_POSITION,
                TESTER3_ROOM,
                TESTER3_EMAIL,
                TESTER3_PHONE,
                new Schedule(
                        new ArrayList<Event>(),
                        new ArrayList<Boolean>()
                )
        );
    }

    public void givenTester3RegisterableInDatabase() {
        try {
            when(mockNetworkingManager.login(TESTER3_USERNAME, TESTER3_PASSWORD)).thenReturn(TESTER3);
            when(mockNetworkingManager.getUser(TESTER3_USERNAME)).thenReturn(TESTER3);
        } catch (IOException | LoginErrorException e) {
            exceptionThrown = e;
        }
    }

    public void givenTester3Registerable() {
        try {
            when(mockNetworkingManager.registerUser(TESTER3, TESTER3_PASSWORD)).thenReturn(TESTER3);
        } catch (IOException | InvalidUsernameException e) {
            exceptionThrown = e;
        }
    }

    public void givenUserListAvailable() {
        try {
            String[] tester1Triple = new String[] {TESTER1_USERNAME, TESTER1_FNAME, TESTER1_LNAME};
            String[] tester2Triple = new String[] {TESTER2_USERNAME, TESTER2_FNAME, TESTER2_LNAME};
            String[] tester4Triple = new String[] {TESTER4_USERNAME, TESTER4_FNAME, TESTER4_LNAME};
            ArrayList<String[]> userList = new ArrayList<>();
            userList.add(tester1Triple);
            userList.add(tester2Triple);
            userList.add(tester4Triple);
            when(mockNetworkingManager.getUserList()).thenReturn(userList);
        } catch (IOException ioe) {
            exceptionThrown = ioe;
        }
    }

    public void givenServerSimulated() {
        // create the mock networkManager
        mockNetworkingManager = mock(ClientNetworkingManager.class);
        ClientManager.setNetworkManager(mockNetworkingManager);
        givenTester1Registered();
        givenTester2Registered();
        givenTester4Registered();
        givenMeeting1ObjectCreated();
        givenEvent1ObjectCreated();
        givenTester3ObjectCreated();
        givenTester3Registerable();
        givenUserListAvailable();
    }

    public void whenTester1LoggedIn() {
        try {
            loggedIn = ClientManager.getClientManager().login(TESTER1.getUsername(), TESTER1_PASSWORD);
        } catch (Exception e) {
            exceptionThrown = e;
        }
    }

    public void thenTester1LoggedIn() {
        assert(loggedIn);
    }

    @Test
    public void testLoginCorrect() {
        givenLaunched();
        givenServerSimulated();
        whenTester1LoggedIn();
        thenTester1LoggedIn();
    }

    public void whenIncorrectLoginDetailsGiven() {
        try {
            loggedIn = ClientManager.getClientManager().login(TESTER1.getUsername(), INCORRECT_PASSWORD);
        } catch (Exception e) {
            exceptionThrown = e;
        }
    }

    public void thenNotLoggedIn() {
        assert(! loggedIn);
    }

    @Test
    public void testLoginIncorrect() {
        givenLaunched();
        givenServerSimulated();
        whenIncorrectLoginDetailsGiven();
        thenNotLoggedIn();
    }

    public void thenCurrentUserIsTester1() {
        assertEquals(TESTER1, ClientManager.getClientManager().getCurrentUser());
    }

    public void thenCurrentUserIsNull() {
        assertNull(ClientManager.getClientManager().getCurrentUser());
    }

    @Test
    public void testGetCurrentUserWhenLoggedIn() {
        givenLaunched();
        givenServerSimulated();
        whenTester1LoggedIn();
        thenCurrentUserIsTester1();
    }

    @Test
    public void testGetCurrentUserNotLoggedIn() {
        givenLaunched();
        givenServerSimulated();
        thenCurrentUserIsNull();
    }

    public void whenLogoutCalled() {
        try {
            ClientManager.getClientManager().logout();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Test
    public void testLogoutWhenLoggedIn() {
        givenLaunched();
        givenServerSimulated();
        whenTester1LoggedIn();
        whenLogoutCalled();
        thenCurrentUserIsNull();
    }

    public void whenCreateEvent1Called() {

        // create a calendar object to set the start Time
        try {
            EVENT1_START_TIME = Calendar.getInstance();
            EVENT1_START_TIME.setTime(DATE_FORMAT.parse(EVENT1_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
            fail();
        }
        // create the meeting object
        try {
            ClientManager.getClientManager().createEvent(
                    EVENT1_NAME,
                    EVENT1_LOCATION,
                    EVENT1_DESCRIPTION,
                    EVENT1_DURATION,
                    EVENT1_START_TIME
            );
        } catch(IOException ioe) {
            ioe.printStackTrace();
        } catch(InvalidTimeslotException ite) {
            exceptionThrown = ite;
        }
    }

    public void thenEvent1Created() {
        assert( TESTER1.getSchedule().getEventList().contains(EVENT1) );
    }

    @Test
    public void testCreateEventValidTimeSlot() {
        givenLaunched();
        givenServerSimulated();
        whenTester1LoggedIn();
        whenCreateEvent1Called();
        thenEvent1Created();
    }

    public void thenInvalidTimeSlotExceptionThrown() {
        // check the correct exception was thrown
        assertEquals(InvalidTimeslotException.class, exceptionThrown.getClass());
    }

    public void thenEvent1NotCreated() {
        // check that the event was not created
        assertFalse(TESTER1.getSchedule().getEventList().contains(EVENT1));
    }

    @Test
    public void testCreateEventInvalidTimeSlot() {
        givenLaunched();
        givenServerSimulated();
        givenMeeting2Created();
        whenTester1LoggedIn();
        whenCreateEvent1Called();
        thenInvalidTimeSlotExceptionThrown();
        thenEvent1NotCreated();
    }

    public void whenCreateMeeting1Called() {
        // create a calendar object to set the start Time
        try {
            MEETING1_START_TIME = Calendar.getInstance();
            MEETING1_START_TIME.setTime(DATE_FORMAT.parse(MEETING1_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
            fail();
        }
        // create the meeting object
        try {
            ClientManager.getClientManager().createMeeting(
                    MEETING1_NAME,
                    MEETING1_LOCATION,
                    MEETING1_DESCRIPTION,
                    MEETING1_DURATION,
                    MEETING1_START_TIME,
                    MEETING1_ATTENDEES
            );
        } catch(IOException ioe) {
            ioe.printStackTrace();
        } catch(InvalidTimeslotException ite) {
            exceptionThrown = ite;
        }
    }

    public void thenMeeting1Created() {
        assertTrue(
                TESTER1.getSchedule().getEventList().contains(MEETING1) &&
                        TESTER2.getSchedule().getEventList().contains(MEETING1) &&
                        MEETING1.getInitiator() == TESTER1.getUsername() &&
                        MEETING1.getAttendees().contains(TESTER2.getUsername())
        );
    }

    @Test
    public void testCreateMeetingValidTimeSlot() {
        givenLaunched();
        givenServerSimulated();
        whenTester1LoggedIn();
        whenCreateMeeting1Called();
        thenMeeting1Created();
    }

    public void thenMeeting1NotCreated() {
        // check the meeting wasn't created
        assertFalse(TESTER1.getSchedule().getEventList().contains(MEETING1));
    }

    @Test
    public void testCreateMeetingInvalidTimeSlot() {
        givenLaunched();
        givenServerSimulated();
        givenEvent2CreatedByTester1();
        whenTester1LoggedIn();
        whenCreateMeeting1Called();
        thenInvalidTimeSlotExceptionThrown();
        thenMeeting1NotCreated();
    }

    public void whenTester2LoggedIn() {
        try {
            ClientManager.getClientManager().login(TESTER2_USERNAME, TESTER2_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void whenUserAcceptsMeeting1() {
        try {
            ClientManager.getClientManager().acceptMeeting(MEETING1_ID);
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void thenTester2AcceptedMeeting1() {
        assert(
                TESTER2.getSchedule().getAcceptedEvents().contains(MEETING1) &&
                MEETING1.getAttendees().contains(TESTER2.getUsername()) &&
                MEETING1.getAccepted().get(MEETING1.getAttendees().indexOf(TESTER2.getUsername()))
        );
    }

    @Test
    public void testAcceptMeeting() {
        givenLaunched();
        givenServerSimulated();
        whenTester2LoggedIn();
        whenUserAcceptsMeeting1();
        thenTester2AcceptedMeeting1();
    }

    public void whenUserRejectsMeeting1() {
        try {
            ClientManager.getClientManager().rejectMeeting(MEETING1_ID);
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void thenTester2RejectedMeeting1() {
        assert(
                ! TESTER2.getSchedule().getAcceptedEvents().contains(MEETING1) &&
                ! MEETING1.getAttendees().contains(TESTER2.getUsername())
        );
    }

    @Test
    public void testRejectMeeting() {
        givenLaunched();
        givenServerSimulated();
        whenTester2LoggedIn();
        whenUserRejectsMeeting1();
        thenTester2RejectedMeeting1();
    }

    public void whenCheckStartTimeFormatCalledWithFutureTime() {
        Calendar futureTime = Calendar.getInstance();
        futureTime.add(Calendar.MINUTE, 30);
        correctFormat = ClientManager.getClientManager().checkStartTimeFormat(futureTime);
    }

    public void thenFormatCheckReturnedTrue() {
        assertTrue(correctFormat);
    }

    @Test
    public void testCheckStartTimeFormatCorrect() {
        givenLaunched();
        whenCheckStartTimeFormatCalledWithFutureTime();
        thenFormatCheckReturnedTrue();
    }

    public void whenCheckStartTimeFormatCalledWithPastTime() {
        Calendar pastTime = Calendar.getInstance();
        pastTime.add(Calendar.MINUTE, -30);
        correctFormat = ClientManager.getClientManager().checkStartTimeFormat(pastTime);
    }

    public void thenFormatCheckReturnedFalse() {
        assertFalse(correctFormat);
    }

    @Test
    public void testCheckStartTimeFormatIncorrect() {
        givenLaunched();
        whenCheckStartTimeFormatCalledWithPastTime();
        thenFormatCheckReturnedFalse();
    }

    public void whenCheckWindowFormatCalledWithGoodWindow() {
        Calendar windowStart = Calendar.getInstance();
        // add one minute so its not in the past by a few seconds
        windowStart.add(Calendar.MINUTE, 1);
        Calendar windowEnd = (Calendar) windowStart.clone();
        // add duration + one minute to assure it can fit the duration
        windowEnd.add(Calendar.MINUTE, TEST_WINDOW_DURATION + 1);
        correctFormat = ClientManager.getClientManager().checkWindowFormat(windowStart, windowEnd, TEST_WINDOW_DURATION);
    }

    @Test
    public void testCheckWindowFormatCorrect() {
        givenLaunched();
        whenCheckWindowFormatCalledWithGoodWindow();
        thenFormatCheckReturnedTrue();
    }

    public void whenCheckWindowFormatCalledWithBadWindow() {
        Calendar windowStart = Calendar.getInstance();
        // add one minute so its not in the past by a few seconds
        // startTimeFormat check is called on the start time
        windowStart.add(Calendar.MINUTE, 1);
        Calendar windowEnd = (Calendar) windowStart.clone();
        // add duration - one minute to assure it can't fit the duration
        windowEnd.add(Calendar.MINUTE, TEST_WINDOW_DURATION - 1);
        correctFormat = ClientManager.getClientManager().checkWindowFormat(windowStart, windowEnd, TEST_WINDOW_DURATION);
    }

    @Test
    public void testCheckWindowFormatIncorrect() {
        givenLaunched();
        whenCheckWindowFormatCalledWithBadWindow();
        thenFormatCheckReturnedFalse();
    }

    public void givenMeeting2IsToday() {
        Calendar newStartTime = Calendar.getInstance();
        newStartTime.set(Calendar.HOUR_OF_DAY, 10);
        MEETING2.setStartTime(newStartTime);
    }

    public void givenEvent2IsToday() {
        Calendar newStartTime = Calendar.getInstance();
        newStartTime.set(Calendar.HOUR_OF_DAY, 10+MEETING2_DURATION/60);
        EVENT2.setStartTime(newStartTime);
    }

    public void whenGetDayEventsCalled() {
        Calendar startOfToday = CalendarHelper.getStartOfDay(Calendar.getInstance());
        todaysEvents = ClientManager.getClientManager().getDayEvents(startOfToday);
    }

    public void thenResultContainsMeeting2andEvent2() {
        assertTrue(todaysEvents.contains(MEETING2));
        assertTrue(todaysEvents.contains(EVENT2));
    }

    @Test
    public void testGetDayEventsWith2Events() {
        givenLaunched();
        givenServerSimulated();
        givenMeeting2Created();
        givenMeeting2IsToday();
        givenEvent2CreatedByTester1();
        givenEvent2IsToday();
        whenTester1LoggedIn();
        whenGetDayEventsCalled();
        thenResultContainsMeeting2andEvent2();
    }

    public void thenResultIsEmpty() {
        assertTrue(todaysEvents.isEmpty());
    }

    @Test
    public void testGetDayEventWithNoEvents() {
        givenLaunched();
        givenServerSimulated();
        whenTester2LoggedIn();
        whenGetDayEventsCalled();
        thenResultIsEmpty();
    }

    public void whenRegisterUserTester3Called() {
        try {
            loggedIn = ClientManager.getClientManager().registerUser(
                    TESTER3_USERNAME,
                    TESTER3_PASSWORD,
                    TESTER3_FNAME,
                    TESTER3_LNAME,
                    TESTER3_EMAIL,
                    TESTER3_PHONE,
                    TESTER3_ROOM,
                    TESTER3_POSITION,
                    TESTER3_BIO
            );
        } catch (IOException | InvalidUsernameException  | LoginErrorException e) {
            exceptionThrown = e;
        }
    }


    public void thenTester3LoggedIn() {
        // assert the current user is now TESTER3
        assertEquals(TESTER3, ClientManager.getClientManager().getCurrentUser());
    }

    @Test
    public void testRegisterUserWithValidUsername() {
        givenLaunched();
        givenServerSimulated();
        givenTester3RegisterableInDatabase();
        whenRegisterUserTester3Called();
        thenTester3LoggedIn();
    }

    public void whenRegisterUserTester1Called() {
        try {
            loggedIn = ClientManager.getClientManager().registerUser(
                    TESTER1_USERNAME,
                    TESTER1_PASSWORD,
                    TESTER1_FNAME,
                    TESTER1_LNAME,
                    TESTER1_EMAIL,
                    TESTER1_PHONE,
                    TESTER1_ROOM,
                    TESTER1_POSITION,
                    TESTER1_BIO
            );
        } catch (IOException | InvalidUsernameException | LoginErrorException e) {
            exceptionThrown = e;
        }
    }

    public void thenInvalidUsernameExceptionThrown() {
        assertEquals(InvalidUsernameException.class, exceptionThrown.getClass());
        //assertFalse(loggedIn);
    }

    @Test
    public void testRegisterUser_InvalidUsername() {
        givenLaunched();
        givenServerSimulated();
        whenRegisterUserTester1Called();
        thenInvalidUsernameExceptionThrown();
    }

    public void whenUpdateEvent2Called_ValidTimeSlot() {
        try {
            EVENT2_NEW_START_TIME = Calendar.getInstance();
            EVENT2_NEW_START_TIME.setTime(DATE_FORMAT.parse(EVENT2_NEW_START_STRING));
            ClientManager.getClientManager().updateEvent(EVENT2_ID, EVENT2_NEW_NAME, EVENT2_NEW_LOCATION,
                    EVENT2_NEW_DESCRIPTION, EVENT2_NEW_DURATION, EVENT2_NEW_START_TIME);
        } catch (IOException | ParseException | InvalidTimeslotException e) {
            exceptionThrown = e;
        }
    }

    public void thenEvent2Updated() {
        Event updatedEvent2 = new Event(EVENT2_ID, false, EVENT2_NEW_NAME, EVENT2_NEW_LOCATION, EVENT2_NEW_DESCRIPTION,
                EVENT2_NEW_DURATION, EVENT2_NEW_START_TIME, TESTER1_USERNAME);
        assertTrue(TESTER1.getSchedule().getEventList().contains(updatedEvent2));
        /*try {
            Event event2 = TESTER1.getSchedule().getEvent(EVENT2_ID);
            assertEquals(EVENT2_NEW_NAME, event2.getName());
            assertEquals(EVENT2_NEW_LOCATION, event2.getLocation());
            assertEquals(EVENT2_NEW_DESCRIPTION, event2.getDescription());
            assertEquals(EVENT2_NEW_DURATION, event2.getDuration());
            assertEquals(EVENT2_NEW_START_TIME, event2.getStartTime());
        } catch (EventNotFoundException enfe) {
            fail("Event with EVENT2_ID : " + EVENT2_ID + " not found");
        }*/
    }

    @Test
    public void testUpdateEventValidTimeSlot() {
        givenLaunched();
        givenServerSimulated();
        givenEvent2CreatedByTester1();
        whenTester1LoggedIn();
        whenUpdateEvent2Called_ValidTimeSlot();
        thenEvent2Updated();
    }

    public void whenUpdateEvent2CalledWithInvalidTimeSlot() {
        try {
            ClientManager.getClientManager().updateEvent(EVENT2_ID, EVENT2_NEW_NAME, EVENT2_NEW_LOCATION,
                    EVENT2_NEW_DESCRIPTION, EVENT2_NEW_DURATION, MEETING2_START_TIME);
        } catch (IOException | InvalidTimeslotException e) {
            exceptionThrown = e;
        }
    }

    @Test
    public void testUpdateEventInvalidTimeSlot() {
        givenLaunched();
        givenServerSimulated();
        givenMeeting2Created();
        givenEvent2CreatedByTester1();
        whenTester1LoggedIn();
        whenUpdateEvent2CalledWithInvalidTimeSlot();
        thenInvalidTimeSlotExceptionThrown();
    }

    public void whenUpdateMeeting2Called() {
        try {
            ClientManager.getClientManager().updateMeeting(MEETING2_ID, MEETING2_NEW_NAME, MEETING2_NEW_LOCATION,
                    MEETING2_NEW_DESCRIPTION, MEETING2_DURATION, MEETING2_START_TIME, TESTER1_USERNAME,
                    MEETING2_NEW_ATTENDEES_1, MEETING2.getAccepted(), MEETING2.getComments());
        } catch (IOException ioe) {
            exceptionThrown = ioe;
        }
    }

    public void thenMeeting2Updated() {
        Meeting updatedMeeting2 = new Meeting(MEETING2_ID, true, MEETING2_NEW_NAME, MEETING2_NEW_LOCATION,
                MEETING2_NEW_DESCRIPTION, MEETING2_DURATION, MEETING2_START_TIME, TESTER1_USERNAME,
                MEETING2_NEW_ATTENDEES_1, MEETING2.getAccepted(), MEETING2.getComments());
        assertTrue(TESTER1.getSchedule().getEventList().contains(updatedMeeting2));
    }

    @Test
    public void testUpdateMeeting() {
        givenLaunched();
        givenServerSimulated();
        givenMeeting2Created();
        whenTester1LoggedIn();
        whenUpdateMeeting2Called();
        thenMeeting2Updated();
    }

    public void whenCheckUpdateMeeting2Called_NoUnavailableUsers() {
        try {
            userTriples = ClientManager.getClientManager().checkUpdateMeeting(MEETING2_NEW_ATTENDEES_1,
                    MEETING2_ATTENDEES, MEETING2_START_TIME, MEETING2_DURATION, MEETING2_ID);
        } catch (IOException ioe) {
            exceptionThrown = ioe;
        }
    }

    public void thenReturnedArrayListIsEmpty() {
        assertTrue(userTriples.isEmpty());
    }

    @Test
    public void testCheckUpdateMeeting_NoUnavailableUsers() {
        givenLaunched();
        givenServerSimulated();
        givenMeeting2Created();
        givenTester3Registered();
        whenTester1LoggedIn();
        whenCheckUpdateMeeting2Called_NoUnavailableUsers();
        thenReturnedArrayListIsEmpty();
    }

    public void whenCheckUpdateMeeting2Called_UnavailableUsers() {
        try {
            userTriples = ClientManager.getClientManager().checkUpdateMeeting(MEETING2_NEW_ATTENDEES_2,
                    MEETING2_ATTENDEES, MEETING2_START_TIME, MEETING2_DURATION, MEETING2_ID);
        } catch (IOException ioe) {
            exceptionThrown = ioe;
        }
    }

    public void thenReturnedArrayListContainsTester4Triple() {
        try {
            assertEquals(TESTER4_USERNAME, userTriples.get(0)[0]);
            assertEquals(TESTER4_FNAME, userTriples.get(0)[1]);
            assertEquals(TESTER4_LNAME, userTriples.get(0)[2]);
        } catch (IndexOutOfBoundsException e) {
            exceptionThrown = e;
            fail();
        }
    }

    @Test
    public void testCheckUpdateMeeting_UnavailableUsers() {
        givenLaunched();
        givenServerSimulated();
        givenMeeting2Created();
        givenEvent2CreatedByTester4();
        whenTester1LoggedIn();
        whenCheckUpdateMeeting2Called_UnavailableUsers();
        thenReturnedArrayListContainsTester4Triple();
    }

    @Test
    public void testDeleteMeeting() {
        givenLaunched();
        givenServerSimulated();
    }

    public void whenTester1CommentsOnMeeting2() {
        try {
            ClientManager.getClientManager().createComment(MEETING2_ID, COMMENT1_COMMENT);
        } catch (IOException ioe) {
            exceptionThrown = ioe;
        }
    }

    public void thenMeeting2ContainsTester1sComment() {
        assertTrue(MEETING2.getComments().contains(COMMENT1));
    }

    @Test
    public void testCreateComment() {
        givenLaunched();
        givenServerSimulated();
        givenMeeting2Created();
        whenTester1LoggedIn();
        whenTester1CommentsOnMeeting2();
        thenMeeting2ContainsTester1sComment();
    }

    public void whenGetOtherUserListCalled() {
        try {
            userTriples = ClientManager.getClientManager().getOtherUsersList();
        } catch (IOException ioe) {
            exceptionThrown = ioe;
        }
    }

    public void thenCorrectOtherUserListReturned() {
        ArrayList<String[]> expectedOtherUserList = new ArrayList<>();
        expectedOtherUserList.add(new String[] {TESTER2_USERNAME, TESTER2_FNAME, TESTER2_LNAME});
        expectedOtherUserList.add(new String[] {TESTER4_USERNAME, TESTER4_FNAME, TESTER4_LNAME});
        assertEquals(TESTER2_USERNAME, userTriples.get(0)[0]);
        assertEquals(TESTER2_FNAME, userTriples.get(0)[1]);
        assertEquals(TESTER2_LNAME, userTriples.get(0)[2]);
        assertEquals(TESTER4_USERNAME, userTriples.get(1)[0]);
        assertEquals(TESTER4_FNAME, userTriples.get(1)[1]);
        assertEquals(TESTER4_LNAME, userTriples.get(1)[2]);

    }

    @Test
    public void testGetOtherUsersList() {
        givenLaunched();
        givenServerSimulated();
        whenTester1LoggedIn();
        whenGetOtherUserListCalled();
        thenCorrectOtherUserListReturned();
    }

}