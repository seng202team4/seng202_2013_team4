package test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
  
@RunWith(Suite.class)
@Suite.SuiteClasses({
	ClientManagerTest.class,
	DatabaseTest.class,
	EventTest.class,
	RequestHandlerTest.class,
	ScheduleTest.class,
	SchedulingEngineTest.class,
	UserTest.class
	})


public class TestingSuite {
	
	@BeforeClass
	public static void printStart() {
		System.out.println("Staring tests");
	}
	
	@AfterClass
	public static void printEnd() {
		System.out.println("Finished tests");
	}
}
