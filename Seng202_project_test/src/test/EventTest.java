package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

import shared.Event;
import shared.Schedule;
import shared.User;
  
public class EventTest {
	User user;
	Event event;
	Calendar start;
	
	String username = "u0";
	String fname = "User";
	String lname = "Zero";
	String bio = "Blank";
	String position = "None";
	String room = "nowhere";
	String email = "nothing@nothing.com";
	String phone = "0270000000";
	
	String ename = "Event Zero";
	String desc = "Desc";
	String loc = "Loc";
	int len = 30;
	int ID = 3;
	
	@Before
	public void setup() {
		user = new User(username, fname, lname, bio, position, room,
				email, phone, new Schedule(new ArrayList<Event>(), new ArrayList<Boolean>()));
		start = Calendar.getInstance();
		event = new Event(ID, true, ename, loc, desc, len, start, username);
	}
	
	@Test
	public void testGetDescription() {
		assertEquals(event.getDescription(), desc);
	}
	
	@Test
	public void testGetDuration() {
		assertEquals(event.getDuration(), len);
	}
	
	@Test
	public void testGetID() {
		assertEquals(event.getID(), ID);
	}
	
	@Test
	public void testGetInitiator() {
		assertEquals(event.getInitiator(), username);
	}
	
	@Test
	public void testGetLocation() {
		assertEquals(event.getLocation(), loc);
	}
	
	@Test
	public void testGetName() {
		assertEquals(event.getName(), ename);
	}
	
	@Test
	public void testGetStartTime() {
		assertEquals(event.getStartTime(), start);
	}
	
	@Test
	public void testGetEndTime() {
		Calendar end = start;
		end.add(Calendar.MINUTE, len);
		assertEquals(event.getEndTime(), end);
	}
	
	@Test
	public void testSetDescription() {
		String desc2 = desc + " 2"; 
		event.setDescription(desc2);
		assertEquals(event.getDescription(), desc2);
	}
	
	@Test
	public void testSetLocation() {
		String loc2 = loc + " 2"; 
		event.setLocation(loc2);
		assertEquals(event.getLocation(), loc2);
	}
	
	@Test
	public void testSetName() {
		String name2 = ename + " 2"; 
		event.setName(name2);
		assertEquals(event.getName(), name2);
	}
	
	@Test
	public void testSet() {
		Calendar start2 = start;
		start2.add(Calendar.MINUTE, 15);
		event.setStartTime(start2);
		assertEquals(event.getStartTime(), start2);
	}
}
