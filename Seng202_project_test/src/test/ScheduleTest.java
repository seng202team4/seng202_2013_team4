package test;


import exceptions.EventNotFoundException;
import org.junit.Before;
import org.junit.Test;
import shared.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

@SuppressWarnings("unchecked")
public class ScheduleTest {

    public static Exception exceptionThrown;
    public static Event returnedEvent;
    public static ArrayList<Event> returnedEventList;
    public static ArrayList<Event> originalEventList;
    public static ArrayList<Boolean> originalAccepted;

    // Creating test user 1, TESTER1
    public static User TESTER1;
    public static String TESTER1_USERNAME = "Tester1";
    public static String TESTER1_FNAME ="Coco";
    public static String TESTER1_LNAME ="Buttersugar";
    public static String TESTER1_BIO = "I liek kittehs.";
    public static String TESTER1_POSITION = "SENG202 tutor";
    public static String TESTER1_ROOM = "The morgue";
    public static String TESTER1_EMAIL ="cocobutter@uclive.ac.nz";
    public static String TESTER1_PHONE = "+64 27 012 3456";

    public static String TESTER2_USERNAME = "Tester2";
    public static String TESTER3_USERNAME = "Tester3";
    public static String TESTER4_USERNAME = "Tester4";

    public static final String DATE_FORMAT_STRING = "EEE dd MMM HH:mm yyyy";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);

    public static int MEETING1_ID = 1;
    public static String MEETING1_NAME = "Test meeting2";
    public static String MEETING1_NEW_NAME = "Updated Test meeting2";
    public static String MEETING1_LOCATION = "The test dimension";
    public static String MEETING1_NEW_LOCATION = "Test dimension 2.0";
    public static String MEETING1_DESCRIPTION = "A multi-dimensional test meeting";
    public static String MEETING1_NEW_DESCRIPTION = "A maxi-dimensional updated test l33ting";
    public static int MEETING1_DURATION = 60;
    public static String MEETING1_START_STRING = "Mon 20 Oct 16:00 2013";
    public static Calendar MEETING1_START_TIME;
    public static ArrayList<String> MEETING1_ATTENDEES =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME));
    public static ArrayList<Boolean> MEETING1_ACCEPTANCE =
            new ArrayList<>(Arrays.asList(false));
    public static ArrayList<String> MEETING1_NEW_ATTENDEES_1 =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME, TESTER3_USERNAME));
    public static ArrayList<String> MEETING1_NEW_ATTENDEES_2 =
            new ArrayList<>(Arrays.asList(TESTER2_USERNAME, TESTER4_USERNAME));
    public static Meeting MEETING1;

    public static int EVENT1_ID = 2;
    public static String EVENT1_NAME = "Test clash with meeting";
    public static String EVENT1_NEW_NAME = "New clash test meeting ;)";
    public static String EVENT1_LOCATION = "My drink bottle";
    public static String EVENT1_NEW_LOCATION = "My lift+ can";
    public static String EVENT1_DESCRIPTION = "An event to test meeting 1 cannot be scheduled in a clash.";
    public static String EVENT1_NEW_DESCRIPTION = "A new updated event2, (daniels are not allowed).";
    public static int EVENT1_DURATION = 90;
    public static int EVENT1_NEW_DURATION = 60;
    public static String EVENT1_START_STRING = "Mon 20 Oct 08:00 2013";
    public static Calendar EVENT1_START_TIME;
    public static Event EVENT1;

    public static int MEETING2_ID = 3;
    public static String MEETING2_NAME = "Test meeting1";
    public static String MEETING2_LOCATION = "The test realm";
    public static String MEETING2_DESCRIPTION = "A test meeting.";
    public static int MEETING2_DURATION = 45;
    public static String MEETING2_START_STRING = "Mon 20 Oct 03:00 2013";
    public static Calendar MEETING2_START_TIME;
    public static ArrayList<String> MEETING2_ATTENDEES =
            new ArrayList<>(Arrays.asList(TESTER1_USERNAME));
    public static ArrayList<Boolean> MEETING2_ACCEPTANCE =
            new ArrayList<>(Arrays.asList(true));
    public static Meeting MEETING2;

    public static Schedule SCHEDULE1;

    public void givenSchedule1Created() {
        SCHEDULE1 = new Schedule(new ArrayList<Event>(), new ArrayList<Boolean>());
    }

    public void givenTester1Registered() {
        TESTER1 = new User(
                TESTER1_USERNAME,
                TESTER1_FNAME,
                TESTER1_LNAME,
                TESTER1_BIO,
                TESTER1_POSITION,
                TESTER1_ROOM,
                TESTER1_EMAIL,
                TESTER1_PHONE,
                SCHEDULE1
        );
    }

    
	public void givenMeeting1Created() {
        // create a calendar object to set the start Time
        try {
            MEETING1_START_TIME = Calendar.getInstance();
            MEETING1_START_TIME.setTime(DATE_FORMAT.parse(MEETING1_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the meeting object
        MEETING1 = new Meeting(
                MEETING1_ID,
                true,
                MEETING1_NAME,
                MEETING1_LOCATION,
                MEETING1_DESCRIPTION,
                MEETING1_DURATION,
                MEETING1_START_TIME,
                TESTER1_USERNAME,
                (ArrayList<String>)MEETING1_ATTENDEES.clone(),
                (ArrayList<Boolean>)MEETING1_ACCEPTANCE.clone()
        );
    }

    public void givenEvent1Created() {
        // create a calendar object to set the start Time
        try {
            EVENT1_START_TIME = Calendar.getInstance();
            EVENT1_START_TIME.setTime(DATE_FORMAT.parse(EVENT1_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the meeting object
        EVENT1 = new Event(
                EVENT1_ID,
                false,
                EVENT1_NAME,
                EVENT1_LOCATION,
                EVENT1_DESCRIPTION,
                EVENT1_DURATION,
                EVENT1_START_TIME,
                TESTER1_USERNAME
        );
    }

    public void givenMeeting2Created() {
        // create a calendar object to set the start Time
        try {
            MEETING2_START_TIME = Calendar.getInstance();
            MEETING2_START_TIME.setTime(DATE_FORMAT.parse(MEETING2_START_STRING));
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        // create the meeting object
        MEETING2 = new Meeting(
                MEETING2_ID,
                true,
                MEETING2_NAME,
                MEETING2_LOCATION,
                MEETING2_DESCRIPTION,
                MEETING2_DURATION,
                MEETING2_START_TIME,
                TESTER2_USERNAME,
                (ArrayList<String>)MEETING2_ATTENDEES.clone(),
                (ArrayList<Boolean>)MEETING2_ACCEPTANCE.clone()
        );
    }

    @Before
    public void givenSetUp() {
        exceptionThrown = null;
        returnedEvent = null;
        returnedEventList = null;
        originalAccepted = null;
        originalEventList = null;
        SCHEDULE1 = null;
        TESTER1 = null;
        MEETING1 = null;
        givenSchedule1Created();
        givenTester1Registered();
        givenMeeting1Created();
        givenEvent1Created();
        givenMeeting2Created();
    }

    public void whenAddEventCalled_WithMeeting1() {
        SCHEDULE1.addEvent(MEETING1, true);
    }

    public void whenGetEventCalled_WithMeeting1() {
        try {
            returnedEvent = SCHEDULE1.getEvent(MEETING1_ID);
        } catch (EventNotFoundException enfe) {
            exceptionThrown = enfe;
        }
    }

    public void thenEventReturnedIsMeeting1() {
        assertEquals(MEETING1, returnedEvent);
    }

    @Test
    public void testGetEvent_WithMeeting() {
        givenSetUp();
        whenAddEventCalled_WithMeeting1();
        whenGetEventCalled_WithMeeting1();
        thenEventReturnedIsMeeting1();
    }

    public void whenAddEventCalled_WithEvent1() {
        SCHEDULE1.addEvent(EVENT1, true);
    }

    public void whenGetEventCalled_WithEvent1() {
        try {
            returnedEvent = SCHEDULE1.getEvent(EVENT1_ID);
        } catch (EventNotFoundException enfe) {
            exceptionThrown = enfe;
        }
    }

    public void thenEventReturnedIsEvent1() {
        assertEquals(EVENT1, returnedEvent);
    }

    @Test
    public void testGetEvent_WithEvent() {
        givenSetUp();
        whenAddEventCalled_WithEvent1();
        whenGetEventCalled_WithEvent1();
        thenEventReturnedIsEvent1();
    }

    public void thenEventNotFoundExceptionThrown() {
        assertNull(returnedEvent);
        assertEquals(EventNotFoundException.class, exceptionThrown.getClass());
    }

    @Test
    public void testGetEvent_NotAddedEvent() {
        givenSetUp();
        whenGetEventCalled_WithEvent1();
        thenEventNotFoundExceptionThrown();
    }

    public void givenEvent1IsToday() {
        Calendar newStartTime = Calendar.getInstance();
        newStartTime.set(Calendar.HOUR_OF_DAY, 10);
        EVENT1.setStartTime(newStartTime);
    }

    public void givenMeeting1IsToday() {
        Calendar newStartTime = Calendar.getInstance();
        newStartTime.set(Calendar.HOUR_OF_DAY, 10+EVENT1_DURATION/60);
        MEETING1.setStartTime(newStartTime);
    }

    public void whenGetDayEventsCalled() {
        Calendar startOfDay = CalendarHelper.getStartOfDay(Calendar.getInstance());
        returnedEventList = SCHEDULE1.getDayEvents(startOfDay);
    }

    public void thenReturnedEventListContainsEvent1() {
        assertTrue(returnedEventList.contains(EVENT1));
    }

    public void thenReturnedEventListContainsMeeting1() {
        assertTrue(returnedEventList.contains(MEETING1));
    }

    @Test
    public void testGetDayEvents() {
        givenSetUp();
        givenEvent1Created();
        givenEvent1IsToday();
        whenAddEventCalled_WithEvent1();
        givenMeeting1Created();
        givenMeeting1IsToday();
        whenAddEventCalled_WithMeeting1();
        whenGetDayEventsCalled();
        thenReturnedEventListContainsMeeting1();
        thenReturnedEventListContainsEvent1();
    }

    public void whenAddEventCalled_WithMeeting2() {
        SCHEDULE1.addEvent(MEETING2, false);
    }

    public void whenGetOwnedEventsCalled() {
        returnedEventList = SCHEDULE1.getOwnedEvents(TESTER1_USERNAME);
    }

    public void thenReturnedEventListDoesntContainMeeting2() {
        assertFalse(returnedEventList.contains(MEETING2));
    }

    @Test
    public void testGetOwnedEvents() {
        givenSetUp();
        givenEvent1Created();
        whenAddEventCalled_WithEvent1();
        givenMeeting1Created();
        whenAddEventCalled_WithMeeting1();
        givenMeeting2Created();
        whenAddEventCalled_WithMeeting2();
        whenGetOwnedEventsCalled();
        thenReturnedEventListContainsEvent1();
        thenReturnedEventListContainsMeeting1();
        thenReturnedEventListDoesntContainMeeting2();
    }

    public void whenAcceptMeetingCalled_WithMeeting2() {
        SCHEDULE1.acceptMeeting(MEETING2_ID, TESTER1_USERNAME);
    }

    public void thenSchedule1AcceptanceListTrueForMeeting2() {
        int i = SCHEDULE1.getEventList().indexOf(MEETING2);
        assertTrue(SCHEDULE1.getAcceptanceList().get(i));
    }

    public void thenMeeting2HasTester1Accepted() {
        int i = MEETING2.getAttendees().indexOf(TESTER1_USERNAME);
        assertTrue(MEETING2.getAccepted().get(i));
    }

    public void thenMeeting2Accepted() {
        thenSchedule1AcceptanceListTrueForMeeting2();
        thenMeeting2HasTester1Accepted();
    }

    @Test
    public void testAcceptMeeting() {
        givenSetUp();
        givenMeeting2Created();
        whenAddEventCalled_WithMeeting2();
        whenAcceptMeetingCalled_WithMeeting2();
        thenMeeting2Accepted();
    }

    public void givenSchedule1OriginalStateStored() {
        originalAccepted = (ArrayList<Boolean>)SCHEDULE1.getAcceptanceList().clone();
        originalEventList = (ArrayList<Event>)SCHEDULE1.getEventList().clone();
    }

    public void whenRemoveEventCalled_WithMeeting2() {
        SCHEDULE1.removeEvent(MEETING2_ID, true, TESTER1_USERNAME);
    }

    public void thenSchedule1AcceptanceListLostMeeting2() {
        int i = originalEventList.indexOf(MEETING2);
        ArrayList<Boolean> expectedAcceptanceList = (ArrayList<Boolean>)originalAccepted.clone();
        expectedAcceptanceList.remove(i);
        assertEquals(expectedAcceptanceList, SCHEDULE1.getAcceptanceList());
        assertFalse(SCHEDULE1.getEventList().contains(MEETING2));
    }

    public void thenMeeting2RemovedFromEventList() {
        assertFalse(SCHEDULE1.getEventList().contains(MEETING2));
    }

    public void thenMeeting2HasTester1Removed() {
        assertFalse(MEETING2.getAttendees().contains(TESTER1_USERNAME));
    }

    public void thenMeeting2Removed() {
        thenSchedule1AcceptanceListLostMeeting2();
        thenMeeting2RemovedFromEventList();
        thenMeeting2HasTester1Removed();
    }

    @Test
    public void testRemoveEvent_Meeting() {
        givenSetUp();
        givenMeeting2Created();
        whenAddEventCalled_WithMeeting2();
        givenSchedule1OriginalStateStored();
        whenRemoveEventCalled_WithMeeting2();
        thenMeeting2Removed();
    }

    public void whenRemoveEventCalled_WithEvent1() {
        SCHEDULE1.removeEvent(EVENT1_ID, false, TESTER1_USERNAME);
    }

    public void thenEvent1Removed() {
        int i = originalEventList.indexOf(EVENT1);
        ArrayList<Boolean> expectedAcceptanceList = (ArrayList<Boolean>)originalAccepted.clone();
        expectedAcceptanceList.remove(i);
        assertEquals(expectedAcceptanceList, SCHEDULE1.getAcceptanceList());
        assertFalse(SCHEDULE1.getEventList().contains(EVENT1));
    }

    @Test
    public void testRemoveEvent_Event() {
        givenSetUp();
        givenEvent1Created();
        whenAddEventCalled_WithEvent1();
        givenSchedule1OriginalStateStored();
        whenRemoveEventCalled_WithEvent1();
        thenEvent1Removed();
    }

    public void thenSchedule1HasEvent1() {
        int i = SCHEDULE1.getEventList().indexOf(EVENT1);
        assertTrue(SCHEDULE1.getEventList().contains(EVENT1));
        assertTrue(SCHEDULE1.getAcceptanceList().get(i));
    }

    public void thenSchedule1HasMeeting1() {
        int i = SCHEDULE1.getEventList().indexOf(MEETING1);
        assertTrue(SCHEDULE1.getEventList().contains(MEETING1));
        assertTrue(SCHEDULE1.getAcceptanceList().get(i));
    }

    public void thenSchedule1HasMeeting2() {
        int i = SCHEDULE1.getEventList().indexOf(MEETING2);
        assertTrue(SCHEDULE1.getEventList().contains(MEETING2));
        assertFalse(SCHEDULE1.getAcceptanceList().get(i));
    }

    public void thenSchedule1OrderIsCorrect() {
        if(!SCHEDULE1.getEventList().isEmpty()) {
            Event moreRecentEvent = SCHEDULE1.getEventList().get(0);
            for(Event event : SCHEDULE1.getEventList()) {
                if(event != SCHEDULE1.getEventList().get(0)) {
                    assertFalse(moreRecentEvent.getStartTime().before(event));
                }
                moreRecentEvent = event;
            }
        }
    }

    public void thenSchedule1Correct() {
        thenSchedule1HasEvent1();
        thenSchedule1HasMeeting1();
        thenSchedule1HasMeeting2();
        thenSchedule1OrderIsCorrect();
    }

    @Test
    public void testAddEvent() {
        givenSetUp();
        givenEvent1Created();
        whenAddEventCalled_WithEvent1();
        whenAddEventCalled_WithMeeting1();
        whenAddEventCalled_WithMeeting2();
        thenSchedule1Correct();
    }

}
