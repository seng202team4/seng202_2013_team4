package client;

import com.alee.managers.notification.NotificationManager;
import exceptions.*;
import gui.login.LoginWindow;
import gui.login.RegisterWindow;
import gui.login.ServerWindow;
import gui.main.MainFrame;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

import shared.Comment;
import shared.Event;
import shared.Meeting;
import shared.Schedule;
import shared.User;
import updates.*;


/**
 * Controls the client application by getting information about the model from the server through the
 * ClientNetworkingManager, then calling the Gui to perform prompts / display views, the gui in turn
 * calls the clientManager with the users decision and the client manager executes the appropriate methods,
 * retrievals and updates.
 * @author Montgomery Anderson
 */
public class ClientManager implements Controller {

    // The currently logged in user.
    private static User currentUser;
    // The instance of the networkManager
    private static ClientNetworkingManager networkManager;
    // The static instance of the ClientManager, used in the singleton pattern*
    private static ClientManager clientManager;
    /* A reference to the main frame containing the GUI past the login stage, used when refreshing after updates have
     * been read */
    private static MainFrame mainFrame;
    // The last successfully connected port number of this client session
    private int previousPortNumber;
    // The last successfully connected IP address of the client in this session
    private String previousIP;
    
    private String previousUsername;
    
    private final String propertiesPath = "clientSettings/settings.properties";
    
    private String defaultProperties = "IP=127.0.0.1\r\nPort=12345";
    
    /**
     * Sets the networking manager, used explicitly for test purposes (mocking networking), other usage
     * can cause serious problems if executed in run-time.
     * @param clientNetworkingManager   the mock networkingManager to be used in testing
     */
    public static void setNetworkManager(ClientNetworkingManager clientNetworkingManager) {
        networkManager = clientNetworkingManager;
    }


    /**
     * Resets the currentUser, networkingManager and clientManager to null, essentially clearing all
     * clientData, explicitly used for test purposes to allow resetting of the singleton clientManager
     * and its associated networkingManager + logs out the user CLIENT-SIDE-ONLY, will cause problems
     * if executed in run-time.
     */
    public static void reset() {
        currentUser = null;
        networkManager = null;
        clientManager = null;
        mainFrame = null;
    }


    /**
     * Singleton pattern accessor for the ClientManager, creates an instance clientManager and assigns it to
     * the static identifier clientManager. If the instance already exists it returns it.
     * @return the singular instance of clientManager
     */
    public static ClientManager getClientManager() {
        if(clientManager == null) {
            return clientManager = new ClientManager();
        } else {
            return clientManager;
        }
    }


    /** Private constructor for ClientManager class. Prevents the constructor being called except for by
     * the singleton pattern accessor above.
     */
    private ClientManager() {
    	getSettings();
    }


    /**
     * Accessor method for the currently logged in user.
     * @return The currently logged in user, User object.
     */
    public User getCurrentUser() {
        return currentUser;
    }


    /**
     * Gets an ArrayList of events the current user is an initiator of.
     * @return an arrayList of initiated events by the current user.
     */
    public ArrayList<Event> getOwnedEventList() {
        return currentUser.getSchedule().getOwnedEvents(currentUser.getUsername());
    }


    /**
     * Takes the username and password of the desired user to be logged in as and attempts
     * to log into the server using these values.
     * @return true if successfully logged in, false otherwise.
     */
    public boolean login(String userName, char[] password) throws IOException, LoginErrorException {
        //connectToServer(serverIP, portNumber, serverPassword);
        currentUser = networkManager.login(userName, password);
        // return true if successfully logged in
        if( currentUser == null) {
            processUpdates();
            return false;
        // return false otherwise
        } else {
            processUpdates();
            writeUsername(userName);
            return true;
        }
    }


    /**
     * Logs the user out serverside then clientside.
     */
    public void logout() throws IOException {
        networkManager.logout(currentUser.getUsername()); 
        currentUser = null;
        mainFrame = null;
        processUpdates();
    }


    /**
     * Registers the user to the database with the given attributes.
     * @param username      desired username
     * @param password      desired password
     * @param firstName     their first name
     * @param lastName      their last name
     * @param email         their email
     * @param phone        their home phone number
     * @param room          their room location / office
     * @param position      their position in the company / business
     * @param bio           their biography / description / generic wall of text
     */
    public boolean registerUser(
            String username,
            char[] password,
            String firstName,
            String lastName,
            String email,
            String phone,
            String room,
            String position,
            String bio)
            throws IOException,
            InvalidUsernameException,
            LoginErrorException {
        // calls the networking manager to create the new user
        User newUser = new User(
                username,
                firstName,
                lastName,
                bio,
                position,
                room,
                email,
                phone,
                new Schedule(
                        new ArrayList<Event>(),
                        new ArrayList<Boolean>()
                )
        );
        User registeredUser = networkManager.registerUser(newUser, password);
        if(registeredUser != null) {
            boolean connected = login(username, password);
            processUpdates();
            return connected;
        } else {
            processUpdates();
            return false;
        }
    }


    /**
     * Checks if the startTime is not before the current system time, in which case it is an invalid format.
     * @param startTime     the starting time
     * @return              true if the starting time is equal to or after the current system time
     */
    public boolean checkStartTimeFormat(Calendar startTime) {
        return ! startTime.before(Calendar.getInstance());
    }


    /**
     * Checks that the time window is of an acceptable format to schedule a meeting in.
     * @param windowStart   the start of the time window as a calendar object
     * @param windowEnd     the end of the time window as a calendar object
     * @param duration      the duration of the meeting as an integer
     * @return              true if the time window is valid, false otherwise
     */
    public boolean checkWindowFormat(Calendar windowStart, Calendar windowEnd, int duration) {
        if(checkStartTimeFormat(windowStart)) {
            Calendar earliestWindowEnd = (Calendar) windowStart.clone();
            earliestWindowEnd.add(Calendar.MINUTE, duration);
            if(! windowEnd.before(earliestWindowEnd)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Called by the gui to create an event with the given attributes, calls the networking manager to request
     * event creation from the server / which checks for clashes, returning an exception in that situation.
     * @param name          the name of the event as a string
     * @param location      the location of the event as a string
     * @param description   the description of the event as a string
     * @param duration      the duration of the event in minutes as an integer
     * @param startTime     the starting time of the event as a calendar object
     * @throws IOException  when there is an error retrieving from / sending to the server
     * @throws InvalidTimeslotException      when the meeting event cannot be scheduled at that given time
     * generally due to a clash in event times with another event on the current users schedule.
     */
    public void createEvent(
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime)
            throws IOException, InvalidTimeslotException {
        // check if it can be scheduled at the given time, throw invalidTimeSlotException otherwise
        currentUser.getSchedule().checkEventForClash(-1, startTime, duration);
        // create the event server side
        Event updatedEvent = networkManager.requestEventCreation(
                name, location, description, duration, startTime, currentUser.getUsername()
        );
        // if successful then create it client side
        currentUser.getSchedule().addEvent(updatedEvent, true);
        // networking performed, check updates queue.
        processUpdates();
    }


    /**
     * Called by the gui to create a meeting with the given attributes, calls the networking manager to request
     * meeting creation from the server.
     * @param name          the name of the meeting as a string
     * @param location      the location of the meeting as a string
     * @param description   the description of the meeting as a string
     * @param duration      the duration of the meeting in minutes as an integer
     * @param startTime     the starting time of the meeting as a calendar object
     * @param attendees     the attendees as an arrayList of unique user names (strings)
     * @throws IOException  when there is an error retrieving from / sending to the server
     * @throws InvalidTimeslotException  when the meeting cannot be scheduled at the given time due to a
     * clash
     */
    public void createMeeting(
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime,
            ArrayList<String> attendees)
            throws IOException, InvalidTimeslotException {
        // remove the initiator from the attendees
        attendees.remove(currentUser.getUsername());
        // create the meeting server side
        Meeting updatedMeeting = networkManager.requestMeetingCreation(
                name, location, description, duration, startTime, currentUser.getUsername(), attendees
        );
        // if successful then create it client side
        currentUser.getSchedule().addEvent(updatedMeeting, true);
        // networking performed, check updates queue.
        processUpdates();
    }


    /**
     * Takes the unique ID of the meeting to be accepted and accepts it client side then server side.
     */
    public void acceptMeeting(int meetingID) throws IOException {
        currentUser.getSchedule().acceptMeeting(meetingID, currentUser.getUsername());
        networkManager.acceptMeeting(meetingID, currentUser.getUsername());
        // networking performed, check updates queue.
        processUpdates();
    }


    /**
     * Takes the unique ID of the meeting to be rejected and rejects it client side then server side.
     */
    public void rejectMeeting(int meetingID) throws IOException {
        currentUser.getSchedule().removeEvent(meetingID, true, currentUser.getUsername());
        networkManager.rejectMeeting(meetingID, currentUser.getUsername());
        // networking performed, check updates queue.
        processUpdates();
    }


    /**
     * Takes the attendees usernames and returns the triples of each of the attendees details, where each triple is
     * a string array of username, firstname and lastname.
     * @return an arrayList of string triples of {username, firstname, lastname} for each attendee.
     * @throws IOException if there was an error in networking
     */
    public ArrayList<String[]> getAttendeeTriples(ArrayList<String> attendees) throws IOException {
        ArrayList<String[]> attendeeTriples = new ArrayList<>();
        try {
            ArrayList<String[]> allUsersTriples = getUserList();
            for(String[] triple : allUsersTriples) {
                if(attendees.contains(triple[0])) {
                    attendeeTriples.add(triple);
                }
            }
            return attendeeTriples;
        } catch (IOException ioe) {
            processUpdates();
            throw ioe;
        }
    }


    /**
     * Updates an event details server side then client side if it was successful.
     * @param id the unique id of the event
     * @param name the updated name
     * @param location the updated location
     * @param description the updated description
     * @param duration the updated duration
     * @param startTime the updated starting time
     * @throws IOException if there was an error in networking
     * @throws InvalidTimeslotException if there was a clash in users schedule with another event
     */
    public void updateEvent(
            int id,
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime)
            throws IOException, InvalidTimeslotException {
        // check for a timeslot clash
        currentUser.getSchedule().checkEventForClash(id, startTime, duration);
        // creates an updated event and sends it to the server to update
        Event updatedEvent = new Event(id, false, name, location, description, duration, startTime,
                                       currentUser.getUsername());
        try {
            networkManager.updateEvent(updatedEvent);
            // if event was found and updated it will not of thrown an exception, so update client side
            currentUser.getSchedule().updateEvent(updatedEvent);
            processUpdates();
        } catch (EventNotFoundException enfe) {
            processUpdates();
        } catch (IOException ioe) {
            processUpdates();
            throw ioe;
        }
    }


    /**
     * Called by the gui in the edit meeting panel that works out which users are new and asks the server to
     * check which ones have conflicting schedules and cannot be added and returns an ArrayList of String arrays
     * where each string array contains one conflicting users username, firstname and lastname.
     * @param newAttendees the attendees list of the updated meeting
     * @param oldAttendees the attendees list of the out of date meeting
     * @param startTime the starting time of the event
     * @param duration the duration of the event in minutes
     * @return an ArrayList of String arrays
     * @throws IOException if there was an error in the networking
     */
    public ArrayList<String[]> checkUpdateMeeting(
            ArrayList<String> newAttendees,
            ArrayList<String> oldAttendees,
            Calendar startTime,
            int duration,
            int meetingID)
            throws IOException {

        ArrayList<String> addedAttendees = new ArrayList<>();
        // new attendees
        for(String newAttendee : newAttendees) {
            if(! oldAttendees.contains(newAttendee)) {
                addedAttendees.add(newAttendee);
            }
        }
        // get the attendees that cannot make it from the server
        try {
            ArrayList<String[]> unavailableAttendees = networkManager.checkAvailable(
                    addedAttendees, startTime, duration, meetingID);
            processUpdates();
            return unavailableAttendees;
        } catch (IOException ioe) {
            processUpdates();
            throw ioe;
        }
    }


    /**
     * Updates the event server side then client side, this method is primarily used to edit meetings in a generic way.
     * @param id updates ID, will generally stay the same
     * @param name updated name of meeting
     * @param location updated location of meeting
     * @param description updated description of the meeting
     * @param attendees updated attendees
     * @param accepted updated acceptance list
     * @param comments updated comments
     * @throws IOException thrown when error in networking
     */
    public void updateMeeting(
            int id,
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime,
            String initiator,
            ArrayList<String> attendees,
            ArrayList<Boolean> accepted,
            ArrayList<Comment> comments)
            throws IOException {
        // create updatedMeeting to send
        Meeting updatedMeeting = new Meeting(
                id, true, name, location, description, duration, startTime, initiator, attendees, accepted, comments);
        try {
            updatedMeeting = networkManager.updateMeeting(updatedMeeting);
            currentUser.getSchedule().updateMeeting(updatedMeeting);
            processUpdates();
        } catch (EventNotFoundException enfe) {
            processUpdates();
        } catch (IOException ioe) {
            processUpdates();
            throw ioe;
        }
    }


    /**
     * Deletes the event with the given event ID from the server then the client side.
     * @param eventID the unique ID of the event to be deleted
     * @param isMeeting a boolean determining if it is a meeting or not
     * @throws IOException if there was problems networking
     */
    public void deleteEvent(int eventID, boolean isMeeting) throws IOException {
        try {
            // pass it on to the server to delete
            networkManager.deleteEvent(eventID, isMeeting);
            currentUser.getSchedule().removeEvent(eventID, isMeeting, currentUser.getUsername());
            // networking was successfully performed, process updates
            processUpdates();
        } catch (IOException ioe) {
            // if networking failed, still need to process updates then throw caught IOException
            processUpdates();
            throw ioe;
        }
    }


    /**
     * Creates the comment on the meeting side then adds it on the client side.
     * @param meetingID the unique id of the meeting being commented on
     * @param commentText the text of the comment itself
     * @throws IOException if there is an error in networking
     */
    public void createComment(int meetingID, String commentText) throws IOException {
        Comment comment = new Comment(-1, meetingID, currentUser.getUsername(), Calendar.getInstance(), commentText);
        try {
            Comment updatedComment = networkManager.createComment(comment);
            currentUser.getSchedule().addComment(updatedComment);
            processUpdates();
        } catch (EventNotFoundException enfe) {
            processUpdates();
        } catch (IOException ioe) {
            processUpdates();
            throw ioe;
        }
    }


    /**
     * Displays the server login window, prompting for the Server address and port. Retrieves last
     * successfully connected server address and port from settings.properties and automatically tries
     * to connect to it.
     */
    public void startServerLoginWindow(Boolean autoConnect) {
        ServerWindow serverConnectWindow = new ServerWindow(autoConnect);
        serverConnectWindow.setVisible(true);
    }


    /**
     * Displays the login window, prompting for the username and password to connect to the server with
     * by calling login() or allows the option of creating / registering a new user on the server.
     */
    public void startLoginWindow() {
        if( currentUser==null ) {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.setVisible(true);
        }
    }


    /**
     * Displays all the fields required to register a new user, called by the login window when the user
     * presses the 'new user' button.
     */
    public void startRegisterWindow() {
    	RegisterWindow registerWindow = new RegisterWindow();
    	registerWindow.setVisible(true);
    }


    /**
     * Starts the application window by calling the GUI to display the mainFrame.
     */
    public void startApplicationWindow() {
        mainFrame = new MainFrame();
        mainFrame.setVisible(true);
    }


    /**
     * Gets all events in the current day given for the currently logged in user.
     * @param date the start of the day as a calendar object.
     * @return an ArrayList of events for the given day.
     */
	public ArrayList<Event> getDayEvents(Calendar date) {
		ArrayList<Event> daysEvents = currentUser.getSchedule().getDayEvents(date);
        return daysEvents;
	}


    /**
     * Gets and returns an ArrayList of String triples, where each triple represents one user, storing their username,
     * then firstname and finally lastname.
     * @return ArrayList of string triples : (username, firstname, lastname)
     * @throws IOException if there was an error during networking / fetching from the server
     */
    public ArrayList<String[]> getUserList() throws IOException {
        ArrayList<String[]> userList = networkManager.getUserList();
        processUpdates();
        return userList;
    }


    /**
     * Gets and returns an ArrayList of String triples, where each triple represents one user, storing their username,
     * then firstname and finally lastname. Excludes the currently logged in user.
     * @return ArrayList of string triples : (username, firstname, lastname) excludes currentUser
     * @throws IOException if there was an error during networking / fetching from the server
     */
    public ArrayList<String[]> getOtherUsersList() throws IOException {
        ArrayList<String[]> otherUsersList = getUserList();
        for(int i = 0; i < otherUsersList.size(); i++) {
        	if(otherUsersList.get(i)[0].equals(currentUser.getUsername())) {
        		otherUsersList.remove(i);
        	}
        }
        return otherUsersList;
    }


    /**
     * Gets an array list of integers from the server representing how many attendees could not go to the given meeting
     * if it was scheduled to start at a given timeslot, where timeslots start at the window start and go up to
     * in windowEnd - duration in intervals of Schedule.TIME_SLOT_LENGTH (currently equal to 5(mins)).
     * @param windowStart the earliest time for the meeting to start
     * @param windowEnd the latest time for the meeting to end
     * @param duration the duration of the meeting
     * @param attendees the attendees as an arrayList of unique usernames
     * @return the arrayList of integers representing how many people can't attend per timeslot
     * @throws IOException if an error occurred in networking
     */
    public ArrayList<ArrayList<String[]>> getAttendanceList(
            Calendar windowStart,
            Calendar windowEnd,
            int duration,
            ArrayList<String> attendees)
            throws IOException {
        ArrayList<ArrayList<String[]>> attendanceList = new ArrayList<>();
        try {
            attendees.add(currentUser.getUsername());
            attendanceList = networkManager.getAttendanceList(windowStart, windowEnd, duration, attendees);
            processUpdates();
        } catch (UserNotFoundException unfe) {
            processUpdates();
        } 
        return attendanceList;
    }


    /**
     * Updates the current users user details to the given details.
     * @param password the updated password
     * @param firstName the updated first name
     * @param lastName the updated last name
     * @param bio the updated biography
     * @param position the updated position
     * @param room the updated room
     * @param email the updated email address
     * @param phone the updated phone number
     * @throws IOException if there was an error in networking
     */
    public void updateUser(
            char[] password,
            String firstName,
            String lastName,
            String bio,
            String position,
            String room,
            String email,
            String phone)
            throws IOException {
        try {
            currentUser = networkManager.updateUser(
                    new User( currentUser.getUsername(), firstName, lastName, bio,
                              position, room, email, phone, currentUser.getSchedule()),
                    password);
            // no IOException thrown, networking was done, process updates
            processUpdates();
        } catch (IOException ioe) { // if an IOException was thrown, we must still process updates
            processUpdates();
            throw ioe;
        }
    }


    /**
     * Gets the updates arrayList of updates from the networking manager and calls process update on each one
     * individually if there are any at all.
     */
    public void processUpdates() {
        ArrayList<Update> updates = networkManager.passUpdates(); // retrieve updates while transmitting
        networkManager.flushUpdates(); // clear the updates list
        networkManager.peekUpdates();
        if(! updates.isEmpty()) {
            for(Update update : updates) {
                processUpdate(update);
            }
        }
        if (mainFrame != null) {
            mainFrame.refresh();
        }
    }


    /**
     * Processes an update for the client to maintain live concurrency with the server.
     * @param update the super-classified update.
     */
    public void processUpdate(Update update) {
        switch (update.command)
        {
            case MEETING_UPDATE :
                ChangedMeetingUpdate changedMeetingUpdate = (ChangedMeetingUpdate) update;
                Meeting updatedMeeting = changedMeetingUpdate.meeting;
                NotificationManager.showNotification("Meeting updated");
                currentUser.getSchedule().updateMeeting(updatedMeeting);
                break;
            case NEW_MEETING :
                NewMeetingUpdate newMeetingUpdate = (NewMeetingUpdate) update;
                Meeting newMeeting = newMeetingUpdate.meeting;
                NotificationManager.showNotification("New meeting invitation");
                currentUser.getSchedule().addEvent(newMeeting, false);
                break;
            case NEW_COMMENT :
                NewCommentUpdate newCommentUpdate = (NewCommentUpdate) update;
                Comment newComment = newCommentUpdate.comment;
                NotificationManager.showNotification("New comment");
                currentUser.getSchedule().addComment(newComment);
                break;
            case REMOVE_MEETING :
                RemoveMeetingUpdate removeMeetingUpdate = (RemoveMeetingUpdate) update;
                NotificationManager.showNotification("Meeting deleted");
                currentUser.getSchedule().removeEvent(removeMeetingUpdate.meetingID, true, currentUser.getUsername());
                break;
        }
    }


    /**
     * Calls the network manager to connect to the server.
     *
     * @param serverAddress the address / IP of the server e.g. 192.168.0.2 or 127.0.0.1 or localhost
     * @param portNumber the port of the server from 0 to 65535
     * @return true if the connection was established, false otherwise.
     */
    public void connectToServer(String serverAddress, int portNumber) throws IOException {
        boolean changed = true;
        if(networkManager != null) {
            if(networkManager.getAddress() == serverAddress && networkManager.getPortNumber() == portNumber) {
                changed = false;
            }
        }
        networkManager = new ClientNetworkingManager();
        networkManager.launch(portNumber, serverAddress);
        // if it made it this far w/o throwing exception, then connected, so we store the IP and port
        if(changed) {
        	previousIP = serverAddress;
        	previousPortNumber = portNumber;
            writeIPandPort(serverAddress, portNumber);
        }
        // networking performed, check updates queue.
        processUpdates();
    }


    /**
     * Loads previous settings from settings.properties file
     * @throws IOException 
     */
    public void getSettings() {
		try {
			FileInputStream file = new FileInputStream(propertiesPath);
			Properties prop = new Properties();
			prop.load(file);
			file.close();
			previousPortNumber = Integer.parseInt(prop.getProperty("Port"));
			previousIP = prop.getProperty("IP");
			previousUsername = prop.getProperty("Username");
		} catch (IOException e) {
			try {
				createDefaultSettings();
			} catch (IOException e1) {
				
			}
			previousPortNumber = 55555;
			previousIP = "127.0.0.1";
		}
    }
    
    
    /**
     * Creates a new settings.properties file if it cannot be found
     * @throws IOException
     */
    public void createDefaultSettings() throws IOException {
		File newDir = new File("clientSettings");
		newDir.mkdirs(); 
		File newFile = new File(propertiesPath);
		newFile.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(propertiesPath));
		writer.write(defaultProperties);
		writer.close();
		
    }
    
    
    /**
     * Gets the previously used IP that gave a successful connection.
     * @return the last IP that gave a successful connection.
     * @throws IOException if an error occurred reading the file settings.properties.
     */
    public String getPreviousIP () {
    	return previousIP;
    }
    
    
    /**
     * Gets the previously used port that gave a successful connection.
     * @return the last port that gave a successful connection.
     * @throws IOException if an error occurred reading the file settings.properties.
     */
    public String getPreviousPort() {
        return Integer.toString(previousPortNumber);
    }
    
    
    /**
     * Returns the username that was alst used to login.
     * @return
     */
    public String getPreviousUsername() {
    	return previousUsername;
    }
    
    
    /**
     * Writes the username that was just used to successfully log into
     * the server to the .properties fie
     * @param username
     */
    public void writeUsername(String username) {
    	try {
    		Properties newProperties = new Properties();
			newProperties.setProperty("IP", previousIP);
			newProperties.setProperty("Port", Integer.toString(previousPortNumber));
			newProperties.setProperty("Username", username);
			File propertiesFile = new File(propertiesPath);
			OutputStream out = new FileOutputStream(propertiesFile);
			newProperties.store(out, "This is an optional header comment string");
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    }
    
    
    /**
     * writes the last successfully used IP and port to the settings.properties file.
     * @param IP last successfully used IP as a string e.g. "192.168.0.2"
     * @param port last successfully used port as an int e.g. 12345
     * @throws IOException if an error occurred writing to the file settings.properties.
     */
    public void writeIPandPort(String IP, int port) {
	     try {
	         Properties newProperties = new Properties();
	 		 newProperties.setProperty("IP", IP);
	 		 newProperties.setProperty("Port", Integer.toString(port));
	 		 File propertiesFile = new File(propertiesPath);
	 	     OutputStream out = new FileOutputStream(propertiesFile);
			newProperties.store(out, "This is an optional header comment string");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }


    /**
     * Accessor method for the currently set address in the networking manager.
     * @return the currently set address in the networking manager.
     */
    public static String getCurrentIP() {
        return networkManager.getAddress();
    }


    /**
     * Accessor method for the currently set port in the networking manager.
     * @return the currently set port in the networking manager.
     */
    public static int getCurrentPort() {
        return networkManager.getPortNumber();
    }

}
