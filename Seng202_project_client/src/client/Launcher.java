package client;

/**
 * The launching class for our client. Using the singleton pattern, instantiates the clientManager and calls it to
 * create the first GUI window, the serverLoginWindow.
 * @author Montgomery Anderson
 */
public class Launcher {

    public static void main(String[] args) {
        ClientManager.getClientManager().startServerLoginWindow(true);
    }

}
