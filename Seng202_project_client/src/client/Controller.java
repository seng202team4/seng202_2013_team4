package client;

import exceptions.InvalidTimeslotException;
import exceptions.InvalidUsernameException;
import exceptions.LoginErrorException;
import shared.Event;
import shared.User;

import java.io.IOException;
import java.util.Calendar;
import java.util.ArrayList;

/**
 * @TODO make controller interface relevant and rename it to something more specific
 * Controller interface, allows the view to access the controller without being dependant on it.
 * @author Montgomery Anderson
 */
public interface Controller {

    /**
     * Logs the user in.
     * @param username the unique username
     * @param password the users password
     * @throws IOException when there is a problem networking
     * @throws LoginErrorException when the details are incorrect
     */
    public boolean login(String username, char[] password) throws IOException, LoginErrorException;

    /**
     * logs the user out
     * @throws IOException  when failed
     */
    public void logout() throws IOException;

    /**
     * Accessor method for the currently logged in user.
     * @return the currently logged in user
     */
    public User getCurrentUser();

    /**
     * Gets all the events the user is the initiator of.
     * @return an ArrayList of all the events with the user as initiator
     */
    public ArrayList<Event> getOwnedEventList();

    /**
     * Registers a new user and logs them in after successfully registering.
     * @param username the desired username
     * @param password the desired password
     * @param firstName their first name
     * @param lastName their last name
     * @param email their email address
     * @param phone their phone number
     * @param room their room in the office
     * @param position their position in the business
     * @param bio a short biography of them
     * @return A boolean if they logged in successfully or not (should return true in all use cases)
     * @throws IOException if there was an error in networking
     * @throws InvalidUsernameException if the username was already take (it must be unique)
     * @throws LoginErrorException if their login details were incorrect (shouldn't throw this in any use case)
     */
    public boolean registerUser(
            String username,
            char[] password,
            String firstName,
            String lastName,
            String email,
            String phone,
            String room,
            String position,
            String bio)
            throws IOException,
            InvalidUsernameException,
            LoginErrorException;

    /**
     * Creates a new event with the given details.
     * @param name the name of the event
     * @param location the location of the event
     * @param description the description of what the event is for
     * @param duration the duration of it in minutes
     * @param startTime the starting time of the event
     * @throws IOException if there was an error in networking
     * @throws InvalidTimeslotException if there is a clash with another event in the users timetable
     */
    public void createEvent(
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime)
            throws IOException,
            InvalidTimeslotException;

    /**
     * Creates a new meeting with the given details
     * @param name the name of the meeting
     * @param location the location of the meeting
     * @param description the description of what the meeting is about
     * @param duration the duration of the meeting in minutes
     * @param startTime the starting time of the meeting
     * @param attendees an arrayList of the attendees usernames
     * @throws IOException if there was an error in networking
     * @throws InvalidTimeslotException if there was a clash with the timeslot (shouldn't happen in any use case)
     */
    public void createMeeting(
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime,
            ArrayList<String> attendees)
            throws IOException,
            InvalidTimeslotException;

    /**
     * Accepts the meeting as the current user.
     * @param meetingID the unique id of the meeting
     * @throws IOException if there was an error in networking
     */
    public void acceptMeeting(int meetingID) throws IOException;

    /**
     * Rejects the meeting as the current user.
     * @param meetingID the unique if of the meeting
     * @throws IOException if there was an error in networking
     */
    public void rejectMeeting(int meetingID) throws IOException;

    /**
     * Updates the user details on the server side then the client side.
     * @param password the updated password
     * @param firstName the updated first name
     * @param lastName the updated last name
     * @param bio the updated biography
     * @param position the updated position in the company
     * @param room the updated room location
     * @param email the updated email address
     * @param phone the updated phone number
     * @throws IOException if there was an error in networking
     */
    public void updateUser(
            char[] password,
            String firstName,
            String lastName,
            String bio,
            String position,
            String room,
            String email,
            String phone)
            throws IOException;

    /**
     * Connects to the server.
     * @param serverIP the IP address for the server
     * @param portNumber the port to connect to the server through
     * @throws IOException
     */
    public void connectToServer(String serverIP, int portNumber) throws IOException;

    /**
     * Starts up the GUI's login window
     */
    public void startLoginWindow();

}
