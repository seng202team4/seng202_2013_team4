package client;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;

import shared.*;
import requests.*;
import exceptions.*;
import updates.Update;

/**
 * Controls communication between server and client on the client side. Takes required information
 * from the clientManager who calls it exclusively, creates appropriate networking objects and sends
 * them to the server.
 * @author Montgomery Anderson
 * @author Daniel Hope
 */
public class ClientNetworkingManager {

    // the port used by the server
    private int portNumber;
    // the IP address used to connect to the server
    private String address;
    // the input stream
    private InputStream inputStream;
    // the output stream
    private OutputStream outputStream;
    // the object input stream used to receive objects from the server
    private ObjectInputStream objectInputStream;
    // the object output stream used to send objects to the server
    private ObjectOutputStream objectOutputStream;
    // the clients socket it uses
    private Socket socket;
    // an arrayList of updates stored while other networking tasks were being performed
    private ArrayList<Update> updates = new ArrayList<>();


    /**
     * Setter for the input stream used by the networking manager.
     * @param inputStream the input stream the networking manager desired to use
     */
    public void setInputStream(ObjectInputStream inputStream) {
        this.inputStream = inputStream;
    }


    /**
     * Setter for the output stream used by the networking manager.
     * @param outputStream the output stream the networking manager desired to use
     */
    public void setOutputStream(ObjectOutputStream outputStream) {
        this.outputStream = outputStream;
    }


    /**
     * Accessor method for the update arrayList.
     * @return the arrayList contain all updates stored during the previous networking task
     */
    public ArrayList<Update> passUpdates() {
        return updates;
    }


    /**
     * Clears the update list, called by the clientManager once it has finished processing the updates.
     */
    public void flushUpdates() {
        updates = new ArrayList<>();
    }


    /**
     * Accessor method for the IP address being used to connect to the server.
     * @return the IP address used to connect to the server
     */
    public String getAddress() {
        return address;
    }


    /**
     * Accessor method for the port number being used to connect to the server.
     * @return the port number used to connect to the server
     */
    public int getPortNumber() {
        return portNumber;
    }


    /**
     * Reads the next object in a safe manner, storing any updates in the update stream before the first non-update
     * object, then returning the first read object that is not an update.
     * @return the first object in the object input stream that is not an update
     * @throws IOException if there was an error in reading from the stream
     */
    public Object getNext() throws IOException {
        while(true) {
            try {
                Object readObject = objectInputStream.readObject();
                try {
                    Update update = (Update) readObject;
                    updates.add(update);
                } catch(ClassCastException cce) {
                    return readObject;
                }
            } catch(ClassNotFoundException cnfe) {
                System.err.println("ERROR: ClassNotFoundException thrown reading object from input stream.");
                cnfe.printStackTrace();
            }
        }
    }


    public void peekUpdates() {
        // TODO
    }


    /**
     * Logs the given user with the passed in username in assuming the password given is correct. Server side will
     * check that the username and password are a correct pair in which case it returns the user object otherwise it
     * throws a login error exception.
     * @param username the username of the user to login
     * @param password the users password as a character array
     * @return the user object if successfully logged in
     * @throws IOException if there was an error transmitting or reading over the object streams
     * @throws LoginErrorException if the username and password were not a match in the database
     */
    public User login(String username, char[] password) throws IOException, LoginErrorException {
        LoginReq loginRequest = new LoginReq(username, password);
        objectOutputStream.writeObject(loginRequest);
        objectOutputStream.flush();
        Object objectRead = getNext();
        if(objectRead.getClass().equals(User.class)) {
            return (User) objectRead;
        } else if(objectRead.getClass().equals(ServerError.class)) {
            // WARNING INCOMING BEAUTIFUL CODE ! ! ! //
            throw (LoginErrorException)((ServerError)objectRead).exception;
        } else {
            throw new IOException();
        }
    }


    /**
     * Logs the user out, essentially informing the server that it no longer needs to send updates to the thread that
     * this client is on.
     * @param username the username of the currentUser logged in to log out
     * @throws IOException if there was an error transmitting over the object stream
     */
    public void logout(String username) throws IOException {
        LogoutReq logoutRequest = new LogoutReq(username);
        objectOutputStream.writeObject(logoutRequest);
        objectOutputStream.flush();
    }


    /**
     * Retrieves the user object of the user with the given username from the server.
     * @param username the unique username
     * @return the object of the user with the unique username
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public User getUser(String username) throws IOException {
        GetUserReq request = new GetUserReq(username);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        User requestedUser = (User) getNext();
        return requestedUser;
    }

    /**
     * Gets an arrayList of triples, one triple for every registered user, where each triple contains the username,
     * firstname and lastname of the associated triple. e.g. {"Tester1","Coco","Buttersugar"}
     * @return an arrayList of triples containing the details of every registered user.
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    @SuppressWarnings("unchecked")
	public ArrayList<String[]> getUserList() throws IOException {
        GetAllUsersReq request = new GetAllUsersReq();
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass().equals(ArrayList.class)) {
            return (ArrayList<String[]>) readObject;
        } else {
            throw new IOException();
        }
    }


    /**
     * Registers a user with the given details passed in as newUser, assigning it a unique ID and registers the given
     * password to the newUser in the database.
     * @param newUser the user details as a user object
     * @param password the users desired password
     * @return the newly registered user object
     * @throws IOException if there was an error transmitting or receiving over the object streams
     * @throws InvalidUsernameException if the username has already been taken
     */
    public User registerUser(User newUser, char[] password) throws IOException, InvalidUsernameException {
        CreateUserReq request = new CreateUserReq(newUser, password);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass().equals(User.class)) {
            return (User) readObject;
        } else if(readObject.getClass().equals(ServerError.class)) {
            throw (InvalidUsernameException)((ServerError)readObject).exception;
        } else {
            throw new IOException();
        }
    }


    /**
     * Launches the networkingManager by setting the port number and server address variables and then calling
     * connect() to establish a connection.
     * @param portNumber the port number to connect to the server through
     * @param serverAddress the IP address to connect to the server through
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public void launch(int portNumber, String serverAddress) throws IOException {
        this.portNumber = portNumber;
        this.address = serverAddress;
        connect();
    }


    /**
     * Establishes a connection to the server, by creating the socket with the given address and port and instantiating
     * the output and input streams.
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public void connect() throws IOException {
        socket = new Socket(address, portNumber);
        outputStream = socket.getOutputStream();
        objectOutputStream = new ObjectOutputStream(outputStream);
        inputStream = socket.getInputStream();
        objectInputStream = new ObjectInputStream(inputStream);
    }


    /**
     * Sends a request to the scheduling engine for the number of users busy between two time intervals
     * @param windowStart the start of the period you want the meeting scheduled in
     * @param windowEnd the latest time of the period you want the meeting scheduled in
     * @param duration the duration of the meeting as an int representing minutes
     * @return An arrayList of integers
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    @SuppressWarnings("unchecked")
	public ArrayList<ArrayList<String[]>> getAttendanceList(
            Calendar windowStart,
            Calendar windowEnd,
            int duration,
            ArrayList<String> attendees) throws IOException, UserNotFoundException {
        ScheduleMeetingReq request = new ScheduleMeetingReq(windowStart, windowEnd, duration, attendees);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass().equals(ArrayList.class)) {
            return (ArrayList<ArrayList<String[]>>) readObject;
        } else if(readObject.getClass().equals(ServerError.class)) {
            throw (UserNotFoundException)((ServerError)readObject).exception;
        } else {
            throw new IOException();
        }
    }


    /**
     * ends a request to the server for a meeting to be created in the database
     * @param name the name of the meeting
     * @param location the location of the meeting
     * @param description the description of the meeting
     * @param duration the duration of the meeting
     * @param startTime the starting time of the meeting
     * @param initiator the meeting initiator
     * @param attendees the attendees an arrayList of usernames
     * @return the created meeting
     * @throws IOException if there was an error transmitting or receiving over the object streams
     * @throws InvalidTimeslotException if the meeting could not be created due to scheduling problems
     */
    public Meeting requestMeetingCreation(
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime,
            String initiator,
            ArrayList<String> attendees) throws IOException, InvalidTimeslotException {
        // create an accepted array
        ArrayList<Boolean> accepted = new ArrayList<>();
        for(int i = 0; i < attendees.size(); i++) {
            accepted.add(false);
        }
        // create the new meeting request
        CreateMeetingReq request = new CreateMeetingReq(
                new Meeting(
                        -1, true, name, location, description, duration, startTime, initiator,
                        attendees, accepted
                )
        );
        // send the meeting request
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        // get the reply
        Object readObject = getNext();
        if(readObject.getClass().equals(Meeting.class)) {
            return (Meeting) readObject;
        } else if(readObject.getClass().equals(ServerError.class)) {
            throw (InvalidTimeslotException)((ServerError)readObject).exception;
        } else {
            throw new IOException();
        }
    }


    /**
     * Sends a request to the server for creating a new personal event.
     * @param name the name of the event
     * @param location the location of the event
     * @param description the description of the event
     * @param duration the duration of the event in minutes as an int
     * @param startTime the start time of the event
     * @param initiator the initiator, currently in all cases is passed in staticallyy as the currently
     *                  logged in user
     * @return the created event
     * @throws IOException if there was an error transmitting or receiving over the object streams
     * @throws InvalidTimeslotException if there was a clash with the users schedule
     */
    public Event requestEventCreation(
            String name,
            String location,
            String description,
            int duration,
            Calendar startTime,
            String initiator) throws IOException, InvalidTimeslotException {
        // create the event request
        CreateEventReq request = new CreateEventReq(
                new Event(-1, false, name, location, description, duration, startTime, initiator
                )
        );
        // send the event request
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        // get the reply
        Object readObject = getNext();
        if(readObject.getClass().equals(Event.class)) {
            return (Event) readObject;
        } else if(readObject.getClass().equals(ServerError.class)) {
            throw (InvalidTimeslotException)((ServerError)readObject).exception;
        } else {
            throw new IOException();
        }
    }

    /**
     * Updates the event with the given id's details.
     * @param id the id of the event to have its details updated
     * @param name the updated name of the event
     * @param location the updated location
     * @param description the updated description
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public void updateEventDetails(int id, String name, String location, String description) throws IOException {
        UpdateEventDetailsReq request = new UpdateEventDetailsReq(id, name, location, description);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
    }


    /**
     * Note: not a feature planned for release
     * Updates the meetings initiator / switch the current initiator with the attendee with the
     * given username
     * @param id the id of the meeting whose initiator is being changed
     * @param oldInitiator the username of the old initiator
     * @param newInitiator the username of the new initiator
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public void updateInitiator(int id, String oldInitiator, String newInitiator) throws IOException {
        UpdateInitiatorReq request = new UpdateInitiatorReq(id, oldInitiator, newInitiator);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
    }


    /**
     * Updates the user details including password on the server side then on the client side.
     * @param updatedUser the updated user object containing all the up to date details
     * @param updatedPassword the new password if changed, otherwise will be the same password
     * @return the updated user object once the server has updated the user
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public User updateUser(User updatedUser, char[] updatedPassword) throws IOException {
        UpdateUserReq request = new UpdateUserReq(updatedUser, updatedPassword);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass().equals(User.class)) {
            return (User) readObject;
        } else {
            throw new IOException();
        }
    }


    /**
     * Sends a Meeting class to the server so the database can be updated and all invited users can be notified.
     * @param updatedMeeting the updated meeting class
     * @return the updated meeting registered on the server (circumstantially necessary, prevents de-synchronisation)
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public Meeting updateMeeting(Meeting updatedMeeting) throws IOException, EventNotFoundException {
        UpdateMeetingReq request = new UpdateMeetingReq(updatedMeeting);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass().equals(Meeting.class)) {
            return (Meeting) readObject;
        } else if(readObject.getClass().equals(ServerError.class)) {
            throw (EventNotFoundException)((ServerError)readObject).exception;
        } else {
            throw new IOException();
        }
    }


    /**
     * Sends an Event class to the server so the database can be updated to match the new settings.
     * @param updatedEvent the updated event object
     * @return the updated event registered on the server (circumstantially necessary, prevents de-synchronisation)
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public Event updateEvent(Event updatedEvent) throws IOException, EventNotFoundException {
        UpdateEventReq request = new UpdateEventReq(updatedEvent);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass().equals(Event.class)) {
            return (Event) readObject;
        } else if(readObject.getClass().equals(ServerError.class)) {
            throw (EventNotFoundException)((ServerError)readObject).exception;
        } else {
            throw new IOException();
        }
    }


    /**
     * Sends a request to the server for the event to be deleted and updates all associated users of the deletion.
     * @param eventID the unique ID of the event to be deleted
     * @param isMeeting a boolean telling whether or not the event is sub-specified into a meeting
     * @throws IOException if there was an error transmitting or receiving over the object stream
     */
    public void deleteEvent(int eventID, boolean isMeeting) throws IOException {
        DeleteEventReq request = new DeleteEventReq(eventID, isMeeting);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
    }


    /**
     * Registers the given user with the unique username passed in as accepted the meeting on the server side, the
     * server then updates associated logged in users.
     * @param meetingID the unique meeting ID of the meeting accepted by the given user
     * @param username the unique username of the user who just accepted the meeting
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public void acceptMeeting(int meetingID, String username) throws IOException {
        EditAcceptanceReq request = new EditAcceptanceReq(meetingID, username, true);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
    }


    /**
     * Registers the user as having rejected the meeting, the server essentially removing them from the attendees and
     * removing the event from their schedule, then updates associated logged in users.
     * @param meetingID the meeting id being rejected
     * @param username the unique username of the user rejecting the meeting
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    public void rejectMeeting(int meetingID, String username) throws IOException {
        EditAcceptanceReq request = new EditAcceptanceReq(meetingID, username, false);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
    }


    /**
     * Requests the server to remove the attendee.
     * @param id the id of the meeting for the attendee to be removed from
     * @param attendee the unique username of the attendee being removed
     * @throws IOException if there is an error in networking
     */
    public void removeAttendee(int id, String attendee) throws IOException {
        RemoveAttendeeReq request = new RemoveAttendeeReq(id, attendee);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
    }


    /**
     * Requests the server to add the attendee given to the meeting with the unique id given
     * @param id the unique id of the meeting for the attendee to be added to
     * @param attendee the unique username of the attendee being added
     * @throws IOException if there was an error transmitting or receiving over the object streams
     * @throws UserNotFoundException if the user was not found in the database
     * @throws EventNotFoundException if the event was not found in the database
     */
    public void addAttendee(int id, String attendee) throws IOException {
        AddAttendeeReq request = new AddAttendeeReq(id, attendee);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
    }


    /**
     * Creates the comment on the server side, called when a comment has been created.
     * @param comment the comment object to be added on the server side
     * @throws IOException if there was an error transmitting or receiving over the object streams
     * @throws EventNotFoundException if the meeting with the meeting ID stored in the comment
     *         couldn't be found on the database
     */
    public Comment createComment(Comment comment) throws IOException, EventNotFoundException {
        CreateCommentReq request = new CreateCommentReq(comment);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass() == Comment.class) {
            return (Comment) readObject;
        } else if(readObject.getClass() == ServerError.class) {
            // frisbee was thrown FATALITY HAS OCCURRED !!!!!!!
            Frisbee frisbee = (Frisbee)((ServerError)readObject).exception;
            System.err.println("Frisbee thrown, Fatality!\n" + frisbee.deathPic);
            frisbee.printStackTrace();
            System.exit(9001);
            return null;
        } else {
            throw new IOException();
        }
    }


    /**
     * Takes the new attendees who the user wanted to add to the meeting, the start time of the meeting, the duration
     * and finds out server side which attendees can't attend and returns an array list of triples, one triple for
     * each user who can't attend. A triple is as follows {username, firstName, lastName}. This information is so
     * the GUI can display who can't attend so the user can make a decision and the unique username can then be used to
     * execute the require tasks.
     * @param potentialAttendees an arrayList of usernames of the users who have been newly added
     * @param startTime the starting time of the meeting
     * @param duration the duration of the meeting
     * @return an arrayList of triples representing the details of the users who can't attend the meeting at the
     * given time
     * @throws IOException if there was an error transmitting or receiving over the object streams
     */
    @SuppressWarnings("unchecked")
	public ArrayList<String[]> checkAvailable(
            ArrayList<String> potentialAttendees,
            Calendar startTime,
            int duration,
            int meetingID) throws IOException {
        CheckAvailabilityReq request = new CheckAvailabilityReq(potentialAttendees, startTime, duration, meetingID);
        objectOutputStream.writeObject(request);
        objectOutputStream.flush();
        Object readObject = getNext();
        if(readObject.getClass() == ArrayList.class) {
            return (ArrayList<String[]>) readObject;
        } else {
            throw new IOException();
        }
    }

}


