package gui.support;

import gui.interfaces.Colour;

import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.JTextComponent;

/**
 * A container to hold and sting and a title for the string.
 * @author Zack McGrath
 */
public class LabelPanel extends SupportPanel {
	private static final long serialVersionUID = -1556382657861484401L;
	private String header;
	private String label;
	
	/**
	 * Create the panel.
	 * @param fullWidth 
	 */
	public LabelPanel(String title, String data, boolean fullWidth) {
		header = title;
		label = data;
		
		//JPanel panel = new JPanel();
		setBorder(new TitledBorder(new LineBorder(Colour.GRAY_BLUE), header, TitledBorder.LEADING, TitledBorder.TOP));
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		//JLabel lblName = new JLabel(name);
		JTextArea lblData = new JTextArea(label);
		lblData.setEditable(false);
		lblData.setLineWrap(true);
		lblData.setColumns(15);
		if (fullWidth)
			lblData.setColumns(32);
		lblData.setBackground(getBackground());
		
		//lblName.setAlignmentX(Component.CENTER_ALIGNMENT);
		//lblName.setFont(new Font("Dialog", Font.BOLD, 10));
		lblData.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		add(lblData);
		
		//add(lblName);
	}
	
	
	@Override
	public JTextComponent getField() {
		return null;
	}
	
	
	@Override
	public String getInput() {
		return label;
	}
	
	
	@Override
	public boolean isFieldNotEmpty() {
		return true;
	}
}
