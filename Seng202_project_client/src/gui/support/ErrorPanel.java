package gui.support;

import gui.interfaces.Colour;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A container to hold an error.
 * @author Zack McGrath
 */
public class ErrorPanel extends JPanel {
	private static final long serialVersionUID = -4718873983572181268L;
	private JLabel lblErrorMessage;
	
	
	/**
	 * Create the panel.
	 */
	public ErrorPanel() {
		lblErrorMessage = new JLabel(" ");
		lblErrorMessage.setForeground(Colour.RED_UNACCEPTABLE);
		add(lblErrorMessage);
	}
	
	
	/**
	 * @param message the message to display
	 */
	public void setErrorMessage(String message) {
		lblErrorMessage.setText(message);
	}
}
