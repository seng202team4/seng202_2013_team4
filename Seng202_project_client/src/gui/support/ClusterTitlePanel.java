package gui.support;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A container to hold a title picture.
 * @author Zack McGrath
 */
public class ClusterTitlePanel extends JPanel {
	private static final long serialVersionUID = -5020260383849214678L;
	
	
	/**
	 * Create the panel.
	 */
	public ClusterTitlePanel() {		
		try {                
			//image = ImageIO.read(new File("gui/images/logo.jpg"));
			add(new JLabel(new ImageIcon(getClass().getResource("logo.jpg"))));
		} catch (Exception ioe) {
			add(new JLabel("CLUSTER"));
	    	System.err.println("Logo not found");
		}
	}
}
