package gui.support;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * A container field to hold a JTextField and it's label.
 * @author Zack McGrath
 */
public class TextFieldPanel extends SupportPanel {
	private static final long serialVersionUID = 4324775063206211194L;
	private JTextField field; 

	/**
	 * Create the panel.
	 */
	public TextFieldPanel(String title, String defaultText) {
		setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		JLabel lbl = new JLabel(title);
		field = new JTextField(defaultText);
		field.setColumns(10);
		
		add(lbl);
		add(field);
	}
	
	
	@Override
	public JTextField getField() {
		return field;
	}
	
	
	@Override
	public String getInput() {
		return field.getText().trim();
	}
	
	
	@Override
	public boolean isFieldNotEmpty() {
		return getInput().length() != 0;
	}
	
	
	public void setText(String text) {
		field.setText(text);
	}
}
