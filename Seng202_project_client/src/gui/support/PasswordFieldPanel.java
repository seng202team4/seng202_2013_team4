package gui.support;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 * A container to hold JPassword and it's label.
 * @author Zack McGrath
 */
public class PasswordFieldPanel extends SupportPanel {
	private static final long serialVersionUID = -4462707018600526957L;
	private JPasswordField field;
	
	
	/**
	 * Create the panel.
	 */
	public PasswordFieldPanel(String name) {
		setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		JLabel lbl = new JLabel(name);
		field = new JPasswordField();
		field.setColumns(10);
		
		add(lbl);
		add(field);
	}
	
	
	@Override
	public JPasswordField getField() {
		return field;
	}
	
	
	@Override
	public char[] getInput() {
		return field.getPassword();
	}
	
	
	@Override
	public boolean isFieldNotEmpty() {
		return getInput().length != 0;
	}
}
