package gui.support;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * A container to hold a JTextArea and it's label.
 * @author Zack McGrath
 */
public class TextAreaPanel extends SupportPanel {
	private static final long serialVersionUID = -7663066207409977160L;
	private JTextArea field;
	
	
	/**
	 * Create the panel.
	 */
	public TextAreaPanel(String title, String defaultText) {
		FlowLayout fl = (FlowLayout) this.getLayout();
		fl.setAlignment(FlowLayout.RIGHT);
		fl.setAlignOnBaseline(true);
		
		JLabel lbl = new JLabel(title);
		field = new JTextArea(defaultText);
		field.setLineWrap(true);
		field.setColumns(20);
		field.setRows(5);
		
		JScrollPane scrollPane = new JScrollPane(field);
		
		add(lbl);
		add(scrollPane);
	}
	
	
	@Override
	public JTextArea getField() {
		return field;
	}
	
	
	@Override
	public String getInput() {
		return field.getText().trim();
	}
	
	
	@Override
	public boolean isFieldNotEmpty() {
		return getInput().length() != 0;
	}
}
