package gui.support;


import gui.interfaces.NonEmptyFields;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 * Abstract class to allow for abstraction of support panels.
 * @author Zack McGrath
 */
public abstract class SupportPanel extends JPanel {
	private static final long serialVersionUID = 5070295478887415540L;
	
	
	/**
	 * @return the editable component in the panel
	 */
	public abstract JTextComponent getField();
	
	
	/**
	 * @return the text in the editable field
	 */
	public abstract Object getInput();
	
	
	/**
	 * @return true if the field is not empty
	 */
	public abstract boolean isFieldNotEmpty();
	
	
	/**
	 * Adds document listeners to the editable field in the support panel
	 * @param parant a component that implements NonEmptyFields
	 */
	public void addEmtpyListener(final NonEmptyFields parant) {
		if (getField() != null) {
			getField().getDocument().addDocumentListener(new DocumentListener() {
				public void changedUpdate(DocumentEvent e) {
					parant.checkEmpty();
				}
				public void removeUpdate(DocumentEvent e) {
					parant.checkEmpty();
				}
				public void insertUpdate(DocumentEvent e) {
					parant.checkEmpty();
				}
			});
		}
	}
}
