package gui.login;

import gui.interfaces.NonEmptyFields;
import gui.support.ErrorPanel;
import gui.support.PasswordFieldPanel;
import gui.support.SupportPanel;
import gui.support.TextFieldPanel;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A JPanel where user's can enter their login info.
 * If there are saved login info the window will try to log them in automatically.
 * @author Zack McGrath
 */
public class LoginDetailsPanel extends JPanel implements NonEmptyFields {
	private static final long serialVersionUID = -8159136671973518982L;
	
	private TextFieldPanel userNamePanel;
	private PasswordFieldPanel userPasswordPanel;
	private JPanel buttonPanel;
	private WebButton btnLogin;
	private WebButton btnNewUser;
	
	private JPanel connectionPanel;
	private WebButton btnReconnect;
	private JLabel lblConnection;
	private ErrorPanel error;

	private boolean allFieldNotEmpty;
	
	
	/**
	 * Create the panel.
	 */
	public LoginDetailsPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		error = new ErrorPanel();
		userNamePanel = new TextFieldPanel("Username", ClientManager.getClientManager().getPreviousUsername());
		userPasswordPanel = new PasswordFieldPanel("Password");
		buttonPanel = new JPanel();
		connectionPanel = new JPanel();
		
		btnLogin = new WebButton	(" Login  ");
		btnNewUser = new WebButton	("New User");
		
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnLogin, btnNewUser);
		buttonPanel.add(iconsGroup);
		
		lblConnection = new JLabel(ClientManager.getCurrentIP());
		btnReconnect = new WebButton("Reconnect");
		connectionPanel.add(lblConnection);
		connectionPanel.add(btnReconnect);
		
		add(error);
		add(userNamePanel);
		add(userPasswordPanel);
		add(buttonPanel);
		add(connectionPanel);
		

		
		initializeEmptyListeners();
	}


	@Override
	public ArrayList<SupportPanel> getNonEmptyPanels() {
		return new ArrayList<SupportPanel>(Arrays.asList(
				userNamePanel, 
				userPasswordPanel));
	}


	@Override
	public void initializeEmptyListeners() {
		for (SupportPanel p: getNonEmptyPanels()) {
			p.addEmtpyListener(this);
		}
		checkEmpty();
	}
	
	
	@Override
	public void checkEmpty() {
		error.setErrorMessage(" ");
		allFieldNotEmpty = true;
		for (SupportPanel p: getNonEmptyPanels()) {
			allFieldNotEmpty = allFieldNotEmpty && p.isFieldNotEmpty();
		}
		btnLogin.setEnabled(allFieldNotEmpty);
	}


	public void setErrorMessage(String message) {
		error.setErrorMessage(message);
	}
	
	
	public String getUserName() {
		return userNamePanel.getField().getText().trim();
	}
	
	
	public char[] getPassword() {
		return userPasswordPanel.getField().getPassword();
	}
	
	
	public WebButton getLoginButton() {
		return btnLogin;
	}
	
	
	public WebButton getNewUserButton() {
		return btnNewUser;
	}

	
	public WebButton getReconnectButton() {
		return btnReconnect;
	}

	
	public boolean areAllFieldNotEmpty() {
		return allFieldNotEmpty;
	}
}