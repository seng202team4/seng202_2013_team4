package gui.login;

import gui.left.LeftPanel;
import gui.main.ParentFrame;
import gui.support.ClusterTitlePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import client.ClientManager;

import com.alee.laf.WebLookAndFeel;

/**
 * A window that contains ServerDetailsPanel.
 * @author Zack McGrath
 */
public class ServerWindow extends ParentFrame {
	private static final long serialVersionUID = -2541233941631558739L;
	private JPanel contentPane;
	private ServerDetailsPanel server;
	
	
	/**
	 * Create the frame.
	 * @param boolean tryConnectOnLaunch: true if you want the window to try 
	 * 		connecting as soon as the window is opened or false if you want the
	 * 		window to open and then wait.
	 */
	public ServerWindow(boolean tryConnectOnLaunch) {
		super("Connect");
		WebLookAndFeel.install();
		
		setBounds(100, 100, 450, 300);
		contentPane = (JPanel) getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		
		ClusterTitlePanel title = new ClusterTitlePanel();
		server = new ServerDetailsPanel();
		
		contentPane.add(title);
		contentPane.add(server);
		
		getRootPane().setDefaultButton(server.getConnectButton());
		setResizable(false);
		pack();
		
		if (tryConnectOnLaunch) {
			addWindowListener(new WindowAdapter() {
	            public void windowOpened(WindowEvent event) {
	            	if (server.areAllFieldNotEmpty()) {
	            		try {
	            			tryConnect();
	            		} catch (NumberFormatException | IOException e) {
	            			//Do nothing when user opens this window
	            		}
	            	}
	            }
	        });
		}
		
		server.getConnectButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					tryConnect();
				} catch (NumberFormatException nfe) {
					server.setErrorMessage("Port must be a number");
					server.getConnectButton().setEnabled(false);
				} catch (IOException ioe) {
					server.setErrorMessage("Cannot connect to server");
					server.getConnectButton().setEnabled(false);
				}
			}
		});
	}
	
	
	private void tryConnect() throws IOException {		
		ClientManager.getClientManager().connectToServer(
				server.getServerIP(), 
				server.getServerPort());
		
		ClientManager.getClientManager().startLoginWindow();
		kill();
	}
	
	
	@Override
	public LeftPanel getLeftPanel() {
		return null;
	}
}
