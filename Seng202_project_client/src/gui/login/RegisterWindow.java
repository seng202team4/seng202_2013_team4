package gui.login;

import exceptions.InvalidUsernameException;
import exceptions.LoginErrorException;
import gui.left.LeftPanel;
import gui.main.ParentFrame;
import gui.support.ClusterTitlePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import client.ClientManager;

/**
 * Window to hold the RegisterDetailsPanel.
 * @author Zack McGrath
 */
public class RegisterWindow extends ParentFrame {
	private static final long serialVersionUID = -1832834249860739035L;
	private JPanel contentPane;
	private RegisterDetailsPanel register;
	
	
	/**
	 * Create the frame.
	 */
	public RegisterWindow() {
		super("Register");
		setBounds(100, 100, 450, 300);
		
		contentPane = (JPanel) getContentPane();
		ClusterTitlePanel title = new ClusterTitlePanel();
		register = new RegisterDetailsPanel();
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		
		contentPane.add(title);
		contentPane.add(register);
		
		getRootPane().setDefaultButton(register.getRegisterButton());
		setResizable(false);
		pack();
		
		register.getRegisterButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				String username = register.getUserName();
				char[] password = register.getPassword();
				char[] passwordConfirm = register.getConfirmPassword();
				String firstName = register.getFirstName();
				String lastName = register.getLastName();
				String email = register.getEmail();
				String phone = register.getPhone();
				String room = register.getRoom();
				String position = register.getPosition();
				String bio = register.getBio();
				
				if (assertPasswordSame(password, passwordConfirm) != true) {
					register.setErrorMessage("Password does not match");
					register.getRegisterButton().setEnabled(false);
				} else {
					try {
						ClientManager.getClientManager().registerUser(
								username, 
								password, 
								firstName, 
								lastName,
								email,
								phone,
								room,
								position,
								bio);
						
						ClientManager.getClientManager().startApplicationWindow();
						kill();
					} catch (IOException ioe) {
			        	disconnectionError();
					} catch (InvalidUsernameException iue) {
						register.setErrorMessage("Failure registering");
						//register.getRegisterButton().setEnabled(false);
					} catch (LoginErrorException lee) {
						register.setErrorMessage("Failure logging in");
					}
				}
			}
		});
		
		register.getCancelButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ClientManager.getClientManager().startLoginWindow();
				kill();
			}
		});
	}
	
	
	private Boolean assertPasswordSame(char[] password, char[] passwordConfirm) {
		int len1 = password.length;
		int len2 = passwordConfirm.length;
		if (len1 != len2) {
			return false;
		}
		
		for (int i = 0; i < len1; i++) {
			if (password[i] != passwordConfirm[i]) {
				return false;
			}
		}
		return true;
	}
	
	
	@Override
	public LeftPanel getLeftPanel() {
		return null;
	}
}
