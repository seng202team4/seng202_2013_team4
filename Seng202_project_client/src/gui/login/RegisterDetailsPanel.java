package gui.login;

import gui.interfaces.NonEmptyFields;
import gui.support.ErrorPanel;
import gui.support.SupportPanel;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * JPanel where user's can register.
 * Username, First Name, Last Name and Password are all required.
 * @author Zack McGrath
 */
public class RegisterDetailsPanel extends JPanel implements NonEmptyFields {
	private static final long serialVersionUID = 551596598036367741L;
	
	private ErrorPanel error;
	private UserDataPanel data;
	private WebButton btnRegister;
	
	private boolean allFieldNotEmpty;
	private WebButton btnCancel;
	
	
	/**
	 * Create the panel.
	 */
	public RegisterDetailsPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		error = new ErrorPanel();
		data = new UserDataPanel();
		JPanel buttonPanel = new JPanel();
		
		btnRegister = new WebButton	("Register");
		btnCancel = new WebButton	(" Cancel ");
		
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnRegister, btnCancel);
		buttonPanel.add(iconsGroup);
		
		add(error);
		add(data);
		add(buttonPanel);
		
		initializeEmptyListeners();
	}


	@Override
	public ArrayList<SupportPanel> getNonEmptyPanels() {
		return new ArrayList<SupportPanel>(Arrays.asList(
				data.getUserNamePanel(), 
				data.getFirstNamePanel(), 
				data.getLastNamePanel(), 
				data.getPasswordPanel(),
				data.getConfirmPasswordPanel()));
	}

	
	@Override
	public void initializeEmptyListeners() {
		for (SupportPanel p: getNonEmptyPanels()) {
			p.addEmtpyListener(this);
		}
		checkEmpty();
	}
	
	
	@Override
	public void checkEmpty() {
		allFieldNotEmpty = true;
		for (SupportPanel p: getNonEmptyPanels()) {
			allFieldNotEmpty = allFieldNotEmpty && p.isFieldNotEmpty();
		}
		btnRegister.setEnabled(allFieldNotEmpty);
	}


	public void setErrorMessage(String message) {
		error.setErrorMessage(message);
	}

	
	public String getUserName() {
		return (String) data.getUserNamePanel().getInput();
	}

	
	public char[] getPassword() {
		return data.getPasswordPanel().getInput();
	}

	
	public char[] getConfirmPassword() {
		return data.getConfirmPasswordPanel().getInput();
	}

	
	public String getFirstName() {
		return data.getFirstNamePanel().getInput();
	}

	
	public String getLastName() {
		return data.getLastNamePanel().getInput();
	}

	
	public String getPosition() {
		return data.getPositionPanel().getInput();
	}

	
	public String getRoom() {
		return data.getRoomPanel().getInput();
	}


	public String getEmail() {
		return data.getEmailPanel().getInput();
	}

	
	public String getPhone() {
		return data.getPhonePanel().getInput();
	}

	
	public String getBio() {
		return data.getBioPanel().getInput();
	}

	
	public WebButton getRegisterButton() {
		return btnRegister;
	}
	
	
	public WebButton getCancelButton() {
		return btnCancel;
	}

	
	public boolean areAllFieldNotEmpty() {
		return allFieldNotEmpty;
	}
}
