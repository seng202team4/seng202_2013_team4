package gui.login;

import exceptions.LoginErrorException;
import gui.left.LeftPanel;
import gui.main.ParentFrame;
import gui.support.ClusterTitlePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import client.ClientManager;

/**
 * A window to contain LoginDetailsPanel.
 * @author Zack McGrath
 */
public class LoginWindow extends ParentFrame {
	private static final long serialVersionUID = -1185074575983346095L;
	
	private JPanel contentPane;
	private LoginDetailsPanel login;
	
	
	/**
	 * Initialize the contents of the frame.
	 */
	public LoginWindow() {
		super("Login");
		setBounds(100, 100, 450, 300);
		contentPane = (JPanel) getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		
		ClusterTitlePanel title = new ClusterTitlePanel();
		login = new LoginDetailsPanel();
		
		contentPane.add(title);
		contentPane.add(login);
		
		getRootPane().setDefaultButton(login.getLoginButton());
		setResizable(false);
		pack();
		
		addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent event) {
            	if (login.areAllFieldNotEmpty()) {
            		try {
						tryLogin();
					} catch (IOException | LoginErrorException e) {
            			//Do nothing when user opens this window
					}
            	}
            }
        });
		
		login.getLoginButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					tryLogin();
				} catch (LoginErrorException lee) {
					login.setErrorMessage("Incorrect login");
					login.getLoginButton().setEnabled(false);
				} catch (IOException ioe) {
		        	disconnectionError();
		        }
			}
		});
		
		login.getNewUserButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ClientManager.getClientManager().startRegisterWindow();
				kill();
			}
		});
		
		login.getReconnectButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ClientManager.getClientManager().startServerLoginWindow(false);
				kill();
			}
		});
	}
	
	
	private void tryLogin() throws IOException, LoginErrorException {
        ClientManager.getClientManager().login(
                login.getUserName(),
                login.getPassword());
        
		ClientManager.getClientManager().startApplicationWindow();
		kill();
	}
	
	
	@Override
	public LeftPanel getLeftPanel() {
		return null;
	}
}
