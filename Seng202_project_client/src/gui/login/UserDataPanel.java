package gui.login;

import gui.support.LabelPanel;
import gui.support.PasswordFieldPanel;
import gui.support.SupportPanel;
import gui.support.TextAreaPanel;
import gui.support.TextFieldPanel;

import javax.swing.JPanel;

import shared.User;
import javax.swing.BoxLayout;

import client.ClientManager;

/**
 * A panel that contains users info. If no user logged allows user to enter info.
 * @author Zack McGrath
 */
public class UserDataPanel extends JPanel {
	private static final long serialVersionUID = 6530412014491822223L;
	
	private SupportPanel userNamePanel;
	private TextFieldPanel firstNamePanel;
	private TextFieldPanel lastNamePanel;
	private TextFieldPanel positionPanel;
	private TextFieldPanel roomPanel;
	private TextFieldPanel emailPanel;
	private TextFieldPanel phonePanel;
	
	private TextAreaPanel bioPanel;
	
	private PasswordFieldPanel userPasswordPanel;
	private PasswordFieldPanel confirmUserPasswordPanel;
	
	
	/**
	 * Create the panel.
	 */
	public UserDataPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		User user = ClientManager.getClientManager().getCurrentUser();
		
		String firstName = user == null ? "" : user.getFirstname();
		String lastName = user == null ? "" : user.getLastName();
		String position = user == null ? "" : user.getPosition();
		String room = user == null ? "" : user.getRoom();
		String email = user == null ? "" : user.getEmail();
		String phone = user == null ? "" : user.getPhone();
		String bio = user == null ? "" : user.getBio();
		
		if (user == null) {
			userNamePanel = new TextFieldPanel("Username", "");
		} else {
			userNamePanel = new LabelPanel("Usrname", user.getUsername(), false);
		}
		userPasswordPanel = new PasswordFieldPanel("Password");
		confirmUserPasswordPanel = new PasswordFieldPanel("Confirm Password");
		firstNamePanel = new TextFieldPanel("First Name", firstName);
		lastNamePanel = new TextFieldPanel("Last Name", lastName);
		positionPanel = new TextFieldPanel("Position", position);
		roomPanel = new TextFieldPanel("Room", room);
		emailPanel = new TextFieldPanel("E-Mail", email);
		phonePanel = new TextFieldPanel("Phone Number", phone);  
		bioPanel = new TextAreaPanel("Bio", bio);
		
		add(userNamePanel);
		add(userPasswordPanel);
		add(confirmUserPasswordPanel);
		add(firstNamePanel);
		add(lastNamePanel);
		add(positionPanel);
		add(roomPanel);
		add(emailPanel);
		add(phonePanel);
		add(bioPanel);
	}

	
	public SupportPanel getUserNamePanel() {
		return userNamePanel;
	}

	
	public PasswordFieldPanel getPasswordPanel() {
		return userPasswordPanel;
	}

	
	public PasswordFieldPanel getConfirmPasswordPanel() {
		return confirmUserPasswordPanel;
	}

	
	public TextFieldPanel getFirstNamePanel() {
		return firstNamePanel;
	}

	
	public TextFieldPanel getLastNamePanel() {
		return lastNamePanel;
	}

	
	public TextFieldPanel getPositionPanel() {
		return positionPanel;
	}

	
	public TextFieldPanel getRoomPanel() {
		return roomPanel;
	}

	
	public TextFieldPanel getEmailPanel() {
		return emailPanel;
	}

	
	public TextFieldPanel getPhonePanel() {
		return phonePanel;
	}

	
	public TextAreaPanel getBioPanel() {
		return bioPanel;
	}


	public void refresh() {
		repaint();
		revalidate();
	}
}
