package gui.login;

import gui.interfaces.NonEmptyFields;
import gui.support.ErrorPanel;
import gui.support.SupportPanel;
import gui.support.TextFieldPanel;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import client.ClientManager;

import com.alee.laf.button.WebButton;

/**
 * A JPanel that allows a user to enter the server IP and port they want to 
 * connect to. Will try to connect automatically if there are stored IP and port.
 * @author Zack McGrath
 */
public class ServerDetailsPanel extends JPanel implements NonEmptyFields {
	private static final long serialVersionUID = 7525321550043127166L;
	
	private ErrorPanel error;
	private TextFieldPanel serverIPPanel;
	private TextFieldPanel serverPortPanel;
	private WebButton btnConnect;
	
	private boolean allFieldNotEmpty;
	
	
	/**
	 * Create the panel.
	 */
	public ServerDetailsPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		error = new ErrorPanel();
		serverIPPanel = new TextFieldPanel("Server IP", ClientManager.getClientManager().getPreviousIP());
		serverPortPanel = new TextFieldPanel("Server Port", ClientManager.getClientManager().getPreviousPort());
		JPanel buttonPanel = new JPanel();
		
		btnConnect = new WebButton("Connect");
		buttonPanel.add(btnConnect);
		
		add(error);
		add(serverIPPanel);
		add(serverPortPanel);
		add(buttonPanel);
				
		initializeEmptyListeners();
	}


	@Override
	public ArrayList<SupportPanel> getNonEmptyPanels() {
		return new ArrayList<SupportPanel>(Arrays.asList(
				serverIPPanel,
				serverPortPanel));
	}


	@Override
	public void initializeEmptyListeners() {
		for (SupportPanel p: getNonEmptyPanels()) {
			p.addEmtpyListener(this);
		}
		checkEmpty();
	}
	
	
	@Override
	public void checkEmpty() {
		allFieldNotEmpty = true;
		for (SupportPanel p: getNonEmptyPanels()) {
			allFieldNotEmpty = allFieldNotEmpty && p.isFieldNotEmpty();
		}
		btnConnect.setEnabled(allFieldNotEmpty);
	}
	
	
	public String getServerIP() {
		return serverIPPanel.getField().getText().trim();
	}
	
	
	public int getServerPort() throws NumberFormatException {
		return Integer.parseInt(serverPortPanel.getField().getText());
	}


	public void setErrorMessage(String message) {
		error.setErrorMessage(message);
	}
	
	
	public WebButton getConnectButton() {
		return btnConnect;
	}
	
	
	public boolean areAllFieldNotEmpty() {
		return allFieldNotEmpty;
	}
}
