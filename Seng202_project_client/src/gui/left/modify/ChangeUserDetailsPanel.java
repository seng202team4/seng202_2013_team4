package gui.left.modify;

import gui.interfaces.NonEmptyFields;
import gui.interfaces.Refresh;
import gui.login.UserDataPanel;
import gui.main.ParentFrame;
import gui.support.ErrorPanel;
import gui.support.SupportPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A panel that allows a user to change their info.
 * @author Zack McGrath
 */
public class ChangeUserDetailsPanel extends JPanel implements Refresh, NonEmptyFields {
	private static final long serialVersionUID = 7662472965163167288L;
	
	private JPanel container;
	
	private ErrorPanel error;
	private UserDataPanel data;
	private JPanel buttonPanel;
	private WebButton btnSave;
	private WebButton btnCancel;
	
	/**
	 * Create the panel.
	 */
	public ChangeUserDetailsPanel() {
		container = new JPanel();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		
		error = new ErrorPanel();
		data = new UserDataPanel();
		buttonPanel = new JPanel();
		
		btnSave = new WebButton(" Save ");
		btnCancel = new WebButton("Cancel");
		
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnSave, btnCancel);
		buttonPanel.add(iconsGroup);
		
		container.add(error);
		container.add(data);
		container.add(buttonPanel);
		
		add(container);
		
		addListeners();
		
		initializeEmptyListeners();
	}


	private void addListeners() {
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
                try {
                    ClientManager.getClientManager().updateUser(
                            data.getPasswordPanel().getInput(),
                            data.getFirstNamePanel().getInput(),
                            data.getLastNamePanel().getInput(),
                            data.getEmailPanel().getInput(),
                            data.getPhonePanel().getInput(),
                            data.getRoomPanel().getInput(),
                            data.getPositionPanel().getInput(),
                            data.getBioPanel().getInput());
                    getRoot().getLeftPanel().noneVisible();
                } catch (IOException ioe) {
                	getRoot().disconnectionError();
                }
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
            	getRoot().getLeftPanel().noneVisible();
            }
        });
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}

	
	@Override
	public ArrayList<SupportPanel> getNonEmptyPanels() {
		return new ArrayList<>(Arrays.asList(
				data.getUserNamePanel(), 
				data.getFirstNamePanel(), 
				data.getLastNamePanel()));
	}

	
	@Override
	public void initializeEmptyListeners() {
		for (SupportPanel p: getNonEmptyPanels()) {
			p.addEmtpyListener(this);
		}
		checkEmpty();
	}

	
	@Override
	public void checkEmpty() {
		boolean allFieldNotEmpty = true;
		for (SupportPanel p: getNonEmptyPanels()) {
			allFieldNotEmpty = allFieldNotEmpty && p.isFieldNotEmpty();
		}
		btnSave.setEnabled(allFieldNotEmpty);
	}
	

	@Override
	public void refresh() {
		data.refresh();
		repaint();
		revalidate();
	}
}
