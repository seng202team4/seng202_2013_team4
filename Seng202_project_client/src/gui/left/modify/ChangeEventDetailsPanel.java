package gui.left.modify;

import exceptions.InvalidTimeslotException;
import gui.interfaces.Refresh;
import gui.left.details.EventDetailsPanel;
import gui.main.ParentFrame;
import gui.support.ErrorPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.Event;
import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A panel that allows a user to change a personal event.
 * @author Zack McGrath
 */
public class ChangeEventDetailsPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = 7384428082793541628L;
	
	private EventDetailsPanel details;
	private ErrorPanel error;
	
	private WebButton btnSaveChanges;
	private WebButton btnCancel;
	
	private Event event;
	

	/**
	 * Create the panel.
	 */
	public ChangeEventDetailsPanel(Event selected) {
		event = selected;
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		error = new ErrorPanel();
		details = new EventDetailsPanel(selected);		
		JPanel buttonPanel = new JPanel();
		
		btnSaveChanges = new WebButton(" Save ");
		btnCancel = new WebButton("Cancel");
		
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnSaveChanges, btnCancel);
		buttonPanel.add(iconsGroup);
		
		add(error);
		add(details);
		add(buttonPanel);
		
		addListeners();
	}


	private void addListeners() {
		btnSaveChanges.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					ClientManager.getClientManager().updateEvent(
							event.getID(), 
							details.getEventTitle().getInput(), 
							details.getLocationPanel().getInput(), 
							details.getDescription().getInput(), 
							details.getDurationSlider().getValue(), 
							details.getStartTime());
					getRoot().getLeftPanel().noneVisible();
				} catch (IOException ioe) {
					getRoot().disconnectionError();
				} catch (InvalidTimeslotException itse) {
					error.setErrorMessage("Invaild time selection.");
				}
			}
		});

		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				getRoot().getLeftPanel().noneVisible();
			}
		});
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
