package gui.left.modify;

import gui.interfaces.Refresh;
import gui.left.LeftPanel;
import gui.left.details.MeetingDetailsPanel;
import gui.main.ParentFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.Meeting;
import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A warning window that appears and warns the user about the users that 
 * 		cannot attend.
 * @author Zack McGrath
 */
public class AttendeesBusyWindow extends ParentFrame implements Refresh {
	private static final long serialVersionUID = 515040438012623746L;
	private JPanel contentPane;
	
	private WebButton btnProceedAnyway;
	private WebButton btnCancelCreation;
	
	private ChangeMeetingDetailsPanel change;
	
	private Meeting meeting;
	private ArrayList<String> users;
	
	
	/**
	 * Create the panel.
	 */
	public AttendeesBusyWindow(
			ArrayList<String[]> usernames, 
			Meeting selected, 
			ChangeMeetingDetailsPanel change) {
		super("Users Busy");
		this.meeting = selected;
		this.change = change;
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		contentPane = (JPanel) getContentPane();
		JPanel textPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
		
		JLabel lblInfo = new JLabel("These users can not attend the meeting at this time:");
		textPanel.add(lblInfo);
		
		users = new ArrayList<>();
		for (String[] user: usernames) {
			JLabel name = new JLabel(user[1] + " " + user[2]);
			textPanel.add(name);
			users.add(user[0]);
		}
		
		btnProceedAnyway = new WebButton("Procced Anyway");
		btnCancelCreation = new WebButton("Cancel Creation");
		
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnProceedAnyway, btnCancelCreation);
		buttonPanel.add(iconsGroup);
		
		contentPane.add(textPanel);
		contentPane.add(buttonPanel);
		
		pack();
		
		addListeners();
	}


	private void addListeners() {
		btnProceedAnyway.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
            	MeetingDetailsPanel details = change.getDetailsPanel();
                try {
                    ClientManager.getClientManager().updateMeeting(
                            meeting.getID(),
                            details.getEventTitle().getInput(),
                            details.getLocationPanel().getInput(),
                            details.getDescription().getInput(),
                            meeting.getDuration(),
                            meeting.getStartTime(),
                            meeting.getInitiator(),
                            users,
                            meeting.getAccepted(),
                            meeting.getComments());
                    change.setVisible(false);
                } catch (IOException ioe) {
                	((ParentFrame) SwingUtilities.getWindowAncestor(btnProceedAnyway)).disconnectionError();
                }
            }
        });
		
		btnCancelCreation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				change.getBtnSaveChanges().setEnabled(true);
				kill();
			}
		});
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
	
	
	@Override
	public LeftPanel getLeftPanel() {
		return null;
	}
}
