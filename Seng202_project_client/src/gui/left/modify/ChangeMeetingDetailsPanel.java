package gui.left.modify;

import gui.interfaces.Refresh;
import gui.left.details.MeetingDetailsPanel;
import gui.main.ParentFrame;
import gui.support.ErrorPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.Meeting;
import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A panel that allows a user to change a meeting that they are the initiator for.
 * @author Zack McGrath
 */
public class ChangeMeetingDetailsPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = -2323669604648525869L;
	
	private ErrorPanel error;
	private MeetingDetailsPanel details;
	
	private AbstractButton btnSaveChanges;
	private AbstractButton btnCancel;
	
	private Meeting meeting;
	
	
	/**
	 * Create the panel.
	 */
	public ChangeMeetingDetailsPanel(Meeting selected) {
		meeting = selected;
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		error = new ErrorPanel();
		details = new MeetingDetailsPanel(selected);
		JPanel buttonPanel = new JPanel();
		
		btnSaveChanges = new WebButton	(" Save ");
		btnCancel = new WebButton		("Cancel");
		
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnSaveChanges, btnCancel);
		buttonPanel.add(iconsGroup);
		
		add(details);
		add(buttonPanel);
		
		addListeners();
	}


	private void addListeners() {
		btnSaveChanges.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ArrayList<String[]> usersBusy = new ArrayList<>();
                ArrayList<String> username = new ArrayList<>();
                username.addAll(details.getAttendeesList());
                
                try {
                    usersBusy = ClientManager.getClientManager().checkUpdateMeeting(
                            meeting.getAttendees(),
                            username,
                            meeting.getStartTime(),
                            meeting.getDuration(),
                            meeting.getID());
    			} catch (IOException ioe) {
    				getRoot().disconnectionError();
    			}

                if (usersBusy.size() == 0) {
                    try {
                        ClientManager.getClientManager().updateMeeting(
                                meeting.getID(),
                                details.getEventTitle().getInput(),
                                details.getLocationPanel().getInput(),
                                details.getDescription().getInput(),
                                meeting.getDuration(),
                                meeting.getStartTime(),
                                meeting.getInitiator(),
                                username,
                                meeting.getAccepted(),
                                meeting.getComments());
                    } catch (IOException ioe) {
                    	getRoot().disconnectionError();
                    }
                    getRoot().getLeftPanel().noneVisible();
                } else {
                    AttendeesBusyWindow dialog = new AttendeesBusyWindow(usersBusy, meeting, (ChangeMeetingDetailsPanel) details.getParent());
                    dialog.setVisible(true);
                    btnSaveChanges.setEnabled(false);
                }
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				getRoot().getLeftPanel().noneVisible();
			}
		});
	}
	
	
	public AbstractButton getBtnSaveChanges() {
		return btnSaveChanges;
	}


	public AbstractButton getBtnCancel() {
		return btnCancel;
	}


	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	public MeetingDetailsPanel getDetailsPanel() {
		return details;
	}
	
	
	public void setErrorMessage(String message) {
		error.setErrorMessage(message);
	}


	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
