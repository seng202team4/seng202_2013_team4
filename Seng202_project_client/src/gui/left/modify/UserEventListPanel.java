package gui.left.modify;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;
import gui.main.ParentFrame;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import shared.Event;
import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * Displays a list of the events which the logged in user can edit.
 * That is the meeting for which they are they initiator,
 * and the events which are personal events.
 * @author Zack McGrath
 */
public class UserEventListPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = -6168479934487931424L;
	private ArrayList<Event> eventArray;
	private DefaultListModel<String> eventListModel;
	private JList<String> eventList;
	private WebButton btnModifyEvent;
	private WebButton btnDeleteEvent;
	private WebButton btnCancel;
	private JScrollPane listScrollPanel;
	private JPanel buttonPanel;
	
	
	/**
	 * Create the panel.
	 */
	public UserEventListPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setPreferredSize(new Dimension(SizeConstants.LEFT_INTERNAL_WIDTH, 500));
		
		buttonPanel = new JPanel();

		btnModifyEvent = new WebButton	("Modify Event");
		btnDeleteEvent = new WebButton	("Delete Event");
		btnCancel = new WebButton		("   Cancel   ");

		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnModifyEvent, btnDeleteEvent, btnCancel);
		buttonPanel.add(iconsGroup);
		
		refresh();
		
		addListeners();
	}


	private void addListeners() {
		btnModifyEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				getRoot().getLeftPanel().setChangeEventPanel(getSelectedEvent());
			}
		});
		
		btnDeleteEvent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					Event event = eventArray.get(eventList.getSelectedIndex());
					ClientManager.getClientManager().deleteEvent(event.getID(), event.isMeeting());
					getRoot().getLeftPanel().noneVisible();
				} catch (IOException ioe) {
					getRoot().disconnectionError();
				}
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				getRoot().getLeftPanel().noneVisible();
			}
		});
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	
	/**
	 * @return the event that is selected in the JList.
	 */
	public Event getSelectedEvent() {
		return eventArray.get(eventList.getSelectedIndex());
	}


	@Override
	public void refresh() {
		removeAll();
		
		eventArray = ClientManager.getClientManager().getOwnedEventList();
		eventListModel = new DefaultListModel<String>();
		
		for (Event event: eventArray) {
			eventListModel.addElement(event.getName());
		}
		
		eventList = new JList<String>(eventListModel);
		eventList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		listScrollPanel = new JScrollPane(eventList);
		
		add(listScrollPanel);
		add(buttonPanel);
	}
}
