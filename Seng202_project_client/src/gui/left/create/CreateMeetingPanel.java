package gui.left.create;

import gui.interfaces.NonEmptyFields;
import gui.interfaces.Refresh;
import gui.left.details.MeetingDetailsPanel;
import gui.left.timeselection.MeetingTimeSelectionPanel;
import gui.main.ParentFrame;
import gui.support.ErrorPanel;
import gui.support.SupportPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A JPanel that allow for the creation of meetings.
 * @author Zack McGrath
 */
public class CreateMeetingPanel extends JPanel implements Refresh, NonEmptyFields {
	private static final long serialVersionUID = 5845185630597932955L;
	
	private JPanel container;
	
	private ErrorPanel error;
	private MeetingDetailsPanel detailsPanel;
	private JPanel buttonPanel;
	private WebButton btnCreate;
	private WebButton btnCancel;
	
	
	/**
	 * Create the panel.
	 */
	public CreateMeetingPanel() {		
		container = new JPanel();

		error = new ErrorPanel();
		detailsPanel = new MeetingDetailsPanel(null);
		buttonPanel = new JPanel();
		
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

		btnCreate = new WebButton("Create");
		btnCancel = new WebButton("Cancel");
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnCreate, btnCancel);
		
		buttonPanel.add(iconsGroup);
		
		container.add(error);
		container.add(detailsPanel);
		container.add(buttonPanel);
		
		add(container);
		
		addListeners();
		
		initializeEmptyListeners();
	}


	private void addListeners() {
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ArrayList<String> userList = getAttendeesList();
				ParentFrame root = getRoot();
				int duration = getDuration();
				Calendar startTime = getEarliestTime();
				Calendar endTime = getLatestTime();
				
				if (!ClientManager.getClientManager().checkStartTimeFormat(startTime)) {
					getError().setErrorMessage("Meeting window start time in the past");
				} else if (!ClientManager.getClientManager().checkWindowFormat(startTime, endTime, duration)) {
					getError().setErrorMessage("Meeting window end must be after start time plus duration");
				} else {
					try {
						ArrayList<ArrayList<String[]>> attendanceList = ClientManager.getClientManager()
								.getAttendanceList(startTime, endTime, duration, userList);
						MeetingTimeSelectionPanel panel = new MeetingTimeSelectionPanel(
								getDetailsPanel().getEventTitle().getInput(),
								getDetailsPanel().getLocationPanel().getInput(),
								getDetailsPanel().getDescription().getInput(),
								duration,
								startTime,
								endTime,
								userList,
								attendanceList);
						root.getLeftPanel().setScheduleMeetingPanel(panel);
						repaint();
					} catch (IOException ioe) {
						root.disconnectionError();
					}
				}
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
				getRoot().getLeftPanel().noneVisible();
            }
        });
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	@Override
	public ArrayList<SupportPanel> getNonEmptyPanels() {
		return new ArrayList<>(Arrays.asList(
				detailsPanel.getEventTitle(), 
				detailsPanel.getDescription(),
				detailsPanel.getLocationPanel()));
	}
	
	
	@Override
	public void initializeEmptyListeners() {
		for (SupportPanel p: getNonEmptyPanels()) {
			p.addEmtpyListener(this);
		}
	}
	
	
	@Override
	public void checkEmpty() {
		boolean allFieldNotEmpty = true;
		for (SupportPanel p: getNonEmptyPanels()) {
			allFieldNotEmpty = allFieldNotEmpty && p.isFieldNotEmpty();
		}
		btnCreate.setEnabled(allFieldNotEmpty);
	}
	
	
	/**
	 * @return the error panel associated with CreateMeetingPanel.
	 */
	public ErrorPanel getError() {
		return error;
	}
	
	
	/**
	 * @return the JPanel containing the general event details.
	 */
	public MeetingDetailsPanel getDetailsPanel() {
		return detailsPanel;
	}
	
	
	/**
	 * @return the int value of the duration slider.
	 */
	public int getDuration() {
		return detailsPanel.getDurationSlider().getValue();
	}
	
	
	/**
	 * @return the earliest time that a meeting can be held as a Calendar object.
	 */
	public Calendar getEarliestTime() {
		return (Calendar) detailsPanel.getEarliestTime();
	}
	
	
	/**
	 * @return the latest time that a meeting can be held as a Calendar object.
	 */
	public Calendar getLatestTime() {
		return (Calendar) detailsPanel.getLatestTime();
	}
	
	/**
	 * @return an ArrayList containing the usernames of all the meeting's attendees.
	 */
	public ArrayList<String> getAttendeesList() {
		return detailsPanel.getAttendeesList();
	}
	
	
	/**
	 * @return the create meeting button.
	 */
	public WebButton getCreateButton() {
		return btnCreate;
	}


	public void setTime(Calendar time) {
		detailsPanel.setTime(time);
	}


	@Override
	public void refresh() {
		detailsPanel.refresh();
	}
}
