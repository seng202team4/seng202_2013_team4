package gui.left.create;

import exceptions.InvalidTimeslotException;
import gui.interfaces.NonEmptyFields;
import gui.interfaces.Refresh;
import gui.left.details.EventDetailsPanel;
import gui.main.ParentFrame;
import gui.support.ErrorPanel;
import gui.support.SupportPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A JPanel that allows creation of personal events.
 * @author Zack McGrath
 */
public class CreateEventPanel extends JPanel implements Refresh, NonEmptyFields  {
	private static final long serialVersionUID = -2640475306559048196L;
	
	private JPanel container;
	
	private ErrorPanel error;
	private EventDetailsPanel detailsPanel;
	private JPanel buttonPanel;
	private WebButton btnCreate;
	private WebButton btnCancel;
	
	
	/**
	 * Create the panel.
	 */
	public CreateEventPanel() {
		container = new JPanel();
		
		error = new ErrorPanel();
		detailsPanel = new EventDetailsPanel(null);
		buttonPanel = new JPanel();
		
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		
		btnCreate = new WebButton("Create");
		btnCancel = new WebButton("Cancel");
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnCreate, btnCancel);
		
		buttonPanel.add(iconsGroup);
		
		container.add(error);
		container.add(detailsPanel);
		container.add(buttonPanel);
		
		add(container);
		
		addListeners();
		
		initializeEmptyListeners();
	}


	private void addListeners() {
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ParentFrame root = getRoot();
				int duration = detailsPanel.getDurationSlider().getValue();
				Calendar startTime = detailsPanel.getStartTime();
				boolean startWellPlaced = ClientManager.getClientManager().checkStartTimeFormat(startTime);
				
				if (!startWellPlaced) {
					error.setErrorMessage("Event start time in the past");
				} else {
					try {
						ClientManager.getClientManager().createEvent(
								detailsPanel.getEventTitle().getInput(),
								detailsPanel.getLocationPanel().getInput(),
								detailsPanel.getDescription().getInput(),
								duration,
								startTime);
						root.getLeftPanel().noneVisible();
					} catch (IOException ioe) {
						root.disconnectionError();
					} catch (InvalidTimeslotException ite) {
						error.setErrorMessage("Event cannot be scheduled");
					}
				}
			}
		});

		btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
            	getRoot().getLeftPanel().noneVisible();
            }
        });
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	public void setTime(Calendar time) {
		detailsPanel.setTime(time);
	}


	@Override
	public ArrayList<SupportPanel> getNonEmptyPanels() {
		return new ArrayList<>(Arrays.asList(
				detailsPanel.getEventTitle(), 
				detailsPanel.getDescription(),
				detailsPanel.getLocationPanel()));
	}

	
	@Override
	public void initializeEmptyListeners() {
		for (SupportPanel p: getNonEmptyPanels()) {
			p.addEmtpyListener(this);
		}
		checkEmpty();
	}
	
	
	@Override
	public void checkEmpty() {
		boolean allFieldNotEmpty = true;
		for (SupportPanel p: getNonEmptyPanels()) {
			allFieldNotEmpty = allFieldNotEmpty && p.isFieldNotEmpty();
		}
		btnCreate.setEnabled(allFieldNotEmpty);
	}
	
	
	@Override
	public void refresh() {
		detailsPanel.refresh();
	}
}
