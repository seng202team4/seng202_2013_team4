package gui.left;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;
import gui.left.create.CreateEventPanel;
import gui.left.create.CreateMeetingPanel;
import gui.left.display.EventPanel;
import gui.left.modify.ChangeEventDetailsPanel;
import gui.left.modify.ChangeMeetingDetailsPanel;
import gui.left.modify.ChangeUserDetailsPanel;
import gui.left.modify.UserEventListPanel;
import gui.left.timeselection.MeetingTimeSelectionPanel;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import shared.Event;
import shared.Meeting;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * A JPanel that contains event and meeting creation/display/modification
 * and user details modification.
 * @author Zack McGrath
 */
public class LeftPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = -5887593013484057199L;
	
	private CreateEventPanel createEventPanel;
	private CreateMeetingPanel createMeetingPanel;
	private ChangeUserDetailsPanel changeUserDetailsPanel;
	private UserEventListPanel eventListPanel;
	private JPanel containerPanel;
	private Refresh childPanel;
	
	private WebButton btnCreateEvent;
	private WebButton btnCreateMeeting;
	private WebButton btnModifyUser;
	private WebButton btnEventList;
	
	
	/**
	 * Create the panel.
	 */
	public LeftPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JPanel container = new JPanel();
		JScrollPane scrollPane = new JScrollPane(container);
		
		createEventPanel = new CreateEventPanel();
		createMeetingPanel = new CreateMeetingPanel();
		changeUserDetailsPanel = new ChangeUserDetailsPanel();
		eventListPanel = new UserEventListPanel();
		containerPanel = new JPanel();
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setMaximumSize(new Dimension(SizeConstants.LEFT_INTERNAL_WIDTH, SizeConstants.LEFT_WIDTH));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		eventListPanel.setLayout(new GridLayout(2, 1));
		
		btnCreateEvent = new WebButton	("Create a New Event");
		btnCreateMeeting = new WebButton("Create a New Meeting");
		btnModifyUser = new WebButton	("Modify Personal Info");
		btnEventList = new WebButton	("Modify an Event");
		
		WebButtonGroup leftCol = new WebButtonGroup(WebButtonGroup.VERTICAL, btnCreateEvent, btnModifyUser);
		WebButtonGroup rightCol = new WebButtonGroup(WebButtonGroup.VERTICAL, btnCreateMeeting, btnEventList);
		WebButtonGroup iconsGroup = new WebButtonGroup(true, leftCol, rightCol);
		buttonPanel.add(iconsGroup);
		
		container.add(createEventPanel);
		container.add(createMeetingPanel);
		container.add(changeUserDetailsPanel);
		container.add(eventListPanel);
		container.add(containerPanel);
		
		add(buttonPanel);
		add(scrollPane);
		
		noneVisible();
		
		addListeners();
	}
	
	
	private void addListeners() {
		btnCreateEvent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
				noneVisible();
				createEventPanel.setTime(null);
            	createEventPanel.setVisible(true);
            }
        });
		
		btnCreateMeeting.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
				noneVisible();
				createMeetingPanel.setTime(null);
        		createMeetingPanel.setVisible(true);
            }
        });
		
		btnModifyUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				noneVisible();
				changeUserDetailsPanel.setVisible(true);
			}
		});
		
		btnEventList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				noneVisible();
				eventListPanel.setVisible(true);
			}
		});
	}
	
	
	/**
	 * Show the create event panel with the passed in time already set.
	 * @param Calendar time: the start time of the event.
	 */
	public void setCreatePanel(Calendar time) {
		createEventPanel.setTime(time);
		createMeetingPanel.setTime(time);
		if (createMeetingPanel.isVisible()) {
			noneVisible();
			createMeetingPanel.setVisible(true);
		} else {
			noneVisible();
			createEventPanel.setVisible(true);
		}
	}
	
	
	/**
	 * Shows the passed in panel in the left panel.
	 * @param ScheduleMeetingListViewPanel panel to be displayed.
	 */
	public void setScheduleMeetingPanel(MeetingTimeSelectionPanel panel) {
		noneVisible();
		childPanel = panel;
		containerPanel.setVisible(true);
		containerPanel.add(panel);
	}
	
	
	/**
	 * Shows the passed in panel in the left panel.
	 * @param EventPanel (or subclass MeetingPanel) panel to be displayed.
	 */
	public void setEventPanel(EventPanel panel) {
		noneVisible();
		childPanel = panel;
		containerPanel.setVisible(true);
		containerPanel.add(panel);
	}
	
	
	/**
	 * Display the panel that allows for changing an event.
	 * @param event the event to be modified.
	 */
	public void setChangeEventPanel(final Event event) {
		noneVisible();
		containerPanel.setVisible(true);
		
		if (!event.isMeeting()) {
			ChangeEventDetailsPanel panel = new ChangeEventDetailsPanel(event);
			childPanel = panel;
			containerPanel.add(panel);
		} else {
			ChangeMeetingDetailsPanel changePanel = new ChangeMeetingDetailsPanel((Meeting) event);
			childPanel = changePanel;
			containerPanel.add(changePanel);
		}
	}
	
	
	/**
	 * Hide all panels and remove unneeded sub-panels.
	 */
	public void noneVisible() {
		refresh();
		createEventPanel.setVisible(false);
		createMeetingPanel.setVisible(false);
		changeUserDetailsPanel.setVisible(false);
		eventListPanel.setVisible(false);
		containerPanel.setVisible(false);
		containerPanel.removeAll();
	}


	@Override
	public void refresh() {
		createEventPanel.refresh();
		createMeetingPanel.refresh();
		changeUserDetailsPanel.refresh();
		eventListPanel.refresh();
		if (childPanel != null)
			childPanel.refresh();
	}
}
