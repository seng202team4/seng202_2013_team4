package gui.left.display;

import gui.interfaces.Colour;
import gui.interfaces.Refresh;
import gui.main.ParentFrame;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import client.ClientManager;

/**
 * A JPanel containing all the users attending a meeting.
 * @author Zack McGrath
 */
public class AttendeesPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = -6862804837925014411L;
	private ArrayList<String> attendees;
	private ArrayList<Boolean> accepted;
	private ArrayList<String[]> users;
	
	
	/**
	 * Create the panel.
	 */
	public AttendeesPanel(ArrayList<String> attendees, ArrayList<Boolean> accepted) {
		this.attendees = attendees;
		this.accepted = accepted;
		
		try {
			users = ClientManager.getClientManager().getAttendeeTriples(attendees);
		} catch (IOException e) {
			getRoot().disconnectionError();
			users = new ArrayList<>();
		}
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		refresh();
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	@Override
	public void refresh() {
		removeAll();
		
		int len = attendees.size();
		for (int i = 0; i < len; i++) {
			JPanel panel = new JPanel();
			JLabel attendee = new JLabel(users.get(i)[1] + " " + users.get(i)[2]);
			
			if (accepted.get(i)) {
				attendee.setForeground(Colour.GREEN_ACCEPTABLE);
			} else {
				attendee.setForeground(Colour.RED_UNACCEPTABLE);
			}
			
			panel.add(attendee);
			add(panel);
		}
		
		repaint();
		revalidate();
	}
}
