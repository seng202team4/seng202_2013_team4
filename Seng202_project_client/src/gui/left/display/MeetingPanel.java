package gui.left.display;

import gui.interfaces.Refresh;
import gui.main.ParentFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.Meeting;
import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;

/**
 * MeetingPanel extends EventPanel and adds an AttendeesPanel and a CommentsPanel.
 * @author Zack McGrath
 */
public class MeetingPanel extends EventPanel implements Refresh {
	private static final long serialVersionUID = 4508926101791031595L;
	
	private JPanel buttonPanel;
	private WebButton btnAcceptMeeting;
	private WebButton btnRejectMeeting;

	private AttendeesPanel attendeesPanel;
	private CommentsPanel commentsPanel;
	
	/**
	 * Create the panel.
	 */
	public MeetingPanel(Meeting meeting) {
		super(meeting);
		
		buttonPanel = new JPanel();
		
		btnAcceptMeeting = new WebButton("Accept Meeting");
		btnRejectMeeting = new WebButton("Reject Meeting");
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnAcceptMeeting, btnRejectMeeting);
		
		if (meeting != null) {
			boolean userNotAccepted = false;
			try {
				attendeesPanel = new AttendeesPanel(meeting.getAttendees(), meeting.getAccepted());
				commentsPanel = new CommentsPanel(meeting);

				
				
				userNotAccepted = !meeting.getAccepted().get(meeting.getAttendees().indexOf(
						ClientManager.getClientManager().getCurrentUser().getUsername()));
			} catch (ArrayIndexOutOfBoundsException e){
				// Changing stuff while it has not finished updating.
			}
			
			if (meeting.getInitiator().equals(ClientManager.getClientManager().getCurrentUser().getUsername())) {
				super.getModifyButton().setText("Modify Meeting");
				super.getDeleteButton().setText("Delete Meeting");
				buttonPanel.add(super.getModifyButton());
				buttonPanel.add(super.getDeleteButton());
			} else if (userNotAccepted) {
				buttonPanel.add(iconsGroup);
			}
			
			add(attendeesPanel);
			add(commentsPanel);
			add(buttonPanel);
			
			addListeners();
		}
	}


	private void addListeners() {
		btnAcceptMeeting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					ClientManager.getClientManager().acceptMeeting(event.getID());
					buttonPanel.setVisible(false);
				} catch (IOException ioe) {
					getRoot().disconnectionError();
				}
			}
		});
		
		btnRejectMeeting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					ClientManager.getClientManager().rejectMeeting(event.getID());
					buttonPanel.setVisible(false);
				} catch (IOException ioe) {
					getRoot().disconnectionError();
				}
			}
		});
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	@Override
	public void refresh() {
		attendeesPanel.refresh();
		commentsPanel.refresh();
	}
}
