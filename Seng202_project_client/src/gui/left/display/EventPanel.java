package gui.left.display;

import gui.interfaces.Refresh;
import gui.main.ParentFrame;
import gui.support.LabelPanel;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.CalendarHelper;
import shared.Event;
import shared.TimeConstants.TimeFormat;
import client.ClientManager;

/**
 * Displays an event.
 * @author Zack McGrath
 */
public class EventPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = 7829239086375456552L;
	
	private EventButton btnModify;
	private EventButton btnDelete;
	protected Event event;
	
	/**
	 * Create the panel.
	 */
	public EventPanel(Event e) {
		event = e;
		if (event != null) {
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			
			JPanel titlePanel = new JPanel();
			JPanel contianer = new JPanel();
			JPanel buttonPanel = new JPanel();
			
			contianer.setLayout(new GridLayout(3, 2));

			LabelPanel descriptionPanel = new LabelPanel("Description:", event.getDescription(), false);
			LabelPanel initiatorPanel = new LabelPanel("Initiator:", event.getInitiator(), false);
			LabelPanel locationPanel = new LabelPanel("Location:", event.getLocation(), false);
			LabelPanel startTimePanel = new LabelPanel("Start Time:", CalendarHelper.getDateTime(event.getStartTime()), false);
			LabelPanel endTimePanel = new LabelPanel("End Time:", CalendarHelper.getDateTime(event.getEndTime()), false);
			LabelPanel durationPanel = new LabelPanel("Duration:", CalendarHelper.minutesToString(event.getDuration(), TimeFormat.TWENTY_FOUR_HOUR), false);
			
			JLabel lblTitle = new JLabel(event.getName());
			lblTitle.setFont(new Font("Dialog", Font.BOLD, 20));	
			titlePanel.add(lblTitle);
			
			contianer.add(descriptionPanel);
			contianer.add(initiatorPanel);
			contianer.add(locationPanel);
			contianer.add(durationPanel);
			contianer.add(startTimePanel);
			contianer.add(endTimePanel);
			
			btnModify = new EventButton("Modify Event", event);
			btnDelete = new EventButton("Delete Event", event);
			buttonPanel.add(btnModify);
			buttonPanel.add(btnDelete);
			
			add(titlePanel);
			add(contianer);
			
			if (!event.isMeeting()) {
				add(buttonPanel);
			}
			
			addListeners();
		}
	}
	
	
	private void addListeners() {
		btnModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getRoot().getLeftPanel().setChangeEventPanel(((EventButton) e.getSource()).getEvent());
			}
		});
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					Event event = ((EventButton) actionEvent.getSource()).getEvent();
					ClientManager.getClientManager().deleteEvent(event.getID(), event.isMeeting());
					getRoot().getLeftPanel().noneVisible();
				} catch (IOException ioe) {
					getRoot().disconnectionError();
				}
			}
		});
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	public Event getEvent() {
		return event;
	}
	
	
	public EventButton getModifyButton() {
		return btnModify;
	}
	
	
	public EventButton getDeleteButton() {
		return btnDelete;
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
