package gui.left.display;

import gui.interfaces.NonEmptyFields;
import gui.interfaces.Refresh;
import gui.main.ParentFrame;
import gui.support.SupportPanel;
import gui.support.TextAreaPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.Comment;
import shared.Meeting;
import client.ClientManager;

import com.alee.laf.button.WebButton;

/**
 * Displays all the comments for a meeting.
 * @author Zack McGrath
 */
public class CommentsPanel extends JPanel implements Refresh, NonEmptyFields {
	private static final long serialVersionUID = 4407949349244988258L;
	private TextAreaPanel newComment;
	private WebButton btnSubmitComment;
	private Meeting meeting;
	private JPanel buttonPanel;
	
	/**
	 * Create the panel.
	 */
	public CommentsPanel(Meeting meeting) {
		this.meeting = meeting;
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		newComment = new TextAreaPanel("Create Comment", "");
		buttonPanel = new JPanel();
		
		btnSubmitComment = new WebButton("Submit Comment");
		buttonPanel.add(btnSubmitComment);
		
		refresh();
		
		addListeners();
	}


	private void addListeners() {
		btnSubmitComment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					ClientManager.getClientManager().createComment(meeting.getID(), newComment.getInput());
				} catch (IOException ioe) {
					getRoot().disconnectionError();
				}
			}
		});
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}


	@Override
	public ArrayList<SupportPanel> getNonEmptyPanels() {
		ArrayList<SupportPanel> array = new ArrayList<>();
		array.add(newComment);
		return array;
	}


	@Override
	public void initializeEmptyListeners() {
		for (SupportPanel p: getNonEmptyPanels()) {
			p.addEmtpyListener(this);
		}
		checkEmpty();
	}


	@Override
	public void checkEmpty() {
		boolean allFieldNotEmpty = true;
		for (SupportPanel p: getNonEmptyPanels()) {
			allFieldNotEmpty = allFieldNotEmpty && p.isFieldNotEmpty();
		}
		btnSubmitComment.setEnabled(allFieldNotEmpty);
	}
	
	
	@Override
	public void refresh() {
		removeAll();
		
		add(newComment);
		add(buttonPanel);
		
		ArrayList<Comment> comments = meeting.getComments();
		for (Comment c: comments) {
			add(new CommentPanel(c));
		}
		
		newComment.getField().requestFocusInWindow();
	}
}
