package gui.left.display;

import gui.interfaces.Refresh;
import gui.support.LabelPanel;

import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import shared.CalendarHelper;
import shared.Comment;

/**
 * Displays one comment and author and time stamp.
 * @author Zack McGrath
 */
public class CommentPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = -7187252646799478487L;
	
	
	/**
	 * Create the panel.
	 */
	public CommentPanel(Comment c) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		Calendar timeStamp = c.getTimeStamp();
		LabelPanel textPanel = new LabelPanel(
				c.getOwner() + 
				" | " + CalendarHelper.getDate(timeStamp) + 
				", " + CalendarHelper.getTime(timeStamp), 
				c.getComment(),
				true);
		
		add(textPanel);
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
