package gui.left.display;

import gui.interfaces.Refresh;
import shared.Event;

import com.alee.laf.button.WebButton;

/**
 * A button that has an event associated with it.
 * @author Zack McGrath
 */
public class EventButton extends WebButton implements Refresh {
	private static final long serialVersionUID = 1558605474001106989L;
	private Event event;
	
	/**
	 * Create the panel.
	 */
	public EventButton(String label, Event event) {
		super(label);
		this.event = event;
	}
	
	
	public Event getEvent() {
		return event;
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
