package gui.left.details;

import gui.interfaces.Refresh;

import java.awt.FlowLayout;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DateEditor;
import javax.swing.SpinnerDateModel;

import shared.CalendarHelper;
import shared.TimeConstants;

import com.toedter.calendar.JDateChooser;

/**
 * A container to hold date and time selection.
 * @author Zack McGrath
 */
public class TimeChooserPanel extends JPanel  implements Refresh, TimeConstants {
	private static final long serialVersionUID = 290046905737576596L;
	private JDateChooser dateChooser;
	private JSpinner timeSpinner;
	
	
	/**
	 * Create the panel.
	 */
	public TimeChooserPanel(String labelName, Calendar time) {
		setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		//time.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
		
		JLabel label = new JLabel(labelName);
		dateChooser = new JDateChooser();
		dateChooser.setDate(time.getTime());
		timeSpinner = new JSpinner(new SpinnerDateModel());
		DateEditor de_timeSpinner = new DateEditor(timeSpinner, "HH:mm");
		timeSpinner.setEditor(de_timeSpinner);
		timeSpinner.setValue(time.getTime());
		
		add(label);
		add(dateChooser);
		add(timeSpinner);
	}
	
	
	private Calendar date() {
		return dateChooser.getCalendar();
	}
	
	
	private Date time() {
		return (Date) timeSpinner.getValue();
	}
	
	
	public Calendar getDateTime() {
		Calendar date = (Calendar) date().clone();
		Calendar time = (Calendar) date.clone();
		time.setTime(time());
		
		date = CalendarHelper.getStartOfDay(date);
		
		date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
		date.set(Calendar.MINUTE, MIN_PER_TIME_SLOT * (time.get(Calendar.MINUTE) / MIN_PER_TIME_SLOT));
		
		return date;
	}
	
	
	public void setTime(Calendar time) {
		timeSpinner.setValue(time.getTime());
		dateChooser.setDate(time.getTime());
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
