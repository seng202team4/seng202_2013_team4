package gui.left.details;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Calendar;

import shared.CalendarHelper;
import shared.Meeting;

/**
 * A panel that contains a meeting details.
 * @author Zack McGrath
 */
public class MeetingDetailsPanel extends GeneralDetailsPanel implements Refresh {
	private static final long serialVersionUID = 6351737123735473931L;
	
	private TimeChooserPanel eariestTime;
	private TimeChooserPanel latestTime;
	private AddAttendeesPanel meetingAttendees;
	
	/**
	 * Create the panel.
	 */
	public MeetingDetailsPanel(Meeting meeting) {
		super(meeting);
		
		if (meeting == null) {
			setPreferredSize(new Dimension(SizeConstants.LEFT_INTERNAL_WIDTH, 550));
			
			Calendar now = (Calendar) CalendarHelper.roundToNextTimeSlot(Calendar.getInstance()).clone();
			now.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
			Calendar later = (Calendar) now.clone();
			later.add(Calendar.HOUR, HOUR_PER_WORK_DAY);
			eariestTime = new TimeChooserPanel("Eariest Time", now);
			latestTime = new TimeChooserPanel("Latest Time", later);
			
			add(eariestTime);
			add(latestTime);
		} else {
			setPreferredSize(new Dimension(SizeConstants.LEFT_INTERNAL_WIDTH, 400));
		}
		
		meetingAttendees = new AddAttendeesPanel(meeting);
		
		add(meetingAttendees);
	}
	
	
	/**
	 * @return the earliest time that a meeting can be held as a Calendar object.
	 */
	public Calendar getEarliestTime() {
		return (Calendar) eariestTime.getDateTime().clone();
	}
	
	
	/**
	 * @return the latest time that a meeting can be held as a Calendar object.
	 */
	public Calendar getLatestTime() {
		return (Calendar) latestTime.getDateTime().clone();
	}
	
	
	public ArrayList<String> getAttendeesList() {
		return meetingAttendees.getAttendeesList();
	}


	public void setTime(Calendar time) {
		if (time == null) {
			getEventTitle().getField().setText("");
			getLocationPanel().getField().setText("");
			getDescription().getField().setText("");
			getDurationSlider().setValue(MIN_PER_HOUR);
			meetingAttendees.refresh();
			
			Calendar now = Calendar.getInstance();
			eariestTime.setTime(now);
			now.add(Calendar.HOUR, HOUR_PER_WORK_DAY);
			latestTime.setTime(now);
		} else {
			eariestTime.setTime(time);
			time.add(Calendar.HOUR, HOUR_PER_WORK_DAY);
			latestTime.setTime(time);
		}
	}


	@Override
	public void refresh() {
		super.refresh();
		repaint();
		revalidate();
	}
}
