package gui.left.details;

import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;

/**
 * Extends TransferHandler.
 * Overwrites getSourceActions, createTransferable, exportDone, canImport, importData
 * @author Zack McGrath
 */
class ListTransferHandler extends TransferHandler {
	private static final long serialVersionUID = 6334100032924821573L;
	private static final int NULL = -1;
	private int action = MOVE;
	private JList<String> dragFromJList;
	private DefaultListModel<String> fromListModel;
	
	
	/**
	 * 
	 * @param dragFromJList the origin JList.
	 * @param fromListModel the ListModel associated with the origin JList.
	 */
	public ListTransferHandler(JList<String> dragFromJList, DefaultListModel<String> fromListModel) {
		this.dragFromJList = dragFromJList;
		this.fromListModel = fromListModel;
	}
	
	
	private int getIndex() {
		int index = dragFromJList.getSelectedIndex();
		if (index < 0 || index >= fromListModel.getSize()) {
			return NULL;
		}
		return index;
	}
	
	
	public int getSourceActions(JComponent comp) {
		return action;
	}
	
	
	public Transferable createTransferable(JComponent comp) {
		int index = getIndex();
		if (index == NULL)
			return null;
		return new StringSelection((String) dragFromJList.getSelectedValue());
	}
	
	
	public void exportDone(JComponent source, Transferable data, int action) {
		int index = getIndex();
		if (index != NULL) {
			fromListModel.removeElementAt(index);
		}
	}
	
	
	public boolean canImport(TransferHandler.TransferSupport support) {
		boolean actionSupported = (action & support.getSourceDropActions()) == action;
		boolean dataSupported = support.isDataFlavorSupported(DataFlavor.stringFlavor);
		// Only support drops and only import Strings
		if (actionSupported && support.isDrop() && dataSupported) {
			support.setDropAction(action);
			return true;
		}
		return false;
	}
	
	
	public boolean importData(TransferHandler.TransferSupport support) {
		String data;
		JList.DropLocation dropLoc = (JList.DropLocation) support.getDropLocation();
		int index = dropLoc.getIndex();
		
		if (canImport(support) == false) {
			return false;
		}
		// Try to get the data and bail if this fails
		try {
			data = (String) support.getTransferable().getTransferData(DataFlavor.stringFlavor);
		} catch (UnsupportedFlavorException ufe) {
			return false;
		} catch (IOException ioe) {
			return false;
		}

		@SuppressWarnings("unchecked")
		JList<String> list = (JList<String>) support.getComponent();
		DefaultListModel<String> model = (DefaultListModel<String>) list.getModel();
		model.insertElementAt(data, index);

		Rectangle rect = list.getCellBounds(index, index);
		list.scrollRectToVisible(rect);
		list.setSelectedIndex(index);
		list.requestFocusInWindow();

		return true;
	}  
}