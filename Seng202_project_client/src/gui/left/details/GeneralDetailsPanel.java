package gui.left.details;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;
import gui.support.TextAreaPanel;
import gui.support.TextFieldPanel;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSlider;

import shared.Event;
import shared.TimeConstants;

/**
 * JPanel to contain all the fields needed in the creation of an event and meeting.
 * @author Zack McGrath
 */
public class GeneralDetailsPanel extends JPanel  implements Refresh, TimeConstants {
	private static final long serialVersionUID = -7437104312047954727L;

	private TextFieldPanel eventTitlePanel;
	private TextFieldPanel locationPanel;
	private TextAreaPanel eventDescriptionPanel;
	private SilderPanel eventDurationPanel;
	
	
	/**
	 * Create the panel.
	 */
	public GeneralDetailsPanel(Event event) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setPreferredSize(new Dimension(SizeConstants.LEFT_INTERNAL_WIDTH, 300));
		
		String title = event == null ? "" : event.getName();
		String loc = event == null ? "" : event.getLocation();
		String desc = event == null ? "" : event.getDescription();
		int duration = event == null ? MIN_PER_HOUR : event.getDuration();
		
		eventTitlePanel = new TextFieldPanel("Title", title);
		locationPanel = new TextFieldPanel("Location", loc);
		eventDescriptionPanel = new TextAreaPanel("Description", desc);
		eventDurationPanel = new SilderPanel(duration);
		
		add(eventTitlePanel);
		add(locationPanel);
		add(eventDescriptionPanel);
		if (event == null || !event.isMeeting())
			add(eventDurationPanel);
	}
	
	public TextFieldPanel getEventTitle() {
		return eventTitlePanel;
	}
	
	
	public TextFieldPanel getLocationPanel() {
		return locationPanel;
	}
	
	
	public TextAreaPanel getDescription() {
		return eventDescriptionPanel;
	}
	
	
	public JSlider getDurationSlider() {
		return eventDurationPanel.getDurationSlider();
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
