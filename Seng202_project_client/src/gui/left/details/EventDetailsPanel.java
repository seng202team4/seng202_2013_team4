package gui.left.details;

import gui.interfaces.Refresh;

import java.util.Calendar;

import shared.CalendarHelper;
import shared.Event;

/**
 * A panel that contains a event details.
 * @author Zack McGrath
 */
public class EventDetailsPanel extends GeneralDetailsPanel  implements Refresh {
	private static final long serialVersionUID = -2313371358394456056L;
	private TimeChooserPanel statTime;
	
	
	/**
	 * Create the panel.
	 * @param event the event to be displayed, if null fields will be blank.
	 */
	public EventDetailsPanel(Event event) {
		super(event);
		
		Calendar now = (Calendar) CalendarHelper.roundToNextTimeSlot(Calendar.getInstance()).clone();
		statTime = new TimeChooserPanel("Start Time", event == null ? now : event.getStartTime());
		add(statTime);
	}
	
	
	public Calendar getStartTime() {
		return statTime.getDateTime();
	}
	
	
	public void setTime(Calendar time) {
		if (time == null) {
			getEventTitle().getField().setText("");
			getLocationPanel().getField().setText("");
			getDescription().getField().setText("");
			getDurationSlider().setValue(MIN_PER_HOUR);
			statTime.setTime(Calendar.getInstance());
		} else {
			statTime.setTime(time);
		}
	}
	
	
	@Override
	public void refresh() {
		super.refresh();
		repaint();
		revalidate();
	}
}
