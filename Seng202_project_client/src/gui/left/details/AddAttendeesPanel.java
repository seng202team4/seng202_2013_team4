package gui.left.details;

import gui.interfaces.Refresh;
import gui.main.ParentFrame;

import java.awt.GridLayout;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import shared.Meeting;
import client.ClientManager;

/**
 * AddAttendeesPanel is a JPanel that contains three JList:
 * 		- a full list of all the users that are not the current user
 * 		- a list of users who are essential to a meeting
 * 		- a list of users who are optional to a meeting
 * Usernames can be dragged between these three JLists.
 * 
 * @author Zack McGrath
 */
public class AddAttendeesPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = 3993788096007304719L;
	
	private Meeting meeting;
	
	private DefaultListModel<String> allUsersListModel;
	private DefaultListModel<String> attendeesListModel;
	
	private JList<String> allUsersList;
	private JList<String> attendeesList;
	
	
	/**
	 * Create the panel.
	 * @param event 
	 */
	public AddAttendeesPanel(Meeting meeting) {
		this.meeting = meeting;
		refresh();
	}
	
	
	@Override
	public void refresh() {
		removeAll();
		
		setLists();
		
		allUsersList = new JList<String>(allUsersListModel);
		attendeesList = new JList<String>(attendeesListModel);
		
		allUsersList.setTransferHandler(new ListTransferHandler(allUsersList, allUsersListModel));
		attendeesList.setTransferHandler(new ListTransferHandler(attendeesList, attendeesListModel));
		
		allUsersList.setDropMode(DropMode.INSERT);
		attendeesList.setDropMode(DropMode.INSERT);
		
		allUsersList.setDragEnabled(true);
		attendeesList.setDragEnabled(true);
		
		allUsersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		attendeesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JPanel allUsersPanel = new JPanel();
		JPanel addedUsersPanel = new JPanel();
		
		JScrollPane allUsersSP = new JScrollPane(allUsersList);
		JScrollPane essentialUsersSP = new JScrollPane(attendeesList);
		
		JLabel allUsersLabel = new JLabel("Drag users from here:");
		JLabel essentialUsersLabel = new JLabel("Attendees:");
				
		allUsersLabel.setAlignmentX(0f);
		essentialUsersLabel.setAlignmentX(0f);
		allUsersSP.setAlignmentX(0f);
		essentialUsersSP.setAlignmentX(0f);
		
		allUsersPanel.add(allUsersLabel);
		allUsersPanel.add(allUsersSP);
		
		addedUsersPanel.add(essentialUsersLabel);
		addedUsersPanel.add(essentialUsersSP);
		
		setLayout(new GridLayout(1, 2));
		allUsersPanel.setLayout(new BoxLayout(allUsersPanel, BoxLayout.Y_AXIS));
		addedUsersPanel.setLayout(new BoxLayout(addedUsersPanel, BoxLayout.Y_AXIS));
		addedUsersPanel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
		
		add(allUsersPanel);
		add(addedUsersPanel);
	}
	
	
	private void setLists() {
		allUsersListModel = new DefaultListModel<String>();
		attendeesListModel = new DefaultListModel<String>();
		
		ArrayList<String[]> userList;
		
		try {
			userList = ClientManager.getClientManager().getOtherUsersList();
		} catch (IOException ioe) {
			getRoot().disconnectionError();
			userList = new ArrayList<>();
		}
		
		if (meeting == null) {
			for (String[] user: userList) {
				allUsersListModel.addElement(user[0] + ", " + user[1] + " " + user[2]);
			}
		} else {
			for (String[] user: userList) {
				boolean attending = false;
				for (String username: meeting.getAttendees()) {
					if (user[0].equals(username)) {
						attending = true;
					}
				}
				
				if (attending) {
					attendeesListModel.addElement(user[0] + ", " + user[1] + " " + user[2]);
				} else {
					allUsersListModel.addElement(user[0] + ", " + user[1] + " " + user[2]);	
				}
			}
		}
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	private ArrayList<String> getUserNames(ListModel<String> users) {
		ArrayList<String> usernames = new ArrayList<>();
		int len = users.getSize();
		for (int i = 0; i < len; i++) {
			usernames.add(users.getElementAt(i).split(",")[0]);
		}
		return usernames;
	}
	
	
	/**
	 * @return an ArrayList of usernames of the user who are invited to a 
	 * meeting but are not essential to the meeting.
	 */
	public ArrayList<String> getAttendeesList() {
		ListModel<String> optional = attendeesList.getModel();
		return getUserNames(optional);
	}
}
