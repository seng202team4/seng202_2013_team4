package gui.left.details;

import gui.interfaces.Refresh;

import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import shared.CalendarHelper;
import shared.TimeConstants;

/**
 * A container top hold a JSlier and it's label.
 * @author Zack McGrath
 */
public class SilderPanel extends JPanel  implements Refresh, TimeConstants {
	private static final long serialVersionUID = -8231435838740584661L;
	private JSlider durationSlider;
	private JLabel lblLength;

	public SilderPanel(int initialMinute) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel lblDuration = new JLabel("Duration");
		durationSlider = new JSlider();
		lblLength = new JLabel(CalendarHelper.minutesToString(initialMinute, TimeFormat.TWENTY_FOUR_HOUR));

		lblDuration.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblLength.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		durationSlider.setPaintTicks(true);
		durationSlider.setSnapToTicks(true);
		durationSlider.setMinorTickSpacing(MIN_PER_TIME_SLOT);
		durationSlider.setMaximum(MIN_PER_DAY);
		durationSlider.setMinimum(MIN_PER_TIME_SLOT);
		durationSlider.setValue(initialMinute);
		
		add(lblDuration);
		add(durationSlider);
		add(lblLength);
		
		addListeners();
	}


	private void addListeners() {
		durationSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSlider slider = (JSlider) e.getSource();
                lblLength.setText(CalendarHelper.minutesToString(slider.getValue(), TimeFormat.TWENTY_FOUR_HOUR));
            }
        });
	}
	
	
	public JSlider getDurationSlider() {
		return durationSlider;
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}