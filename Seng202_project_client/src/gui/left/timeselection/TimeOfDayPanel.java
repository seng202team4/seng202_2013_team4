package gui.left.timeselection;

import gui.interfaces.Colour;
import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import shared.CalendarHelper;
import shared.TimeConstants;

/**
 * Displays the times.
 * @author Zack McGrath
 */
public class TimeOfDayPanel extends JPanel implements Refresh, TimeConstants {
	private static final long serialVersionUID = -8297872183109094381L;
	
	
	public TimeOfDayPanel() {
		setLayout(new GridLayout(TIME_SLOTS_PER_DAY, 1));
		refresh();
	}
	
	
	private void addTime(int timeSlot) {
		JPanel panel = new JPanel();
		
		panel.setPreferredSize(new Dimension(SizeConstants.COLUMN_WIDTH, SizeConstants.TEXT_HEIGHT));
		if (timeSlot % TIME_SLOTS_PER_HOUR == 0) {
			panel.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Colour.BLACK));
			JLabel timeLabel = new JLabel(CalendarHelper.minutesToString(MIN_PER_TIME_SLOT * timeSlot, TimeFormat.TWELVE_HOUR));
			panel.add(timeLabel);
		}
		
		add(panel);
	}


	@Override
	public void refresh() {
		removeAll();
		
		for (int timeSlot = 0; timeSlot < TIME_SLOTS_PER_DAY; timeSlot++) {
			addTime(timeSlot);
		}
		
		repaint();
		revalidate();
	}
}
