package gui.left.timeselection;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JPanel;

import shared.TimeConstants;

/**
 * Shows all the events a user has for one day.
 * @author Zack McGrath
 */
public class DayPanelTimeSelection extends JPanel implements Refresh, TimeConstants {
	private static final long serialVersionUID = 7600709745830429644L;
	
	private Calendar date;
	private TimeSlotPanel[] timeSlotPanels = new TimeSlotPanel[TIME_SLOTS_PER_DAY];
	private ArrayList<ArrayList<String[]>> usersBusy;
	private boolean eventOverlapsDays = false;
	private MeetingTimeSelectionPanel parentPanel;
	
	
	/**
	 * Create the panel.
	 */
	public DayPanelTimeSelection(Calendar date, ArrayList<ArrayList<String[]>> usersBusy,
			MeetingTimeSelectionPanel parentPanel) {
		this.date = date;
		this.usersBusy = usersBusy;
		this.parentPanel = parentPanel;
		
		setLayout(new GridLayout(TIME_SLOTS_PER_DAY, 1));
		
		refresh();
	}
	
	
	private void addTimeSlotPanel(int timeSlot, ArrayList<String[]> usersBusy) {
		Calendar time = (Calendar) date.clone();
		time.add(Calendar.MINUTE, timeSlot * MIN_PER_TIME_SLOT);
		
		TimeSlotPanel emptyPanel = new TimeSlotPanel(time, usersBusy, parentPanel);
		emptyPanel.setPreferredSize(new Dimension(SizeConstants.COLUMN_WIDTH, SizeConstants.TEXT_HEIGHT));
		
		timeSlotPanels[timeSlot] = emptyPanel;
		add(emptyPanel);
	}
	
	
	/**
	 * @return an ArrayList containing all the empty panels.
	 */
	public TimeSlotPanel[] getEmptyPanels() {
		return timeSlotPanels;
	}
	
	
	public Calendar getDate() {
		return date;
	}
	
	
	/**
	 * @return true it an event lays on two days.
	 */
	public boolean eventOverLaps() {
		return eventOverlapsDays;
	}


	public TimeSlotPanel[] getTimSlotPanels() {
		return timeSlotPanels;
	}
	
	
	@Override
	public void refresh() {
		removeAll();
		timeSlotPanels = new TimeSlotPanel[TIME_SLOTS_PER_DAY];
			
		for (int timeSlot = 0; timeSlot < TIME_SLOTS_PER_DAY; timeSlot++) {
			addTimeSlotPanel(timeSlot, usersBusy.get(timeSlot));
		}
		
		repaint();
		revalidate();
	}
}
