package gui.left.timeselection;

import exceptions.InvalidTimeslotException;
import gui.interfaces.Colour;
import gui.interfaces.Refresh;
import gui.main.ParentFrame;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import client.ClientManager;

/**
 * A panel that show how many attendees are busy in a specific time slot.
 * @author Zack McGrath
 */
public class TimeSlotPanel extends JPanel implements Refresh {
	private static final long serialVersionUID = 4909566751321913127L;
	private Calendar date;
	private ArrayList<String[]> usersBusy;
	private MeetingTimeSelectionPanel parentPanel;
	private boolean isUserInUsersBusy = false;
	
	
	/**
	 * Create the panel.
	 * @param date a Calendar object that represents the start of the time slot.
	 * @param usersBusy an ArrayList of tuples of each of the users who are 
	 * 		busy in the time slot.
	 */
	public TimeSlotPanel(Calendar date, ArrayList<String[]> usersBusy,
			MeetingTimeSelectionPanel parentPanel) {
		this.date = date;
		this.usersBusy = usersBusy;
		this.parentPanel = parentPanel;
		
		setBorder(new LineBorder(Colour.LIGHT_GRAY));
		
		String listOfUsers = usersBusy.size() + " user are busy: ";
		for (int i = 0; i < usersBusy.size(); i++) {
			String[] user = usersBusy.get(i);
			
			if (ClientManager.getClientManager().getCurrentUser().getUsername().equals(user[0])) {
				isUserInUsersBusy = true;
			}
			
			if (i != usersBusy.size() - 1) {
				listOfUsers += user[1] + " " + user[2] + ", ";
			} else {
				listOfUsers += user[1] + " " + user[2];
			}
		}
		
		if (isUserInUsersBusy) {
			setBackground(Colour.LIGHT_GRAY);
		} else if (usersBusy.size() == 0) {
			setToolTipText("No users are busy.");
			setBackground(Colour.LIGHT_BLUE);
			addListeners();
		} else {
			setToolTipText(listOfUsers);
			setBackground(Colour.RED_ORANGE);
			addListeners();
		}
	}
	
	
	private void addListeners() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				TimeSlotPanel source = (TimeSlotPanel) e.getSource();
				ParentFrame root = getRoot();
				if (!isUserInUsersBusy) {
					try {
						createMeeting(source);
						root.getLeftPanel().noneVisible();
					} catch (IOException ioe) {
						root.disconnectionError();
					} catch (InvalidTimeslotException itse) {
						parentPanel.setErrorMessage("Invaild time slot");
					}
				}
			}
		});
	}
	
	
	private void createMeeting(TimeSlotPanel source) throws IOException, InvalidTimeslotException {
		ArrayList<String> attendees = new ArrayList<>();
		
		boolean isUserBusy = false;
		for (String[] user: source.getUsersBusy()) {
			isUserBusy = false;
			for (String username: parentPanel.getAttendees()) {
				if (username.equals(user[0])) {
					isUserBusy = true;
				}
			}
			if (!isUserBusy) {
				//Adds only the non-busy users to a list.
				attendees.add(user[0]);
			}
		}
		
		ClientManager.getClientManager().createMeeting(
				parentPanel.getName(),
				parentPanel.getEventLocation(), 
				parentPanel.getDescription(), 
				parentPanel.getDuration(),
				source.getDate(),
				attendees);
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	public Calendar getDate() {
		return date;
	}
	
	
	public ArrayList<String[]> getUsersBusy() {
		return usersBusy;
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
