package gui.left.timeselection;

import gui.interfaces.Colour;
import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;
import gui.support.ErrorPanel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.ClientManager;

import shared.CalendarHelper;
import shared.TimeConstants;
import shared.User;

/**
 * Displays a time selection panel for a meeting.
 * @author Zack McGrath
 */
public class MeetingTimeSelectionPanel extends JPanel implements Refresh, TimeConstants {
	private static final long serialVersionUID = -7532650391749444009L;
	private ArrayList<DayPanelTimeSelection> dayPanelsList;
	private ArrayList<JPanel> dayNamesList;
	private ErrorPanel error;
	private JPanel daysPanel;
	private JPanel dayNamesPanel;
	
	private int numDays;
	
	private String name;
	private String eventLocation;
	private String description;
	private int duration;
	private Calendar startOfWindow;
	private Calendar endOfWindow;
	private ArrayList<String> attendees;
	private ArrayList<ArrayList<String[]>> attendanceList;
	
	
	/**
	 * Create the panel.
	 */
	public MeetingTimeSelectionPanel(
			String name,
			String location,
            String description,
            int duration,
            Calendar windowStart, 
			Calendar windowEnd,
			ArrayList<String> attendees,
			ArrayList<ArrayList<String[]>> attendanceList) {
		
		this.name = name;
		this.eventLocation= location; 
        this.description = description; 
        this.duration = duration;
        this.startOfWindow = windowStart;
		this.endOfWindow = windowEnd;
		this.attendees = attendees;
		this.attendanceList = attendanceList;
		
		numDays = CalendarHelper.getDifference(windowStart, windowEnd, Format.DAYS) + 1;
		
		dayNamesPanel = new JPanel();
		daysPanel = new JPanel();
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		dayNamesPanel.setLayout(new GridLayout(0, numDays + 1, 0, 0));
		daysPanel.setLayout(new GridLayout(0, numDays + 1, 0, 0));
		dayNamesPanel.setMaximumSize(new Dimension(SizeConstants.MAX_SIZE, SizeConstants.TEXT_HEIGHT));
		
		dayPanelsList = new ArrayList<>();
		dayNamesList = new ArrayList<>();
		
		setUp();
		
		error = new ErrorPanel();
		JScrollPane scrollPaneDay = new JScrollPane(dayNamesPanel);
		JScrollPane scrollPaneCal = new JScrollPane(daysPanel);
		
		add(error);
		add(scrollPaneDay);
		add(scrollPaneCal);
	}
	
	
	public void setUp() {
		daysPanel.removeAll();
		dayNamesPanel.removeAll();
		dayNamesList = new ArrayList<>(); 
		
		ArrayList<Calendar> days = new ArrayList<>();
		
		Calendar startOfFirstDay = CalendarHelper.getStartOfDay(startOfWindow);
		Calendar endOfLastDay = CalendarHelper.getStartOfNextDay(endOfWindow);
		
		int numSlotsBefore = CalendarHelper.getDifference(startOfFirstDay, startOfWindow, Format.TIME_SLOTS);
		int numSlotsAfter = CalendarHelper.getDifference(endOfWindow, endOfLastDay, Format.TIME_SLOTS) + 100;
		
		User currentUser = ClientManager.getClientManager().getCurrentUser();
		String[] triple = {currentUser.getUsername(), currentUser.getFirstname(), currentUser.getLastName()};
		ArrayList<String[]> noUsers = new ArrayList<>();
		noUsers.add(triple);
		
		for (int i = 0; i < numSlotsBefore; i++)
			attendanceList.add(0, noUsers);
		
		for (int i = 0; i < numSlotsAfter; i++)
			attendanceList.add(noUsers);
		
		days.add(startOfFirstDay);
		for (int i = 1; i < numDays; i++) {
			Calendar nextDay = (Calendar) days.get(i-1).clone();
			nextDay.add(Calendar.DATE, 1);
			days.add(nextDay);
		}
		
		TimeOfDayPanel times = new TimeOfDayPanel();
		dayNamesPanel.add(new JPanel());
		daysPanel.add(times);
		
		for (int iDay = 0; iDay < numDays; iDay++) {
			Calendar day = days.get(iDay);
			ArrayList<ArrayList<String[]>> array = new ArrayList<>();
			for (int timeSlot = iDay * TIME_SLOTS_PER_DAY; timeSlot < (iDay + 1) * TIME_SLOTS_PER_DAY; timeSlot++) {
				ArrayList<String[]> item = attendanceList.get(timeSlot);
				array.add(item);
			}
			
			createDayName(day);
			createDayPanel(day, array);
		}
	}
	
	
	private void createDayPanel(Calendar day, ArrayList<ArrayList<String[]>> array) {
		DayPanelTimeSelection dayPanel = new DayPanelTimeSelection(day, array, this);
		dayPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Colour.BLACK));
		
		daysPanel.add(dayPanel);
		dayPanelsList.add(dayPanel);
	}
	
	
	private void createDayName(Calendar day) {
		JPanel dayNamePanel = new JPanel();
		JLabel dayName = new JLabel(CalendarHelper.getDayOfWeek(day) + " " + day.get(Calendar.DAY_OF_MONTH));
		dayName.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		dayNamePanel.add(dayName);
		dayNamesPanel.add(dayNamePanel);
		dayNamesList.add(dayNamePanel);
	}


	public void setErrorMessage(String message) {
		error.setErrorMessage(message);
	}
	
	
	public ArrayList<TimeSlotPanel> getTimeSlotPanels() {
		ArrayList<TimeSlotPanel> array = new ArrayList<>();
		
		for (DayPanelTimeSelection day: dayPanelsList) {
			for (TimeSlotPanel timeslot: day.getTimSlotPanels()) {
				array.add(timeslot);
			}
		}
		
		return array;
	}
	
	
	public TimeSlotPanel[] getEmptyPanels(int day) {
		day = (day % DAYS_PER_WEEK);
		return dayPanelsList.get(day).getEmptyPanels(); 
	}
	
	
	public String getName() {
		return name;
	}


	public String getEventLocation() {
		return eventLocation;
	}


	public String getDescription() {
		return description;
	}


	public int getDuration() {
		return duration;
	}


	public ArrayList<String> getAttendees() {
		return attendees;
	}


	public ArrayList<ArrayList<String[]>> getAttendanceList() {
		return attendanceList;
	}


	public void setAttendanceList(ArrayList<ArrayList<String[]>> attendanceList) {
		this.attendanceList = attendanceList;
	}
	
	
	@Override
	public void refresh() {
		for (JPanel p: dayNamesList) {
			p.repaint();
			p.revalidate();
		}
		for (DayPanelTimeSelection p: dayPanelsList) {
			p.refresh();
		}
	}
}
