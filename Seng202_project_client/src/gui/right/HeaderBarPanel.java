package gui.right;

import java.awt.FlowLayout;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.ClientManager;

import com.alee.extended.panel.WebButtonGroup;
import com.alee.laf.button.WebButton;
import com.toedter.calendar.JDateChooser;

/**
 * A JPanel to contain the current logged in user and a logout button.
 * And change display week button.
 * @author Zack McGrath
 */
public class HeaderBarPanel extends JPanel {
	private static final long serialVersionUID = -8103992839539571383L;
	private WebButton btnLogout;
	private WebButton btnPreviousWeek;
	private WebButton btnToday;
	private WebButton btnNextWeek;
	private JDateChooser dateChooser;
	
	
	/**
	 * Create the panel.
	 */
	public HeaderBarPanel() {		
		
		JPanel changeWeekPanel = new JPanel();
		JPanel userPanel = new JPanel();
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		userPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		changeWeekPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		dateChooser = new JDateChooser();
		dateChooser.setDate(Calendar.getInstance().getTime());
		
		btnPreviousWeek = new WebButton	(" <<  ");
		btnToday = new WebButton		("Today");
		btnNextWeek = new WebButton		("  >> ");
		
		WebButtonGroup iconsGroup = new WebButtonGroup(true, btnPreviousWeek, btnToday, btnNextWeek);
		
		changeWeekPanel.add(iconsGroup);
		changeWeekPanel.add(dateChooser);
		
		JLabel lblUserName = new JLabel(ClientManager.getClientManager().getCurrentUser().getUsername());
		btnLogout = new WebButton("Logout");
		
		userPanel.add(lblUserName);
		userPanel.add(btnLogout);
		
		add(changeWeekPanel);
		add(userPanel);
	}
	
	
	public WebButton getLogoutButton() {
		return btnLogout;
	}
	
	
	public WebButton getPreviousWeekButton() {
		return btnPreviousWeek;
	}
	
	
	public WebButton getTodayButton() {
		return btnToday;
	}
	
	
	public WebButton getNextWeekButton() {
		return btnNextWeek;
	}
	
	
	public JDateChooser getDateChooser() {
		return dateChooser;
	}
}
