package gui.right.schedule;

import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.Calendar;

import shared.CalendarHelper;
import shared.Event;
import shared.TimeConstants;
import client.ClientManager;

/**
 * Shows all the events a user has for one day.
 * @author Zack McGrath
 */
public class DayPanel extends ColumnPanel implements TimeConstants {
	private static final long serialVersionUID = 7600709745830429644L;
	
	private Calendar date;
	private ArrayList<TimePeriodPanel> eventPanels = new ArrayList<>();
	private EmptyPanel[] emptyPanels = new EmptyPanel[TIME_SLOTS_PER_DAY];
	private ArrayList<Integer> emptySlots = new ArrayList<>();
	private Calendar startOfDay;
	private Calendar endOfDay;
	
	
	/**
	 * Create the panel.
	 * @param the date to be displayed
	 */
	public DayPanel(Calendar d) {
		date = d;
		
		startOfDay = CalendarHelper.getStartOfDay(date);
		endOfDay = CalendarHelper.getStartOfNextDay(date);
		
		refresh();
	}
	
	
	private void addEmptyPanel(int timeSlot) {
		GridBagConstraints gbc_emptyPanel = new GridBagConstraints();
		gbc_emptyPanel.fill = GridBagConstraints.BOTH;
		gbc_emptyPanel.gridx = 0;
		gbc_emptyPanel.gridy = timeSlot;
		
		Calendar time = (Calendar) date.clone();
		time.add(Calendar.MINUTE, timeSlot * MIN_PER_TIME_SLOT);
		
		EmptyPanel emptyPanel = new EmptyPanel(time);
		
		emptyPanels[timeSlot] = emptyPanel;
		add(emptyPanel, gbc_emptyPanel);
	}
	
	
	public void addEvent(Event event) {
		int duration = event.getDuration() / MIN_PER_TIME_SLOT;
		int fromDayStartToEventStart = CalendarHelper.getDifference(startOfDay, event.getStartTime(), Format.TIME_SLOTS);
		int tillEndOfDay = CalendarHelper.getDifference(event.getStartTime(), endOfDay, Format.TIME_SLOTS);
		
		if (fromDayStartToEventStart == 0) {
			duration = CalendarHelper.getDifference(startOfDay, event.getEndTime(), Format.TIME_SLOTS);
			tillEndOfDay = CalendarHelper.getDifference(startOfDay, endOfDay, Format.TIME_SLOTS);
		}
		
		if (duration > tillEndOfDay) {
			duration = tillEndOfDay;
		}
		
		GridBagConstraints gbc_timePeriodPanel = new GridBagConstraints();
		gbc_timePeriodPanel.fill = GridBagConstraints.BOTH;
		gbc_timePeriodPanel.gridheight = duration;
		gbc_timePeriodPanel.gridx = 0;
		gbc_timePeriodPanel.gridy = fromDayStartToEventStart;
		
		TimePeriodPanel panel = new TimePeriodPanel(event);
		
		for (Integer i = fromDayStartToEventStart; i < fromDayStartToEventStart + duration; i++) {
			emptySlots.remove(i);
			emptyPanels[i] = null;
		}
		
		eventPanels.add(panel);
		add(panel, gbc_timePeriodPanel);
	}

	
	/**
	 * @return an ArrayList containing all the event panels.
	 */
	public ArrayList<TimePeriodPanel> getTimePeriodPanels() {
		return eventPanels;
	}
	
	
	/**
	 * @return an ArrayList containing all the empty panels.
	 */
	public EmptyPanel[] getEmptyPanels() {
		return emptyPanels;
	}
	
	
	public ArrayList<Integer> getEmptySlots() {
		return emptySlots;
	}
	
	
	public Calendar getDate() {
		return date;
	}
	
	
	@Override
	public void refresh() {
		removeAll();
		emptyPanels = new EmptyPanel[TIME_SLOTS_PER_DAY];
		emptySlots = new ArrayList<>();
		
		for (int i = 0; i < TIME_SLOTS_PER_DAY; i++)
			emptySlots.add(i);
		
		for (Event event: ClientManager.getClientManager().getDayEvents(date))			
			addEvent(event);
		
		for (Integer timeSlot: emptySlots)
			addEmptyPanel(timeSlot);
		
		repaint();
		revalidate(); 
	}
}
