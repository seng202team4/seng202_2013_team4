package gui.right.schedule;

import gui.interfaces.Colour;

import java.awt.GridBagConstraints;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import shared.CalendarHelper;
import shared.TimeConstants;

/**
 * Shows all the events a user has for one day.
 * @author Zack McGrath
 */
public class TimeOfDayPanel extends ColumnPanel implements TimeConstants {
	private static final long serialVersionUID = 7600709745830429644L;
	
	
	/**
	 * Create the panel.
	 */
	public TimeOfDayPanel() {
		refresh();
	}
	
	
	private void addTime(int timeSlot) {
		GridBagConstraints gbc_timeLabel = new GridBagConstraints();
		gbc_timeLabel.fill = GridBagConstraints.BOTH;
		gbc_timeLabel.gridheight = TIME_SLOTS_PER_HOUR;
		gbc_timeLabel.gridx = 0;
		gbc_timeLabel.gridy = timeSlot;
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Colour.LIGHT_GRAY));
		JLabel timeLabel = new JLabel(CalendarHelper.minutesToString(MIN_PER_TIME_SLOT * timeSlot, TimeFormat.TWELVE_HOUR));
		panel.add(timeLabel);
		
		add(panel, gbc_timeLabel);
	}
	
	
	@Override
	public void refresh() {
		removeAll();
		
		for (int timeSlot = 0; timeSlot < TIME_SLOTS_PER_DAY; timeSlot += TIME_SLOTS_PER_HOUR) {
			addTime(timeSlot);
		}
		
		repaint();
		revalidate();
	}
}
