package gui.right.schedule;

import gui.interfaces.Colour;
import gui.main.ParentFrame;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

import shared.TimeConstants;

/**
 * A panel that does not contain an event in the calendar.
 * @author Zack McGrath
 */
public class EmptyPanel extends JPanel implements TimeConstants {
	private static final long serialVersionUID = 4909566751321913127L;
	Calendar date;
	
	
	/**
	 * Create the panel.
	 */
	public EmptyPanel(Calendar date) {
		this.date = date;
		setBackground(Colour.WHITE);
		setBorder(new LineBorder(Colour.LIGHT_GRAY));
		if (date.get(Calendar.MINUTE) % MIN_PER_HOUR == 0) { 
			setBorder(new CompoundBorder(
				    BorderFactory.createMatteBorder(1, 0, 0, 0, Colour.BLACK),
				    BorderFactory.createMatteBorder(0, 1, 1, 1, Colour.LIGHT_GRAY)));
		}

		addListeners();
	}


	private void addListeners() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				EmptyPanel panel = (EmptyPanel) mouseEvent.getSource();
				Calendar time = (Calendar) panel.getDate().clone();
				getRoot().getLeftPanel().setCreatePanel(time);
			}
		});
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}


	public Calendar getDate() {
		return date;
	}
}
