package gui.right.schedule;

import gui.interfaces.Colour;
import gui.interfaces.Refresh;
import gui.left.display.EventPanel;
import gui.left.display.MeetingPanel;
import gui.main.ParentFrame;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.CalendarHelper;
import shared.Event;
import shared.Meeting;
import shared.TimeConstants;

/**
 * A JPanel to display events on the calendar.
 * @author Zack McGrath
 */
public class TimePeriodPanel extends JPanel implements Refresh, TimeConstants {
	private static final long serialVersionUID = -7421726999598170520L;
	private static final int MAX_NAME_LEN = 10;
	private Event event;
	
	
	/**
	 * Create the panel.
	 */
	public TimePeriodPanel(Event e) {
		event = e;
		
		//setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Colour.BLACK));
		setBackground(Colour.LIGHT_BLUE);
		if (e.isMeeting()) {
			setBackground(Colour.DARK_BLUE);
		}
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		char[] name = event.getName().toCharArray();
		if (name.length > MAX_NAME_LEN) {
			char[] array = new char[MAX_NAME_LEN];
			
			for (int i = 0; i < MAX_NAME_LEN - 3; i++)
				array[i] = name[i];
			
			for (int i = MAX_NAME_LEN - 3; i < MAX_NAME_LEN; i++)
				array[i] = '.';
			
			name = array; 
		}
		
		JLabel lblEventname = new JLabel(new String(name));
		JLabel lblStartTime = new JLabel(CalendarHelper.getTime(event.getStartTime()));
		JLabel lblEndTime = new JLabel(CalendarHelper.getTime(event.getEndTime()));
		JLabel lblDuration = new JLabel(CalendarHelper.minutesToString(event.getDuration(), TimeFormat.TWENTY_FOUR_HOUR));
		
		lblEventname.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblStartTime.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblEndTime.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblDuration.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		add(lblEventname);
		
		int len = event.getDuration();
		if (len > 2 * MIN_PER_TIME_SLOT) {
			add(lblStartTime);
		}
		if (len > 3 * MIN_PER_TIME_SLOT) {
			add(lblEndTime);
		}
		if (len > 4 * MIN_PER_TIME_SLOT) {
			add(lblDuration);
		}
		
		addListeners();	
	}
	
	
	private void addListeners() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				Event event = ((TimePeriodPanel) mouseEvent.getSource()).getEvent();
				EventPanel panel;
				
				ParentFrame root = getRoot();
				
				try {
					if (event.isMeeting()) {
						panel = new MeetingPanel((Meeting) event);
					} else {
						panel = new EventPanel(event);
					}
					root.getLeftPanel().setEventPanel(panel);
				} catch (NullPointerException e) {
					//????
				}
				
			}
		});
	}
	
	
	private ParentFrame getRoot() {
		return (ParentFrame) SwingUtilities.getWindowAncestor(this);
	}
	
	
	/**
	 * @return length of the time period in minutes.
	 */
	public int getLength() {
		return event.getDuration();
	}
	
	
	/**
	 * @return the event that is contained in this panel.
	 */
	public Event getEvent() {
		return event;
	}
	
	
	@Override
	public void refresh() {
		repaint();
		revalidate();
	}
}
