package gui.right.schedule;

import gui.interfaces.Colour;
import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import shared.CalendarHelper;
import shared.TimeConstants;

/**
 * A week view of a calendar from Sunday to Saturday.
 * @author Zack McGrath
 */
public class CalendarPanel extends JPanel implements Refresh, TimeConstants {
	private static final long serialVersionUID = -7532650391749444009L;
	private ArrayList<DayPanel> dayPanelsList;
	private ArrayList<JPanel> dayNamesList;
	private JPanel daysPanel;
	private JPanel dayNamesPanel;
	private Calendar date;
	
	
	/**
	 * Create the panel.
	 */
	public CalendarPanel(Calendar d) {
		dayNamesPanel = new JPanel();
		daysPanel = new JPanel();
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		dayNamesPanel.setLayout(new GridLayout(0, DAYS_PER_WEEK + 1, 0, 0));
		daysPanel.setLayout(new GridLayout(0, DAYS_PER_WEEK + 1, 0, 0));
		dayNamesPanel.setMaximumSize(new Dimension(SizeConstants.MAX_SIZE, SizeConstants.MIN_HEIGHT));
		
		setWeek(d);
		
		JScrollPane scrollPaneDay = new JScrollPane(dayNamesPanel);
		JScrollPane scrollPaneCal = new JScrollPane(daysPanel);
		
		add(scrollPaneDay);
		add(scrollPaneCal);
	}
	
	
	public void setWeek(Calendar d) {
		date = d;
		daysPanel.removeAll();
		dayNamesPanel.removeAll();
		dayPanelsList = new ArrayList<>();
		dayNamesList = new ArrayList<>();
		
		ArrayList<Calendar> days = new ArrayList<>();
		
		days.add(CalendarHelper.getFirstDayOfWeek(date));
		for (int i = 1; i < DAYS_PER_WEEK; i++) {
			Calendar nextDay = (Calendar) days.get(i-1).clone();
			nextDay.add(Calendar.DATE, 1);
			days.add(nextDay);
		}
		
		TimeOfDayPanel times = new TimeOfDayPanel();
		dayNamesPanel.add(new JPanel());
		daysPanel.add(times);
		
		for (Calendar day: days) {
			createDayNames(day);
			createDayPanels(day);
		}
	}


	private void createDayPanels(Calendar day) {
		DayPanel dayPanel = new DayPanel(day);
		dayPanel.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Colour.BLACK));
		
		daysPanel.add(dayPanel);
		dayPanelsList.add(dayPanel);
	}


	private void createDayNames(Calendar day) {
		JPanel dayNamePanel = new JPanel();
		JLabel dayName = new JLabel(CalendarHelper.getDayOfWeek(day) + " " + day.get(Calendar.DAY_OF_MONTH));
		dayName.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		dayNamePanel.add(dayName);
		dayNamesPanel.add(dayNamePanel);
		dayNamesList.add(dayNamePanel);
	}
	
	
	/**
	 * @return an ArrayList of 7 DayPanels in the Calendar.
	 */
	public ArrayList<TimePeriodPanel> getEventPanels() {
		ArrayList<TimePeriodPanel> eventPanels = new ArrayList<>();
		for (DayPanel panel: dayPanelsList) {
			eventPanels.addAll(panel.getTimePeriodPanels());
		}
		return eventPanels;
	}


	public ArrayList<Integer> getEmptySlots(int day) {
		return dayPanelsList.get(day).getEmptySlots();
	}


	public EmptyPanel[] getEmptyPanels(int day) {
		day = (day % DAYS_PER_WEEK);
		return dayPanelsList.get(day).getEmptyPanels(); 
	}
	
	
	public Calendar getDate() {
		return date;
	}
	
	
	@Override
	public void refresh() {
		for (JPanel p: dayNamesList) {
			p.repaint();
			p.revalidate();
		}
		
		for (DayPanel p: dayPanelsList) {
			p.refresh();
		}
	}
}
