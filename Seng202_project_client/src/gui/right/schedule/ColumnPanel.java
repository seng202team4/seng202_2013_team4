package gui.right.schedule;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import shared.TimeConstants;

/**
 * Shows all the events a user has for one day.
 * @author Zack McGrath
 */
public abstract class ColumnPanel extends JPanel implements Refresh, TimeConstants {
	private static final long serialVersionUID = 7600709745830429644L;
	
	
	/**
	 * Create the panel.
	 */
	public ColumnPanel() {
		int[] rows = new int[TIME_SLOTS_PER_DAY];
		for (int timeSlot = 0; timeSlot < TIME_SLOTS_PER_DAY; timeSlot++) {
			rows[timeSlot] = SizeConstants.COLUMN_HEIGHT;
		}
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {SizeConstants.COLUMN_WIDTH};
		gridBagLayout.rowHeights = rows;
		gridBagLayout.columnWeights = new double[]{Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{Double.MIN_VALUE};
		setLayout(gridBagLayout);
	}
}
