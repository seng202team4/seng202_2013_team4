package gui.main;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;
import gui.left.LeftPanel;
import gui.right.HeaderBarPanel;
import gui.right.schedule.CalendarPanel;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import shared.CalendarHelper;
import shared.TimeConstants;

import com.alee.laf.button.WebButton;

/**
 * The main application panel.
 * Divided into right and left panels.
 * The left panel contains actions.
 * The right panel contains the user's calendar  
 * @author Zack McGrath
 */
public class MainPanel extends JPanel implements Refresh, TimeConstants {
	private static final long serialVersionUID = -8438576029794021570L;
	private HeaderBarPanel headerPanel;
	private LeftPanel leftPanel;
	private CalendarPanel personalCalendarPanel;
	
	
	/**
	 * Create the panel.
	 */
	public MainPanel() {	
		JPanel rightPanel = new JPanel();
		
		leftPanel = new LeftPanel();
		headerPanel = new HeaderBarPanel();
		personalCalendarPanel = new CalendarPanel(Calendar.getInstance());
				
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		
		leftPanel.setPreferredSize(new Dimension(SizeConstants.LEFT_WIDTH, SizeConstants.MAX_SIZE));
		leftPanel.setMaximumSize(new Dimension(SizeConstants.LEFT_WIDTH, SizeConstants.MAX_SIZE));
		rightPanel.setPreferredSize(new Dimension(SizeConstants.RIGHT_WIDTH, SizeConstants.MAX_SIZE));
		headerPanel.setMaximumSize(new Dimension(SizeConstants.MAX_SIZE, SizeConstants.MIN_HEIGHT));
		
		rightPanel.add(headerPanel);
		rightPanel.add(personalCalendarPanel);
		
		add(leftPanel);
		add(rightPanel);
		
		addListeners();
	}


	private void addListeners() {
		headerPanel.getPreviousWeekButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				Calendar prev = personalCalendarPanel.getDate();
				prev.add(Calendar.DATE, -DAYS_PER_WEEK);
				personalCalendarPanel.setWeek(prev);
				headerPanel.getDateChooser().setDate(CalendarHelper.getFirstDayOfWeek(prev).getTime());
				refresh();
			}
		});
		
		headerPanel.getTodayButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				Calendar now = Calendar.getInstance();
				personalCalendarPanel.setWeek(now);
				headerPanel.getDateChooser().setDate(CalendarHelper.getFirstDayOfWeek(now).getTime());
				refresh();
			}
		});
		
		headerPanel.getNextWeekButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				Calendar next = personalCalendarPanel.getDate();
				next.add(Calendar.DATE, DAYS_PER_WEEK);
				personalCalendarPanel.setWeek(next);
				headerPanel.getDateChooser().setDate(CalendarHelper.getFirstDayOfWeek(next).getTime());
				refresh();
			}
		});
		
		headerPanel.getDateChooser().getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
	        @Override
	        public void propertyChange(PropertyChangeEvent e) {
	            if ("date".equals(e.getPropertyName())) {
	            	Calendar time = Calendar.getInstance();
	            	time.setTime((Date) e.getNewValue());
	            	personalCalendarPanel.setWeek(time);
					refresh();
	            }
	        }
	    });
	}
	
	
	/**
	 * @return the logout button.
	 */
	public WebButton getLogoutButton() {
		return headerPanel.getLogoutButton();
	}
	
	
	/**
	 * @return the left panel from inside this panel
	 */
	public LeftPanel getLeftPanel() {
		return leftPanel;
	}
	
	
	@Override
	public void refresh() {
		leftPanel.refresh();
		personalCalendarPanel.refresh();
	}
}
