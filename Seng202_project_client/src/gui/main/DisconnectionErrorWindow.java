package gui.main;

import gui.left.LeftPanel;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import client.ClientManager;

import com.alee.laf.button.WebButton;

/**
 * Window to be shown when the client disconnects from the server.
 * Clicking the button will bring the user back to the connect to a server window.
 * @author Zack McGrath
 */
public class DisconnectionErrorWindow extends ParentFrame {
	private static final long serialVersionUID = 6497007158788179301L;
	private JPanel contentPane;
	private WebButton btnReconnect;

	/**
	 * Create the frame.
	 */
	public DisconnectionErrorWindow() {
		super("Error");
		setBounds(100, 100, 450, 300);
		contentPane = (JPanel) getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JLabel lblError = new JLabel("Error: Dissconected from server.");
		btnReconnect = new WebButton("Reconnect");
	
		lblError.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnReconnect.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		contentPane.add(lblError);
		contentPane.add(btnReconnect);
		
		getRootPane().setDefaultButton(btnReconnect);
		setResizable(false);
		pack();
		
		addListeners();
	}
	
	
	private void addListeners() {
		btnReconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ClientManager.getClientManager().startServerLoginWindow(true);
				kill();
			}
		});
	}
	
	
	@Override
	public LeftPanel getLeftPanel() {
		return null;
	}
}
