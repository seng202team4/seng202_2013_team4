package gui.main;

import gui.left.LeftPanel;

import javax.swing.JFrame;

/**
 * A abstract class that all frames inherit from.
 * @author Zack McGrath
 */
public abstract class ParentFrame extends JFrame {
	private static final long serialVersionUID = 3115619680032363240L;

	/**
	 * Create the frame.
	 * @param title the string to be display on the title bar.
	 */
	public ParentFrame(String title) {
		super(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	/**
	 * To be called if there is a connection problem, ie an IOEception is thrown
	 */
	public void disconnectionError() {
		DisconnectionErrorWindow errorWindow = new DisconnectionErrorWindow();
		errorWindow.setVisible(true);
		kill();
	}
	
	
	/**
	 * Close the frame
	 */
	public void kill() {
		setVisible(false); //you can't see me!
		dispose(); //Destroy the JFrame object
	}
	
	
	public int getMaxCharPerCol() {
		return this.getWidth()/100;
	}
	
	
	/**
	 * @return the left panel contained in the frame, null if frame contains no left panel.
	 */
	public abstract LeftPanel getLeftPanel(); 
}
