package gui.main;

import gui.interfaces.Refresh;
import gui.interfaces.SizeConstants;
import gui.left.LeftPanel;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import com.alee.managers.notification.NotificationManager;

import shared.TimeConstants;

import client.ClientManager;

/**
 * The main application window.
 * @author Zack McGrath
 */
public class MainFrame extends ParentFrame implements Refresh, TimeConstants {
	private static final long serialVersionUID = 2292455670620751734L;
	private JPanel contentPane;
	private MainPanel panel;
	private Timer timer;
	
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		super("Cluster");
		setMinimumSize(new Dimension(SizeConstants.WINDOW_WIDTH, SizeConstants.WINDOW_HEIGHT));
		
		timer = new Timer(MILLSEC_PER_SEC*SEC_PER_MIN, new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				ClientManager.getClientManager().processUpdates();
	      	}
		});
		timer.start();
		
		contentPane = (JPanel) getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		panel = new MainPanel();
		
		contentPane.add(panel);
		
		addListeners();
	}
	
	
	private void addListeners() {
		panel.getLogoutButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					timerStop();
					ClientManager.getClientManager().logout();
					ClientManager.getClientManager().startServerLoginWindow(false);
					kill();
				} catch (IOException ioe) {
					disconnectionError();
				}
			}
		});
		
		addWindowListener(new WindowAdapter() {
			@Override
	        public void windowClosing(WindowEvent e) {
				timerStop();
				e.getWindow().dispose();
		    }
		});
	}
	
	
	public void timerStop() {
		timer.stop();
	}
	
	
	public void displayNotification(String name) {
		NotificationManager.showNotification(name);
	}
	
	
	@Override
	public LeftPanel getLeftPanel() {
		return panel.getLeftPanel();
	}
	
	
	@Override
	public void refresh() {
		panel.refresh();
	}
}
