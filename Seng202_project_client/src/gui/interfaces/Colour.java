package gui.interfaces;

import java.awt.Color;

/**
 * An interface containing all the colours used in the GUI
 * @author Zack McGrath
 */
public interface Colour {
	public static final Color LIGHT_BLUE = new Color(135, 206, 250);
	public static final Color DARK_BLUE = new Color(100, 150, 255);
	public static final Color GRAY_BLUE = new Color(184, 207, 229);
	
	public static final Color WHITE = new Color(254, 254, 255);
	public static final Color LIGHT_GRAY = Color.LIGHT_GRAY;
	public static final Color DARK_GRAY = Color.DARK_GRAY;
	public static final Color BLACK = Color.BLACK;
	
	public static final Color RED_ORANGE = new Color(243, 132, 0);
	public static final Color RED_UNACCEPTABLE = Color.RED;
	public static final Color GREEN_ACCEPTABLE = Color.GREEN;
}
