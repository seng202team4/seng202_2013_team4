package gui.interfaces;

import gui.support.SupportPanel;

import java.util.ArrayList;

/**
 * Panels that implement this interface can have buttons that are not clickable
 * 		when certain fields are empty.
 * @author Zack McGrath
 */
public interface NonEmptyFields {
	/**
	 * @return an ArrayList of SupportPanel that cannot have empty fields
	 */
	public ArrayList<SupportPanel> getNonEmptyPanels();
	
	
	/**
	 * Adds documents listeners to the fields that cannot be empty 
	 */
	public void initializeEmptyListeners();
	
	
	/**
	 * The function to be executed when a change is made to one of the fields
	 * 		that cannot be empty.
	 */
	public void checkEmpty();
}
