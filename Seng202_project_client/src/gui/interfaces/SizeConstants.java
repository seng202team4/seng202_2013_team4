package gui.interfaces;

import shared.TimeConstants;

/**
 * The constants used in the GUI for defining the sizes 
 * @author Zack McGrath
 */
public interface SizeConstants {
	public static final int MAX_SIZE = 4000;
	public static final int MIN_HEIGHT = 40;
	
	public static final int COLUMN_WIDTH = 80;
	public static final int COLUMN_HEIGHT = 15;
	public static final int TEXT_HEIGHT = 25;
	
	public static final int RIGHT_WIDTH = COLUMN_WIDTH * (TimeConstants.DAYS_PER_WEEK + 1);
	public static final int LEFT_WIDTH = 420;
	public static final int LEFT_INTERNAL_WIDTH = LEFT_WIDTH - 80;
	
	public static final int WINDOW_HEIGHT = 625;
	public static final int WINDOW_WIDTH = LEFT_WIDTH + RIGHT_WIDTH + 40;
}
