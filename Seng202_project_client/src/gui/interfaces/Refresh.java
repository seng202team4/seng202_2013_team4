package gui.interfaces;

/**
 * @author Zack McGrath
 */
public interface Refresh {
	/**
	 * Refreshes the component and sup-components.
	 */
	public void refresh();
}
