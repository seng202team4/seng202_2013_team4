package server;

import requests.Request;

/**
 * Used to hold the request object recieved from a service thread. Also hols a String command that
 * defines what kind of request the object is. Also contains a reference back to the source thread.
 */
public class DatabaseRequest {
	public Request object;
	public ServiceThread source;
	
	/**
	 * @param command	The type of reques is being held in object
	 * @param object	The request recieved from the service thread
	 * @param source	A reference back to the source thread
	 */
	public DatabaseRequest(Request request, ServiceThread source) {
		this.object = request;
		this.source = source;
	}
}
