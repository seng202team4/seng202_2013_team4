package server;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;


/**
 *	Listens for incoming connections. Starts a new ServceThread when a new client connects.
 */
public class ConnectionListener implements Runnable {
	private int portNumber;
	private RequestHandler requestHandler;
	
	
	/**
	 * Takes the portnumber new connections will be coming in on.
	 * @param portNumber		
	 * @param requestHandler	The request handler that needs to be passed on to each thread.
	 */
	public ConnectionListener(int portNumber, RequestHandler requestHandler) {
		this.portNumber = portNumber;
		this.requestHandler = requestHandler;
	}
	
	
	/**
	 * Loops until server is terminated or connection failiure. Listens for incoming connections
	 * and starts new ServiceThreads upon new connection.
	 */
	public void run() {
		try {
			@SuppressWarnings("resource")
			ServerSocket server = new ServerSocket(portNumber);
			System.out.println("Waiting for meeting clients to connect...");
	
			//Loop goes on as long as server is running
			while(true) {
				//accept() waits for client connection.
				//When client connects, then server obtains socket to communicate with client.
				Socket socket = server.accept();
				System.out.println("new connection");
				ServiceThread service = new ServiceThread(socket, requestHandler);
	
				//Separate thread is allocated for service for individual clients, i.e., each thread is responsible for one client.
				//Thus, multiple clients can connect to server rather than only one client.
				//service is object of type MSS_Service which implemented the Runnable interface.
				Thread t = new Thread(service);
				t.start();
				//The thread dies when client disconnects and run() in MSS_Service terminates.
			}
		} catch (BindException e) {
			System.err.println("Could not start server on port " + Integer.toString(portNumber) + " as it is already in use");
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}
