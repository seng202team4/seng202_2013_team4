package server;

/**
 * Used when a request processed by the request handler does not need
 * to send any result to the user. It is sent to the ServiceThread to
 * notify it that the last request has been completed and that it does
 * not need to send anything back to the user.
 * @author Danielz99
 */
public class RequestComplete {}