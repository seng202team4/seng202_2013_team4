package server;

import java.util.ArrayList;
import java.util.Calendar;

import shared.CalendarHelper;
import shared.TimeConstants;
import shared.User;

/**
 * SchedulingEngine.java
 * Used to show initiator when attendees cannot attend.
 * Does not need to be initialized as all methods are static.
 * @author Zack McGrath
 */
public class SchedulingEngine implements TimeConstants {
	
	
	/**
	 * Find all the Users who are busy between two times
	 * @param attendees 	an ArrayList of Users.
	 * @param start			a Calendar object, the start of the time period.
	 * @param end			a Calendar object, the end of the time period.
	 * @return an ArrayList of all the Users who are busy between start and end.
	 */
	public static ArrayList<String[]> getUserTriplesBusyBetween(ArrayList<User> attendees,
			Calendar start, Calendar end) {
		
		ArrayList<String[]> busyPeople = new ArrayList<>();
		for (User user: attendees) {
			if (user.getSchedule().hasEventBetween(start, end)) {
				busyPeople.add(new String[]{user.getUsername(),user.getFirstname(),user.getLastName()});
			}
		}
		return busyPeople;
	}
	
	
	/**
	 * Find all the Users who are busy at a particular time slot.
	 * @param attendees 	an ArrayList of Users.
	 * @param start			a Calendar object, the start of the time period.
	 * @return an ArrayList of all the Users who are busy at the time slot starting at start.
	 */
	public static ArrayList<String[]> getUserTriplesBusyAt(ArrayList<User> attendees,
			Calendar start) {
		
		Calendar end = (Calendar) start.clone();
		end.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
		return getUserTriplesBusyBetween(attendees, start, end);
	}
	
	
	/**
	 * Find the number of users who are busy between start and end from a list of users.
	 * @param attendees 	an ArrayList of Users.
	 * @param start			a Calendar object, the start of the time period.
	 * @param end			a Calendar object, the end of the time period.
	 * @return an Integer which is the number of busy users in the specified time period. 
	 */
	public static Integer getNumUserBusyBetween(ArrayList<User> attendees, 
			Calendar start, Calendar end) {
		
		Integer count = 0;
		for (User user: attendees) {
			if (user.getSchedule().hasEventBetween(start, end)) {
				count++;
			}
		}
		return count;
	}
	
	
	/**
	 * Find the number of users who are busy from a time to TIME_SLOT_LENGTH after that time.
	 * @param attendees		an ArrayList of Users.
	 * @param start			a Calendar object, the start of the time period.
	 * @return an Integer which is the number of busy users in the specified time slot.
	 */
	public static Integer getNumUserBusyAt(ArrayList<User> attendees, 
			Calendar start) {
		Calendar end = (Calendar) start.clone();
		end.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
		return getNumUserBusyBetween(attendees, start, end);
	}
	
	
	/**
	 * Find the users who are busy at each time slot between windowStart and windowEnd.
	 * @param attendees		an ArrayList of Users.
	 * @param windowStart	a Calendar object, the start of the time period.	
	 * @param windowEnd		a Calendar object, the end of the time period.
	 * @return an ArrayList of Integers, number of user who are busy at each time slot.
	 */
	public static ArrayList<ArrayList<String[]>> getListUserBusy(ArrayList<User> attendees,
			Calendar windowStart, Calendar windowEnd) {
		
		ArrayList<ArrayList<String[]>> busyList = new ArrayList<>();
		for (Calendar time = (Calendar) windowStart.clone(); time.before(windowEnd);
				time.add(Calendar.MINUTE, MIN_PER_TIME_SLOT)) {
			busyList.add(getUserTriplesBusyAt(attendees, time));
		}
		return busyList;
	}
	
	
	/**
	 * For all time slots between windowStart and windowEnd: finds the users that
	 *  could not attend a meeting if it were to start and a point in time.
	 * @param attendees		an ArrayList of Users.
	 * @param windowStart	a Calendar object, the start of the time period.	
	 * @param windowEnd		a Calendar object, the end of the time period.
	 * @param duration		an int, the length of the meeting in minutes.
	 * @return an ArrayList of Integers, number of user who cannot attend a meeting if the 
	 * meeting were to start at each time slot.
	 */
	public static ArrayList<ArrayList<String[]>> getListUserBusyForDuration(ArrayList<User> attendees,
			Calendar windowStart, Calendar windowEnd, int duration) {
		
		int durationRound = CalendarHelper.roundUpToTimeSlot(duration);
		ArrayList<ArrayList<String[]>> busyList = new ArrayList<>();
		Calendar time = (Calendar) windowStart.clone();
		Calendar timePlusDuration = (Calendar) time.clone();
		timePlusDuration.add(Calendar.MINUTE, durationRound - MIN_PER_TIME_SLOT);
		
		while (timePlusDuration.before(windowEnd)) {
			busyList.add(getUserTriplesBusyBetween(attendees, time, timePlusDuration));
			
			time.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
			timePlusDuration.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
		}
		return busyList;
	}
	
	
	/**
	 * Find the number of users who are busy at each time slot between windowStart and windowEnd.
	 * @param attendees		an ArrayList of Users.
	 * @param windowStart	a Calendar object, the start of the time period.	
	 * @param windowEnd		a Calendar object, the end of the time period.
	 * @return an ArrayList of Integers, number of user who are busy at each time slot.
	 */
	public static ArrayList<Integer> getListNumUserBusy(ArrayList<User> attendees, 
			Calendar windowStart, Calendar windowEnd) {
		
		ArrayList<Integer> busyList = new ArrayList<>();
		for (Calendar time = (Calendar) windowStart.clone(); time.before(windowEnd);
				time.add(Calendar.MINUTE, MIN_PER_TIME_SLOT)) {
			busyList.add(getNumUserBusyAt(attendees, time));
		}
		return busyList;
	}
	
	
	/**
	 * For all time slots between windowStart and windowEnd: finds the number of users that
	 *  could not attend a meeting if it were to start and a point in time.
	 * @param attendees		an ArrayList of Users.
	 * @param windowStart	a Calendar object, the start of the time period.	
	 * @param windowEnd		a Calendar object, the end of the time period.
	 * @param duration		an int, the length of the meeting in minutes.
	 * @return an ArrayList of Integers, number of user who cannot attend a meeting if the 
	 * meeting were to start at each time slot.
	 */
	public static ArrayList<ArrayList<String[]>> getBusyUserTriplesForDuration(ArrayList<User> attendees,
			Calendar windowStart, Calendar windowEnd, int duration) {
		
		int durationRound = CalendarHelper.roundUpToTimeSlot(duration);
		ArrayList<ArrayList<String[]>> busyList = new ArrayList<>();
		Calendar time = (Calendar) windowStart.clone();
		Calendar timePlusDuration = (Calendar) time.clone();
		timePlusDuration.add(Calendar.MINUTE, durationRound);
		windowEnd.add(Calendar.MINUTE,  MIN_PER_TIME_SLOT);
		
		while (timePlusDuration.before(windowEnd)) {
			busyList.add(getUserTriplesBusyBetween(attendees, time, timePlusDuration));
			
			time.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
			timePlusDuration.add(Calendar.MINUTE, MIN_PER_TIME_SLOT);
		}
		return busyList;
	}
	
}
