
package server;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.LinkedBlockingQueue;

import exceptions.EventNotFoundException;
import exceptions.Frisbee;
import exceptions.InvalidTimeslotException;
import exceptions.InvalidUsernameException;
import exceptions.LoginErrorException;
import exceptions.ServerError;
import exceptions.UserNotFoundException;

import requests.*;
import shared.*;
import updates.ChangedMeetingUpdate;
import updates.NewCommentUpdate;
import updates.NewMeetingUpdate;
import updates.RemoveMeetingUpdate;
import updates.Update;

/**
 * Handles all requests from each service thread to the database and the
 * scheduling engine so only one operation can be done at any point in time.
 * This stops the possibility of there being several threads reading and writing
 * to the database at the same time.
 * @author Daniel Hope
 */
public class RequestHandler extends Thread implements Runnable {
	
	// The requestList is accessed by all client threads as they add their own
	// requests. A LinkedBlockingQueue
	// is used to provide a threadsafe collection that can handle reads and
	// writes to this collection safely. It will also block the RequestHandler thread
	// while the queue is empty. 
	private LinkedBlockingQueue<DatabaseRequest> requestList = new LinkedBlockingQueue<DatabaseRequest>();
	private ArrayList<Connection> connectedUsers = new ArrayList<>();
	
	
	/**
	 * Deafault constructor
	 */
	public RequestHandler() {

	}
	
	
	/**
	 * Used by a ServiceThread to add a new request from the clent to the
	 * requestList.
	 * 
	 * @param request
	 *            The request to be added to the queue
	 */
	public void addRequest(DatabaseRequest request) {
		requestList.add(request);
	}
	
	
	/**
	 * Finds out what type of request it is and calls the appropriate function.
	 * @param request
	 */
	public void doNextRequest(DatabaseRequest request) {
		System.out.println(request.source.getConnectedUser() + ": Processing " + request.object.command);
		switch (request.object.command) {
		//case "AddAttendeeReq": doAddAttendeeReq(request); break;
		case "CheckAvailabilityReq": doCheckAvailabliltyReq(request); break;
		case "CreateCommentReq": doCreateComment(request); break;
		case "CreateEventReq": doCreateEventReq(request); break;
		case "CreateMeetingReq": doCreateMeetingReq(request); break;
		case "CreateUserReq": doCreateUserReq(request); break;
		case "DeleteEventReq": doDeleteEventReq(request); break;
		case "EditAcceptanceReq": doEditAcceptanceReq(request); break;
		case "GetAllUsersReq": doGetAllUsersReq(request); break;
		case "GetUserReq": doGetUser(request); break;
		case "LoginReq": doLoginReq(request); break;
		case "LogoutReq": doLogoutReq(request); break;
		case "RemoveAttendeeReq": doRemoveAttendeeReq(request); break;
		case "ScheduleMeetingReq": doScheduleMeetingReq(request); break;
		case "UpdateEventReq": doUpdateEventReq(request); break;
		case "UpdateInitiatorReq": doUpdateInitiatorReq(request); break;
		case "UpdateMeetingReq": doUpdateMeetingReq(request); break; //TODO delete?? really? Don't think so anymore
		case "UpdateUserReq": doUpdateUserReq(request); break;
		default:
			System.err.println(">>> RequestHandler has encoutered an unknown command: " + request.object.command);
		}
	}
	
	
	/**
	 * Sends updated object to all users that are in users and are currently
	 * online
	 * 
	 * @param users
	 * @param update
	 */
	public void sendUpdates(ArrayList<String> users, Update update) {
		int usersLength = users.size();
		int connectedLength = connectedUsers.size();
		for (int i = 0; i < usersLength; i++) {
			for (int j = 0; j < connectedLength; j++) {
				if (users.get(i).equals(connectedUsers.get(j).user)) {
					try {
						connectedUsers.get(j).source.sendUpdate(update);
						System.out.println("update pushed to " + users.get(i));
					} catch (NullPointerException e) {}
				}
			}
		}
	}
	
	
	/**
	 * Checks if all of the users are available in a certain timeslot
	 */
	public void doCheckAvailabliltyReq(DatabaseRequest request) {
		CheckAvailabilityReq requestObject = (CheckAvailabilityReq) request.object;
		ArrayList<String> users = requestObject.users;
		
		//Check if the planned event overlaps with any events
		Event newEvent = new Event(requestObject.meetingID, false, "", "", "", requestObject.duration, requestObject.startTime ,"");
		ArrayList<String> unavailableUsers = findBusyUsers(users, newEvent);
		
		ArrayList<String[]> unavailableUsersTriple = new ArrayList<String[]>();
		User tempUser;
		String[] tempString;
		int length = unavailableUsers.size();
		for (int i = 0; i < length; i++) {
			try {
				tempUser = Database.getUser(unavailableUsers.get(i));
				tempString = new String[] {tempUser.getUsername(), tempUser.getFirstname(), tempUser.getLastName()};
				unavailableUsersTriple.add(tempString);
			} catch (UserNotFoundException e) {}
		}
		request.source.input.add(unavailableUsersTriple);
	}
	
	
	/**
	 * Takes a list of users and a temporary event that only needs the duration and start time of
	 * the planned timeslot for a new event.
	 * @param users		An ArrayList of user classes that want to attend the meeting
	 * @param newEvent	An event that contains the start time and duration of the planned event.
	 * @return An arrayList of strings of the usernames of the users who have an event clashing.	
	 */
	public ArrayList<String> findBusyUsers(ArrayList<String> attendees, Event newEvent) {
		ArrayList<User> users = new ArrayList<>();
		//Fetch each users schedule
		for (String attendee: attendees) {
			try {
				users.add(Database.getUser(attendee));
			} catch (UserNotFoundException e) {
				System.err.println("Could not find user " + attendee + " @CheckAvailabilityReq");
			}
		}
		
		ArrayList<String> unavailableUsers = new ArrayList<String>();
		int length = users.size();
		for (int i = 0; i < length; i++) {
			try {
				users.get(i).getSchedule().checkEventForClash(newEvent.getID(), newEvent.getStartTime(), newEvent.getDuration());
			} catch (InvalidTimeslotException e) {
				unavailableUsers.add(users.get(i).getUsername());
			}
		}
		return unavailableUsers;
	}
	
	
	/**
	 * Takes a DatabaseRequest and returns the user if the username and password
	 * matches otherwise returns null
	 * 
	 * @param request
	 */
	public void doLoginReq(DatabaseRequest request) {
		LoginReq requestObject = (LoginReq) request.object;
		User requestedUser = null;
		try {
			requestedUser = Database.login(requestObject);
			// if the user logged on we add them to the
			// logged on list
			connectedUsers.add(new Connection(requestedUser.getUsername(),request.source));
			request.source.input.add(requestedUser);
		
		} catch (UserNotFoundException e) {
			request.source.input.add(new ServerError(new LoginErrorException()));
		}
	}
	
	
	/**
	 * Removes a user from the currently online list.
	 * 
	 * @param username
	 */
	public void doLogoutReq(DatabaseRequest request) {
		LogoutReq requestObject = (LogoutReq) request.object;
		int length = connectedUsers.size();
		for (int i = 0; i < length; i++) {
			if (connectedUsers.get(i).user.equals(requestObject.username)) {
				connectedUsers.remove(i);
				return;
			}
		}
	}
	
	
	/**
	 * Takes a getUser request an returns the corresponding user
	 * 
	 * @param request
	 */
	public void doGetUser(DatabaseRequest request) {
		GetUserReq requestObject = (GetUserReq) request.object;
		User requestedUser = null;
		try {
			requestedUser = Database.getUser(requestObject.username);
		} catch (UserNotFoundException e) {
			request.source.input.add(new ServerError(new UserNotFoundException()));
		}
		if (requestedUser != null) {
			request.source.input.add(requestedUser);
		} else {
			request.source.input.add(new ServerError(new UserNotFoundException()));
		}
	}
	
	
	/**
	 * Takes a new user class to be added to the database.
	 * 
	 * @param request
	 */
	public void doCreateUserReq(DatabaseRequest request) {
		CreateUserReq requestObject = (CreateUserReq) request.object;
		User createdUser;
		try {
			createdUser = Database.addUser(requestObject);
			request.source.input.add(createdUser);
		} catch (InvalidUsernameException e) {
			request.source.input.add(new ServerError(new InvalidUsernameException()));
		}
	}
	
	
	/**
	 * Takes a meeting creation request and creates the meeting in the database
	 * 
	 * @param request
	 */
	public void doCreateMeetingReq(DatabaseRequest request) {
		CreateMeetingReq requestObject = (CreateMeetingReq) request.object;
		Meeting newMeeting = requestObject.meeting;
		//Check if the new Meeting will cause a clash with the users schedule
		ArrayList<String> users = newMeeting.getAttendees();
		users.add(newMeeting.getInitiator());
		ArrayList<String> unavailableUsers = findBusyUsers(users, newMeeting);
		
		if (unavailableUsers.size() == 0) {//There are no clashes
			users.remove(newMeeting.getInitiator());
			Meeting createdMeeting = null;
			createdMeeting = Database.createNewMeeting(requestObject);
			request.source.input.add(createdMeeting);
			//Send updates to users
			sendUpdates(users, new NewMeetingUpdate(createdMeeting));
		} else {
			request.source.input.add(new ServerError(new InvalidTimeslotException()));
		}
	}
	
	
	/**
	 * Takes an update meeting request and updates the corresponding meeting in
	 * the database
	 * 
	 * @param request
	 */
	public void doUpdateMeetingReq(DatabaseRequest request) {
		UpdateMeetingReq requestObject = (UpdateMeetingReq) request.object;
		Meeting meeting = requestObject.updatedMeeting;
		Meeting oldMeeting;
		try {
			oldMeeting = (Meeting) Database.getEvent(meeting.getID());
		} catch (EventNotFoundException e1) {
			System.err.println("EVENT " + meeting.getID() + " WAS NOT FOUND @UPDATEMEETINGREQ");
			request.source.input.add(new ServerError(new Frisbee()));
			return;
		}

		//Find which users are new and which were already there
		ArrayList<String> attendees = meeting.getAttendees();
		ArrayList<String> newAttendees = new ArrayList<String>();
		ArrayList<String> repeatAttendees = new ArrayList<String>();
		for (String attendee: attendees) {
			if (!oldMeeting.getAttendees().contains(attendee)) { //attendee is new
				newAttendees.add(attendee);	
			} else { //attendee is not new
				repeatAttendees.add(attendee);
			}
		}
		
		//Find which users have been removed
		ArrayList<String> removedAttendees = new ArrayList<String>();
		attendees = oldMeeting.getAttendees();
		for (String attendee: attendees) {
			if (!meeting.getAttendees().contains(attendee)) {
				removedAttendees.add(attendee);
			}
		}
		
		
		//Check if any clashes will occur
		attendees = meeting.getAttendees();
		attendees.add(meeting.getInitiator());
		ArrayList<String> busyUsers = findBusyUsers(attendees, meeting);
		
		if (busyUsers.size() == 0) { //All attendees have no clashes
			attendees.remove(meeting.getInitiator());
			Meeting updatedMeeting = null;
			try {
				updatedMeeting = Database.modifyMeeting(meeting);
			} catch (EventNotFoundException e) {
				updatedMeeting = null;
			}
			request.source.input.add(updatedMeeting);
			sendUpdates(newAttendees, new NewMeetingUpdate(updatedMeeting));
			sendUpdates(repeatAttendees, new ChangedMeetingUpdate(updatedMeeting));
			sendUpdates(removedAttendees, new RemoveMeetingUpdate(updatedMeeting.getID()));
		} else { //there is a clash
			request.source.input.add(new ServerError(new InvalidTimeslotException()));
		}
	}
	
	
	/**
	 * Takes a meeting request and runs it through the scheduling engine,
	 * returning an arraylist of integers of how may users are busy at each time
	 * interval.
	 * 
	 * @param request
	 */
	public void doScheduleMeetingReq(DatabaseRequest request) {
		ScheduleMeetingReq requestObject = (ScheduleMeetingReq) request.object;
		ArrayList<User> users = new ArrayList<>();
		int numAttendees = requestObject.attendees.size();
		for (int i = 0; i < numAttendees; i++) {
			try {
				users.add(Database.getUser(requestObject.attendees.get(i)));
				System.out.println("Added user: " + users.get(i).getUsername());
			} catch (UserNotFoundException e) {
				System.err.println(">>> RequestHandler: User not found "
						+ requestObject.attendees.get(i));
			}
		}
		
		Calendar windowStart = requestObject.windowStart;
		Calendar windowEnd = requestObject.windowEnd;
		int duration = requestObject.duration;
		ArrayList<ArrayList<String[]>> result = SchedulingEngine.getBusyUserTriplesForDuration(users, windowStart, windowEnd, duration);
		request.source.input.add(result);
	}
	
	
	/**
	 * Takes an event creation request and creates the event in the database
	 * 
	 * @param request
	 */
	public void doCreateEventReq(DatabaseRequest request) {
		CreateEventReq requestObject = (CreateEventReq) request.object;
		
		Event createdEvent = null;
		try {
			Schedule userSchedule = Database.buildSchedule(requestObject.event.getInitiator());
			if (!userSchedule.isEventClashing(requestObject.event)) {
				createdEvent = Database.addEvent(requestObject);
				request.source.input.add(createdEvent);
			} else {
				request.source.input.add(new ServerError(new InvalidTimeslotException()));
			}
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @addEvent");
			request.source.input.add(new ServerError(e));
		}
	}
	
	
	/**
	 * Takes an event update request and updates the corresponding event in the
	 * database
	 * 
	 * @param request
	 */
	public void doUpdateEventReq(DatabaseRequest request) {
		UpdateEventReq requestObject = (UpdateEventReq) request.object;
		Event updatedEvent = null;
		try {
			updatedEvent = Database.modifyEvent(requestObject);
		} catch (EventNotFoundException e) {
			System.err.println("Could not find event ID: " + Integer.toString(requestObject.updatedEvent.getID()));
		}

		request.source.input.add(updatedEvent);

	}
	
	
	/**
	 * Removes an event or meeting from the database. Does not return anything to the 
	 * client that sent the request.
	 * 
	 * @param request
	 */
	public void doDeleteEventReq(DatabaseRequest request) {
		DeleteEventReq requestObject = (DeleteEventReq) request.object;
		try {
			if (requestObject.isMeeting) {
				ArrayList<String> attendees = Database.getAttendees(requestObject.eventID);
				Database.deleteMeeting(requestObject.eventID);
				attendees.remove(request.source.getConnectedUser());
				sendUpdates(attendees, new RemoveMeetingUpdate(requestObject.eventID));
			} else {
				Database.deleteEvent(requestObject.eventID);
			}
			request.source.input.add(new RequestComplete());
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
	}
	
	
	/**
	 * Takes a create comment request and add it to the database.
	 * 
	 * @param request
	 */
	public void doCreateComment(DatabaseRequest request) {
		CreateCommentReq requestObject = (CreateCommentReq) request.object;
		Comment createdComment;
		createdComment = Database.addComment(requestObject);
		
		if (createdComment != null) {
			request.source.input.add(createdComment);
			try {
				ArrayList<String> users = Database.getAttendees(createdComment.getMeetingID());
				users.remove(requestObject.comment.getOwner());
				sendUpdates(users, new NewCommentUpdate(createdComment));
				//request.source.input.add(new RequestComplete());
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @getAttendees");
				System.err.println("Could not push new comment update to users");
			}
		} else {
			request.source.input.add(new ServerError(new Frisbee()));
		}
	}
	
	
	/**
	 * Gets an arrayList of String[] that conatians a users username, firstname and lastname
	 * of all users in the database.
	 * @param request
	 */
	public void doGetAllUsersReq(DatabaseRequest request) {
		ArrayList<String[]> allUsers = null;
		try {
			allUsers = Database.getUserList();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @getAllUsers");
		}

		request.source.input.add(allUsers);
	}
	
	
	/**
	 * Removes an attendee from a meeting
	 * @param request
	 */
	public void doRemoveAttendeeReq(DatabaseRequest request) {
		RemoveAttendeeReq requestObject = (RemoveAttendeeReq) request.object;
		try {
			Database.rejectMeeting(requestObject.meetingID, requestObject.username);
			Meeting updatedMeeting = (Meeting) Database.getEvent(requestObject.meetingID);
			request.source.input.add(updatedMeeting);
			//Send updates to online users
			ChangedMeetingUpdate newUpdate = new ChangedMeetingUpdate(updatedMeeting);
			sendUpdates(Database.getAttendees(requestObject.meetingID), newUpdate);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			request.source.input.add(new ServerError(new Frisbee()));
		} catch (EventNotFoundException e) {
			request.source.input.add(new ServerError(new Frisbee()));
		}
	}
	
	
	/**
	 * Used when a user request to accept a meeting, reject a meeting or leave a meeting
	 * @param request
	 */
	public void doEditAcceptanceReq(DatabaseRequest request) {
		EditAcceptanceReq requestObject = (EditAcceptanceReq) request.object;
		int meetingID = requestObject.meetingID;
		String username = requestObject.username;
		Boolean result = true;
		try {
			if (requestObject.isAccept) {
				Database.acceptMeeting(meetingID, username);
			} else {
				Database.rejectMeeting(meetingID, username);
			}
			Meeting updatedMeeting = (Meeting) Database.getEvent(meetingID);
			ArrayList<String> attendees = Database.getAttendees(meetingID);
			attendees.remove(username);
			sendUpdates(attendees, new ChangedMeetingUpdate(updatedMeeting));
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @accept/reject Meeting");
			result = false;
		} catch (EventNotFoundException e) {
			e.printStackTrace();
			result = false;
		}
		
		request.source.input.add(result);
	}
	
	
	/**
	 * Changes the initiator on a meeting to a user that is in the attendees in a meeting
	 * @param request
	 */
	public void doUpdateInitiatorReq(DatabaseRequest request) {
		UpdateInitiatorReq requestObject = (UpdateInitiatorReq) request.object;
		try {
			Database.updateInitiator(requestObject);
			Meeting updatedMeeting = (Meeting) Database.getEvent(requestObject.meetingID);
			request.source.input.add(updatedMeeting);
			sendUpdates(updatedMeeting.getAttendees(), new ChangedMeetingUpdate(updatedMeeting));
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			request.source.input.add(new ServerError(new IOException()));
		} catch (EventNotFoundException e) {
			System.err.println("Could not find garlic naan");
			request.source.input.add(new ServerError(new EventNotFoundException()));
		}
	}
	
	
	/**
	 * Updates the users details on the database
	 * @param request
	 */
	public void doUpdateUserReq(DatabaseRequest request) {
		UpdateUserReq requestObject = (UpdateUserReq) request.object;
		User updatedUser;
		try {
			updatedUser = Database.updateUser(requestObject);
			request.source.input.add(updatedUser);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + "@updateUser");
			request.source.input.add(new ServerError(new Frisbee()));
		} catch (UserNotFoundException e) {
			System.err.println("User " + requestObject.user.getUsername() + " not found @UpdateUser");
			request.source.input.add(new ServerError(new UserNotFoundException()));
		}
	}
	
	
	/**
	 * Only used during testing to get the users that are currently online.
	 * @return		An arraylist of the users currently connected to the server.
	 */
	public ArrayList<String> getConnectedUsers() {
		ArrayList<String> users = new ArrayList<>();
		
		int length = connectedUsers.size();
		for (int i = 0 ; i < length; i++) {
			users.add(connectedUsers.get(i).user);
		}
		
		return users;
	}
	
	
	/**
	 * The main loop for the request handler, checks for a new request once a
	 * second.
	 */
	public void run() {
		DatabaseRequest next;
		while (true) {
			try {
				next = requestList.take();
				doNextRequest(next);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
