package server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

/**
 * Contains the main method for the server. Starts connection listener and
 * request handler.
 * 
 * @author Daniel Hope
 */
public class ServerMain {
	ConnectionListener listener;
	RequestHandler requestHandler;
	String propertiesPath = "serverSettings/settings.properties";
	int portNumber;
	String databasePath;
	Connection databaseConnection = null;
	Scanner terminalScanner = new Scanner(System.in);
	String defaultPropertiesFile = "Port=12345\r\nDatabase=databaseObjects/test.db";
	
	
	public static void main(String[] args) {
		ServerMain server = new ServerMain();
		server.start();
	}
	
	
	/**
	 * Creates a new server.properties file serverSettings folder and 
	 * writes the default settings into it.
	 * @throws IOException
	 */
	public void createDefaultSettings() throws IOException {
		System.out.println("Creating new properties file in serverSettings folder");
		File newDir = new File("serverSettings");
		newDir.mkdirs(); 
		File newFile = new File(propertiesPath);
		newFile.createNewFile();
		BufferedWriter writer = new BufferedWriter(new FileWriter(propertiesPath));
		writer.write(defaultPropertiesFile);
		writer.close();
		System.out.println("Please edit the properties file in serverSettings folder and restart the application");
		System.out.println("Press any key to quit");
		System.in.read();
		System.exit(0);
	}
	
	
	/**
	 * Loads the settings from the server.properties file
	 * @throws IOException
	 */
	public void loadSettings() throws IOException {
		try {
			FileInputStream file = new FileInputStream(propertiesPath);
			Properties prop = new Properties();
			prop.load(file);
			file.close();
			portNumber = Integer.parseInt(prop.getProperty("Port"));
			databasePath = prop.getProperty("Database");
			System.out.println("Starting server on port " + Integer.toString(portNumber));
		} catch (IOException ioe) {
			System.err.println("Could not find settings file");
			createDefaultSettings();
		} catch (NumberFormatException nfe) {
			//TODO handle NumberFormatException
			nfe.printStackTrace();
		}
	}
	
	
	/**
	 * Used to quit the server when an error has occurred. Stops the terminal from just 
	 * quitting as it requires the user to press enter before the program will quit.
	 * @throws  
	 */
	private void errorExit() {
		try {
			System.err.println("Press Enter to quit");
			System.in.read();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Starts request handler and connection listener
	 */
	public void start() {
		try {
			loadSettings();
			Database.initiateDatabase(databasePath);
		} catch (IOException e) {
			System.err.println("Error creating default settings file in directory serverSettings/settings.properties");
			System.err.println(e.getClass().getName() + ": " + e.getMessage() +  " @servermain.start");
			errorExit();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() +  " @servermain.start");
			System.err.println("Is server.properties configured correctly?");
			errorExit();
		} catch (ClassNotFoundException e) {
			System.err.println(">>> Database failed to initiate, ClassNotFoundException");
			errorExit();
		}
		
		requestHandler = new RequestHandler();
		Thread requestHandlerThread = new Thread(requestHandler);
		requestHandlerThread.start();
		listener = new ConnectionListener(portNumber, requestHandler);
		Thread listenerThread = new Thread(listener);
		listenerThread.start();
		terminal();
	}
	
	
	/**
	 * loop that waits for terminal commands.
	 */
	private void terminal() {
		String input = null;
		input = terminalScanner.nextLine();
		while (!input.equals("shutdown")) {
			input = terminalScanner.nextLine();
		}
		System.out.println("Shutting down");
		System.exit(0);
	}	
}


/*

      _-_-/  ,                   ,,
     (_/     ||         '         ||    _      _
    (_ --_  =||=  _-_  \\  \\/\\  ||   < \,   / \\   _-_  ,._-_,
	  --_ )  ||  || \\ ||  || ||  ||   /-||  || ||  || \\  ||
	_,   ))  ||  ||/   ||  || ||  ||  (( ||  || ||  ||/    ||
   (-_-_-``   \\, \\,/  \\  \\ \\  \\  \/\\  \\_||  \\,/   \\,
                                           /\\  \\
                                          (  \\_//
        __        ___  __                     __  /  __
  |\ | |_  |  |    _/ |_   /\  |    /\  |\ | |  \   (_
  | \| |__ |/\|   /__ |__ /--\ |__ /--\ | \| |__/   __)

                     .sssssssss.
               .sssssssssssssssssss
             sssssssssssssssssssssssss
            ssssssssssssssssssssssssssss
             @@sssssssssssssssssssssss@ss
             |s@@@@sssssssssssssss@@@@s|s
      _______|sssss@@@@@sssss@@@@@sssss|s
    /         sssssssss@sssss@sssssssss|s
   /  .------+.ssssssss@sssss@ssssssss.|
  /  /       |...sssssss@sss@sssssss...|
 |  |        |.......sss@sss@ssss......|
 |  |        |..........s@ss@sss.......|
 |  |        |...........@ss@..........|
  \  \       |............ss@..........|
   \  '------+...........ss@...........|
    \________ .........................|
             |.........................|
            /...........................\
           |.............................|
              |.......................|
                  |...............|

      __         __  __ ___      __   __  __  __
     |_  | |\ | |_  (_   |      |__) |_  |_  |__)
     |   | | \| |__ __)  |      |__) |__ |__ | \
     
     
     */
