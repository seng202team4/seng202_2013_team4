package server;
//System.err.println(e.getClass().getName() + ": " + e.getMessage());
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import exceptions.CommentNotFoundException;
import exceptions.EventNotFoundException;
import exceptions.InvalidUsernameException;
import exceptions.UserNotFoundException;

import shared.Comment;
import shared.Event;
import shared.Meeting;
import shared.Schedule;
import shared.User;
import requests.CreateMeetingReq;
import requests.CreateEventReq;
import requests.LoginReq;
import requests.CreateCommentReq;
import requests.CreateUserReq;
import requests.UpdateEventReq;
import requests.UpdateInitiatorReq;
import requests.UpdateUserReq;


/**
 * Controls reads and writes to the database files. Can retrive and build Meeting, User, and Comment classes
 * from a string. 
 * @author Daniel Hope
 */
public class Database {
	private static Connection sqlDatabase;
	//databse status
	private static int nextEventID;
	private static int nextCommentID;
	
	
/*	
     _________
    | _______ |
   / \         \
  /___\_________\
  |   | \  O    |
  |   |  \    O |
  |   |   \     |
  |   | M  \ O  |
  |   |     \   |
  | O |\  I  \  |
  |   | \     \ |
  |   |  \  L  \|
  |   |  O\     |
  |   |    \  K |
  |   |     \   |
  |   |    O \  |
  |___|_______\_|
	
	*/
	
	
	/**
	 * Accessor for nextEventID
	 * @return	The ID of the next Event that will be added to the database.
	 */
	public static int getNextEventID() {
		return nextEventID;
	}
	
	
	/**
	 * Accessor for nextCommentID
	 * @return	The ID of the next comment that will be added to the database.
	 */
	public static int getNextCommentID() {
		return nextCommentID;
	}
	
	
	/**
	 * Reads The status file so the database can regain the same state it was in when last shut down.
	 * @throws ClassNotFoundException 
	 * @throws IOException
	 */
	public static void initiateDatabase(String path) throws SQLException, ClassNotFoundException {
		System.out.println("Loading database at " + path);
		Class.forName("org.sqlite.JDBC");
		sqlDatabase = DriverManager.getConnection("jdbc:sqlite:" + path);
		Statement stmt = sqlDatabase.createStatement();
		ResultSet result = stmt.executeQuery("select * from ID_table");
		nextEventID = result.getInt("event");
		nextCommentID = result.getInt("comment");
		System.out.println("Opened database successfully");
	}

	
	/**
	 * Ends the connection to the SQLite database
	 */
	public static void closeDatabase() {
		try {
			sqlDatabase.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @closeDatabase");
		}
	}
	
	
	/**
	 * Gets the next unique ID number for a new event or comment
	 * @param IDType
	 * @return
	 * @throws SQLException 
	 * @throws IOException
	 */
	private static int getNextID(String IDType) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		Integer num = -1;
		switch (IDType) {
			case "event": num = new Integer(nextEventID); nextEventID += 1; break;
			case "comment": num = new Integer(nextCommentID); nextCommentID += 1; break;
		}
		stmt.executeUpdate("update ID_table set " + IDType + " = " + Integer.toString(num + 1) + ";");
		
		return num;
	}
	
	
	/**
	 * Finds all apostrophies and replaces them so they can be stored in the database
	 * @param text
	 * @return
	 */
	private static String fixText(String text) {
		return text.replaceAll("'", "''");
	}
	
	//-----------------------------------------------------------------------------------------------------
	
	// EVENT TABLE
	//	ID | isMeeting | Title | location | description | duration | start_time | initiator 
	//-----------------------------------------------------------------------------------------------------
	
	/**
	 * Adds attending users to the accepted_table ind the database when 
	 * a new meeting is added.
	 * @param users
	 * @throws SQLException 
	 */
	public static void addUsersToEvents(int meetingID, ArrayList<String> users) throws SQLException {
		Statement stmt;
		StringBuilder update;
		for(String user : users) {
			update = new StringBuilder();
			update.append("INSERT INTO accepted_table VALUES (");
			update.append(Integer.toString(meetingID) +",'" + fixText(user) + "',0);");
			stmt = sqlDatabase.createStatement();
			stmt.executeUpdate(update.toString());
		}
	}
	
	
	/**
	 * Creates a new meeting in the database.
	 * @param requestedMeeting
	 * @return
	 * @throws IOException
	 */
	public static Meeting createNewMeeting(CreateMeetingReq requestedMeeting)  {
		Meeting newMeeting = requestedMeeting.meeting;
		int ID;
		try {
			ID = getNextID("event");
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @createNewMeeting");
			return null;
		}
		StringBuilder sqlCommand = new StringBuilder();
		sqlCommand.append("INSERT INTO event VALUES (");
		sqlCommand.append(Integer.toString(ID) + ",1,'" + fixText(newMeeting.getName()) + "','");
		sqlCommand.append(fixText(newMeeting.getLocation()) + "','" + fixText(newMeeting.getDescription()) + "',");
		sqlCommand.append(Integer.toString(newMeeting.getDuration()) + "," + Long.toString(newMeeting.getStartTime().getTimeInMillis()));
		sqlCommand.append(",'" + fixText(newMeeting.getInitiator()) + "');");
		
		try { 
			//add initiator
			Statement stmt = sqlDatabase.createStatement();
			stmt.executeUpdate("INSERT INTO accepted_table VALUES (" + 
			Integer.toString(ID) + ",'" + fixText(newMeeting.getInitiator()) + "',1);");
			
					
			//add other users
			stmt = sqlDatabase.createStatement();
			stmt.executeUpdate(sqlCommand.toString());
			addUsersToEvents(ID, newMeeting.getAttendees());
			return (Meeting) getEvent(ID);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @createNewMeeting");
		} catch (EventNotFoundException e) {
			System.err.println("Could not find meeting after creating new meeting");
		}
		return null;
		
	}
	
	
	/**
	 * Updates an existing meeting in the database
	 * @param updatedMeeting
	 * @return
	 * @throws EventNotFoundException 
	 * @throws IOException
	 */
	public static Meeting modifyMeeting(Meeting updatedMeeting) throws EventNotFoundException {
		StringBuilder update = new StringBuilder();
		update.append("UPDATE event SET title = '" + fixText(updatedMeeting.getName()) + "',");
		update.append("location = '" + fixText(updatedMeeting.getLocation()) + "',");
		update.append("description = '" + fixText(updatedMeeting.getDescription()) + "',");
		update.append("duration = " + updatedMeeting.getDuration() + ",");
		update.append("start_time = " + Long.toString(updatedMeeting.getStartTime().getTimeInMillis()));
		update.append(",initiator = '" + fixText(updatedMeeting.getInitiator()) + "' ");
		update.append("WHERE ID = " + updatedMeeting.getID() + ";");

		Meeting upMeeting = null;
		ArrayList<Boolean> updatedAccepted = new ArrayList<Boolean>();
		int numAttendees = updatedMeeting.getAttendees().size();
		for (int i = 0; i < numAttendees; i++) {
			updatedAccepted.add(false);
		}
		try {
			ArrayList<String> updatedAttendees = updatedMeeting.getAttendees();
			updatedAttendees.add(updatedMeeting.getInitiator());
			updatedAccepted.add(true);
			updateAttendees(updatedMeeting.getID(), updatedAttendees, updatedAccepted);
			Statement stmt = sqlDatabase.createStatement();
			stmt.executeUpdate(update.toString());
			upMeeting = (Meeting) getEvent(updatedMeeting.getID());
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @modifyEvent");
		}
		return upMeeting;
		
	}
	
	
	/**
	 * updates the attendees_table to match the updated meeting
	 * @throws SQLException 
	 */
	public static void updateAttendees(int meetingID, ArrayList<String> users, ArrayList<Boolean> acceptance) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE FROM accepted_table WHERE eventID = " + Integer.toString(meetingID) + ";");
		int length = users.size();
		StringBuilder update;
		for (int i = 0; i < length; i++) {
			update = new StringBuilder();
			update.append("INSERT INTO accepted_table VALUES (" + Integer.toString(meetingID) + ",'");
			update.append(fixText(users.get(i)) + "',");
			if (acceptance.get(i)) {
				update.append("1);");
			} else {
				update.append("0);");
			}
			stmt = sqlDatabase.createStatement();
			
			stmt.executeUpdate(update.toString());
		}
	}
	
	
	/**
	 * Deletes a meeting from the database along with all comments associated with it.
	 * @param meetingID		Int ID of the meeting to be deleted
	 * @throws SQLException
	 */
	public static void deleteMeeting(int meetingID) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from event where ID = " + Integer.toString(meetingID) + ";");
		stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from accepted_table where eventID = " + Integer.toString(meetingID) + ";");
		stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from comment where eventID = " + Integer.toString(meetingID) + ";");
	}
	
	
	/**
	 * Sets a user to have accepted an invite to a meeting in a database.
	 * @param meetingID		The ID of the meeting the user was invited to.
	 * @param username		The username of the user who accepted the meeting.
	 * @throws SQLException 
	 */
	public static void acceptMeeting(int meetingID, String username) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("UPDATE accepted_table SET has_accepted = 1 where eventID = " + 
				Integer.toString(meetingID) + " and username = '" + fixText(username) + "';");
	}
	
	
	/**
	 * Deletes the user from a meeting in the database.
	 * @param meetingID		The ID of the meeting.
	 * @param username		THe username of the user.
	 * @throws SQLException
	 */
	public static void rejectMeeting(int meetingID, String username) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from accepted_table where eventID = " + 
				Integer.toString(meetingID) + " and username = '" + fixText(username) + "';");
	}
	
	
	/**
	 * Gets all users that are attending a meeting.
	 * @param ID	This ID of the meeting.
	 * @return
	 * @throws SQLException 
	 */
	public static ArrayList<String> getAttendees(int ID) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		ResultSet result = stmt.executeQuery("SELECT username from accepted_table where eventID = " + Integer.toString(ID));
		ArrayList<String> users = new ArrayList<>();
		while (result.next()) {
			users.add(result.getString("username"));
		}
		return users;
	}
	
	
	/**
	 * Sorts the comments by their timestamp by most recent first.
	 * @param comments
	 * @return
	 */
	public static ArrayList<Comment> sortComments(ArrayList<Comment> comments) {
		ArrayList<Comment> sorted = new ArrayList<>();
		int length = comments.size();
		if (length == 0) { //checks for zero comments case
			return sorted;
		}
		sorted.add(comments.get(0));
		int slength = 1;
		Boolean added;
		Comment toBeAdded;
		for (int i = 1; i < length; i++) {
			int j = 0;
			toBeAdded = comments.get(i);
			added = false;
			while (j < slength && !added) {
				if (toBeAdded.getTimeStamp().after(sorted.get(j).getTimeStamp())) {
					sorted.add(j, toBeAdded);
					added = true;
					slength += 1;
				}
				j += 1;
				
			}
			if (!added) {
				sorted.add(toBeAdded);
				slength += 1;
			}
		}
		
		return sorted;
	}
	
	
	/**
	 * Gets an event from the database.
	 * 
	 * @param eventID	The unique event ID.
	 * @return 			An Event class of the event.
	 */
	public static Event getEvent(int eventID) throws EventNotFoundException {
		try {
			Statement stmt = sqlDatabase.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM event WHERE id = " + Integer.toString(eventID) + ";");
			int ID = result.getInt("ID");
			Boolean isMeeting = result.getBoolean("isMeeting");
			String title = result.getString("title");
			String location = result.getString("location");
			String description = result.getString("description");
			int duration = result.getInt("duration");
			Calendar startTime = Calendar.getInstance();
			startTime.setTimeInMillis(result.getLong("start_time"));
			String initiator = result.getString("initiator");
			
			if (isMeeting) { //If the event with eventID is a meeting
				ArrayList<String> attendees = new ArrayList<>();
				ArrayList<Boolean> accepted = new ArrayList<>();
				Statement stmt2 = sqlDatabase.createStatement();
				ResultSet result2 = stmt2.executeQuery("SELECT * FROM accepted_table WHERE eventID = " + Integer.toString(eventID));
				while (result2.next()) { //create attendees and accepted tables.
					if (!result2.getString("username").equals(initiator)) {
						attendees.add(result2.getString("username"));
						accepted.add(result2.getBoolean("has_accepted"));
					}
				}
				
			    ArrayList<Comment> comments = new ArrayList<>();
			    Statement stmt3 = sqlDatabase.createStatement();
			    ResultSet result3 = stmt3.executeQuery("SELECT * FROM comment WHERE eventID = " + Integer.toString(eventID));
			    while (result3.next()) {
			    	try {
						comments.add(getComment(result3.getInt("ID")));
					} catch (CommentNotFoundException e) {
						System.err.println("Could not find comment id = " + result3.getInt("commentID"));
					}
			    }
			    comments = sortComments(comments);
			    return new Meeting(ID, isMeeting, title, location, description, duration, startTime, initiator, attendees, accepted, comments);
			} else {
				return new Event(ID, isMeeting, title, location, description, duration, startTime, initiator);
			}
			
		} catch (SQLException e) {
			if (! e.getMessage().equals("ResultSet closed")) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @getEvent");
			}
			throw new EventNotFoundException();
		}
	}
	
	
	/**
	 * Updates and existing Event in the database
	 * @param updatedEvent
	 * @return
	 * @throws IOException
	 */
	public static Event modifyEvent(UpdateEventReq updatedEvent) throws EventNotFoundException {
		Event updated = updatedEvent.updatedEvent;
		StringBuilder update = new StringBuilder();
		update.append("UPDATE event SET title = '" + fixText(updated.getName()) + "',");
		update.append("location = '" + fixText(updated.getLocation()) + "',");
		update.append("description = '" + fixText(updated.getDescription()) + "',");
		update.append("duration = " + updated.getDuration() + ",");
		update.append("start_time = " + Long.toString(updated.getStartTime().getTimeInMillis()));
		update.append(",initiator = '" + fixText(updated.getInitiator()) + "' ");
		update.append("WHERE ID = " + updated.getID() + ";");
		Event newEvent = null;
		try {
			Statement stmt = sqlDatabase.createStatement();
			stmt.executeUpdate(update.toString());
			newEvent = getEvent(updated.getID());
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @modifyEvent");
		} 
		
		return newEvent;
	}
	
	
	/**
	 * Changes the initiator of a meeting and sets the new initiator to have accepted the meeting.
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws EventNotFoundException
	 */
	public static Event updateInitiator(UpdateInitiatorReq request) throws SQLException, EventNotFoundException {
		int meetingID = request.meetingID;
		String newInitiator = request.newInitiator;
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("UPDATE accepted_table SET has_accepted = 1 WHERE username = '" + newInitiator + "';");
		stmt.executeUpdate("UPDATE event SET initiator = '" + newInitiator + "' where ID = " + meetingID + ";");
		return getEvent(meetingID);
	}
	
	
	/**
	 * Creates a new event in the database
	 * @param newEvent
	 * @return
	 * @throws IOException
	 * @throws SQLException 
	 */
	public static Event addEvent(CreateEventReq newEvent) throws SQLException {
		StringBuilder sqlUpdate = new StringBuilder();
		int ID = getNextID("event");
		sqlUpdate.append("insert into event values (");
		sqlUpdate.append(Integer.toString(ID) + ",");
		sqlUpdate.append("0,");
		sqlUpdate.append("'" + fixText(newEvent.event.getName()) + "',");
		sqlUpdate.append("'" + fixText(newEvent.event.getLocation()) + "',");
		sqlUpdate.append("'" + fixText(newEvent.event.getDescription()) + "',");
		sqlUpdate.append(Integer.toString(newEvent.event.getDuration()) + ",");
		sqlUpdate.append(Long.toString(newEvent.event.getStartTime().getTimeInMillis()) + ",'");
		sqlUpdate.append(fixText(newEvent.event.getInitiator()) + "');");
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate(sqlUpdate.toString());
		newEvent.event.setID(ID);
		return newEvent.event;
	}
	
	
	/**
	 * Deletes an event from the database.
	 * @param eventID		The ID of the event to be deleted
	 * @throws SQLException 
	 */
	public static void deleteEvent(int eventID) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from event where ID = " + Integer.toString(eventID) + ";");
		stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from accepted_table where eventID = " + Integer.toString(eventID) + ";");
		stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from comment where eventID = " + Integer.toString(eventID) + ";");
	}
	
	
	/**
	 * Gets the username, firstname, and password of every user in the database
	 * @return			An ArrayList of String[] containing the user information
	 * @throws SQLException
	 */
	public static ArrayList<String[]> getUserList() throws SQLException {
		ArrayList<String[]> users = new ArrayList<>();
		Statement stmt = sqlDatabase.createStatement();
		ResultSet result = stmt.executeQuery("SELECT username,firstname,lastname FROM user");
		String[] user;
		while (result.next()) {
			user = new String[3];
			user[0] = result.getString("username");
			user[1] = result.getString("firstname");
			user[2] = result.getString("lastname");
			users.add(user);
		}
		return users;
	}
	
	//-----------------------------------------------------------------------------------------------------
	
	// USER TABLE
	//	username | password | firstname | bio | position | room | email | phone
	//-----------------------------------------------------------------------------------------------------
	
	/**
	 * Takes a user class and updates the users information on the database.
	 * @param request
	 * @return			The updated user class
	 * @throws SQLException 
	 * @throws UserNotFoundException 
	 */
	public static User updateUser(UpdateUserReq request) throws SQLException, UserNotFoundException {
		String passwordString = new String(request.password);
		if (passwordString.equals("")) { //if they left tht password field blank
				Statement pstmt = sqlDatabase.createStatement();
				ResultSet result = pstmt.executeQuery("SELECT password FROM user WHERE username = '" + request.user.getUsername() + "';");
				passwordString = result.getString("password");
		}
		User user = request.user;
		Statement stmt = sqlDatabase.createStatement();
		StringBuilder sqlCommand = new StringBuilder();
		sqlCommand.append("UPDATE user SET ");
		sqlCommand.append("firstname = '" + fixText(user.getFirstname()) + "',");
		sqlCommand.append("password = '" + fixText(passwordString) + "',");
		sqlCommand.append("lastname = '" + fixText(user.getLastName()) + "',");
		sqlCommand.append("bio = '" + fixText(user.getBio()) + "',");
		sqlCommand.append("position = '" + fixText(user.getPosition()) + "',");
		sqlCommand.append("room = '" + fixText(user.getRoom()) + "',");
		sqlCommand.append("email = '" + fixText(user.getEmail()) + "',");
		sqlCommand.append("phone = '" + fixText(user.getPhone()) + "'");
		sqlCommand.append("WHERE username = '" + fixText(user.getUsername()) +"';");
		stmt.executeUpdate(sqlCommand.toString());
		return getUser(user.getUsername());
	}
	
	
	/**
	 * Checks the credential of the user logging in. If they are correct then a User class is 
	 * sent back, if not the a null user class is sent.
	 * @param request
	 * @return
	 * @throws UserNotFoundException 
	 */
	public static User login(LoginReq request) throws UserNotFoundException {
		try {
			Statement stmt = sqlDatabase.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM user WHERE username = '" + fixText(request.username) + "';");
			result.next();
			if (result.getString("password").equals(request.password)) {
				return getUser(request.username);
			}
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @login");
		}
		throw new UserNotFoundException();
	}
	
	
	/**
	 * Creates a new entry in the database for the user. 
	 * @param newUser		The CreateUserReq that contains the username and password of the new user.
	 */
	public static User addUser(CreateUserReq newUser) throws InvalidUsernameException {
		User user = newUser.user;
		try {
			StringBuilder sqlUpdate = new StringBuilder();
			sqlUpdate.append("insert into user values ('"+ fixText(user.getUsername()) + "','" + fixText(new String(newUser.password)) + "',");
			sqlUpdate.append("'" + fixText(user.getFirstname()) + "','" + fixText(user.getLastName()) + "','");
			sqlUpdate.append(fixText(user.getBio()) + "','" + fixText(user.getPosition()) + "','" + fixText(user.getRoom()) + "','");
			sqlUpdate.append(fixText(user.getEmail()) + "','" + fixText(user.getPhone()) + "');");
			Statement stmt = sqlDatabase.createStatement();
			stmt.executeUpdate(sqlUpdate.toString());
			return getUser(user.getUsername());
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @addUser");
			throw new InvalidUsernameException();
		} catch (UserNotFoundException e) {
			System.err.println("Could not find user after being created");
		}
		return null; //user failed to be created.
	}

	
	/**
	 * Takes a users username and finds all events they are in and build their schedule
	 * @param IDs
	 * @param bools
	 * @return
	 */
	public static Schedule buildSchedule(String username) {
		ArrayList<Event> events = new ArrayList<>();
		ArrayList<Boolean> acceptance = new ArrayList<>();
		try {
			Statement stmt = sqlDatabase.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM accepted_table WHERE username = '" + fixText(username) + "';");
			while (result.next()) {
				try {
					events.add(getEvent(result.getInt("eventID")));
				} catch (EventNotFoundException e) {
					System.err.println("could not find event " + Integer.toString(result.getInt("eventID")));
				}
				acceptance.add(result.getBoolean("has_accepted"));
			}
			return new Schedule(events, acceptance);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		}
		
		Schedule newSchedule = new Schedule(events, acceptance);
		return newSchedule;
	}
	
	
	/**
	 * Gets information about a user from the database.
	 * 
	 * @param username	The uniqe username of the user.
	 * @return 			A User class containing the user associated with the username.
	 */
	public static User getUser(String username) throws UserNotFoundException {
		try {
			Statement stmt = sqlDatabase.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM user WHERE username = '" + fixText(username) + "';");
			//now we build the user class
			String firstname = result.getString("firstname");
			String lastname = result.getString("lastname");
			String bio = result.getString("bio");
			String position = result.getString("position");
			String room = result.getString("room");
			String email = result.getString("email");
			String phone = result.getString("phone");
			Schedule schedule = buildSchedule(username);
			return new User(username, firstname, lastname, bio, position, room, email, phone, schedule);
			
		} catch (SQLException e) {
			if (! e.getMessage().equals("ResultSet closed")) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @getEvent");
			}
			throw new UserNotFoundException();
		}
	}

	//-----------------------------------------------------------------------------------------------------

	//COMMENTS TABLE
	//	ID | eventID | username | time | comment
	//-----------------------------------------------------------------------------------------------------
	
	/**
	 * Creates a new comment in the database
	 * @param newComment
	 * @return
	 */
	public static Comment addComment(CreateCommentReq newComment) {
		Comment commentObj = newComment.comment;
		Comment comment = null;
		try {
			Statement stmt = sqlDatabase.createStatement();
			StringBuilder update = new StringBuilder();
			int ID = getNextID("comment");
			update.append("INSERT INTO Comment values (" + Integer.toString(ID));
			update.append("," + Integer.toString(commentObj.getMeetingID()));
			update.append(",'" + fixText(commentObj.getOwner()) + "',");
			update.append(Long.toString(commentObj.getTimeStamp().getTimeInMillis()));
			update.append(",'" + fixText(commentObj.getComment()) + "');");
			stmt.executeUpdate(update.toString());
			try {
				comment = getComment(ID);
			} catch (CommentNotFoundException e) {
				System.err.println("Failed to retrieve comment after being added to database");
			}
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @addComment");
		}
		
		return comment;
		
	}
	
	
	/**
	 * Gets the comment that has the corresponding ID from the database.
	 * @param ID
	 * @return
	 * @throws CommentNotFoundException
	 */
	public static Comment getComment(int ID) throws CommentNotFoundException {
		try {
			Statement stmt = sqlDatabase.createStatement();
			ResultSet result = stmt.executeQuery("SELECT * FROM comment WHERE ID = " + Integer.toString(ID) + ";");
			int ID2 = result.getInt("ID");
			int meetingID = result.getInt("eventID");
		    String owner = result.getString("username");
		    Calendar timeStamp = Calendar.getInstance();
		    timeStamp.setTimeInMillis(result.getLong("time"));
		    String comment = result.getString("comment");
		    
		    return new Comment(ID2, meetingID, owner, timeStamp, comment);
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage() + " @getComment");
			throw new CommentNotFoundException();
		}
	}
	
	
	/**
	 * Deletes a comment from the database.
	 * @param ID				The ID number of the comment to be deleted
	 * @throws SQLException
	 */
	public static void deleteComment(int ID) throws SQLException {
		Statement stmt = sqlDatabase.createStatement();
		stmt.executeUpdate("DELETE from comment where ID = " + Integer.toString(ID) + ";");
	}
	
}
