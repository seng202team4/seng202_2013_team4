package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

import requests.*;
import shared.User;
import updates.Update;

/**
 * 	Each time a client connects to the server a new service thread is 
 * 	started. Handles requests from the user to the Request handler.
 * 	Does not have direct access to the database.
 * @author Daniel Hope
 */
public class ServiceThread extends Thread implements Runnable {

	private RequestHandler requestHandler;
	private Socket socket;
	private InputStream inputStream;
	private OutputStream outputStream;
	private ObjectOutputStream objectOutputStream;
	private ObjectInputStream objectInputStream;
	private String connectedUser = "unknown";
	private Boolean isRunning = true;
	public LinkedBlockingQueue<Object> input = new LinkedBlockingQueue<Object>(); 
								//Used to pass objects back from
								// RequestHandler;

	
	/**
	 * Constructs service object that processes commands from a socket for a
	 * data item.
	 */
	public ServiceThread(Socket aSocket, RequestHandler requestHandler) {
		socket = aSocket;
		this.requestHandler = requestHandler;
	}

	
	/**
	 * Initialises IO streams and starts the main loop ServiceThread
	 */
	public void run() {
		try {
			outputStream = socket.getOutputStream();
			objectOutputStream = new ObjectOutputStream(outputStream);
			inputStream = socket.getInputStream();
			objectInputStream = new ObjectInputStream(inputStream);
			performService();
		} catch (IOException e) {
			requestHandler.addRequest(new DatabaseRequest(new LogoutReq(connectedUser), this));
		}
	}

	
	/**
	 * Waits for and returns the next object added to the input queue by the request handler.
	 * @return	The next object added.
	 */
	public Object getInput() {
		try {
			return input.take();
		} catch (InterruptedException e) {
			System.err.println("WARNING: Thread was interrupted, returning null. username: " + connectedUser);
			return null;
		}
	}
	
	
	/**
	 * Gets the next command off the input stream
	 * 
	 * @return A string od the next command
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private Request getRequest() throws IOException {
		Request nextRequest = null;
		while (nextRequest == null) {
			try {
				nextRequest = (Request) objectInputStream.readObject();
				System.out.println("got next request: " + nextRequest.command);
			} catch (ClassNotFoundException e) {
				System.err.println(">>> Failed reading Request");
				nextRequest = null;
			}
		}
		return nextRequest;
	}
	
	
	/**
	 * The main loop for the thread, runs until IOException is thrown because the client closed
	 * @throws IOException
	 */
	public void performService() throws IOException {

		Request nextRequest = getRequest();
		String command = nextRequest.command;
		while (isRunning) {
			switch (command) {
			case "LoginReq":
				login((LoginReq) nextRequest);
				break;
			case "LogoutReq":
				logout((LogoutReq) nextRequest);
				break;
			default :
				pushToRequestHandler(nextRequest);
				break;
				
			}
			nextRequest = getRequest();
			command = nextRequest.command;
		}
	}

	
	/**
	 * Sends an update back to the client.
	 * @param update	The updated object.
	 */
	public void sendUpdate(Update update) {
		try {
			objectOutputStream.writeObject(update);
		} catch (IOException e) {
			System.err.println("Could not send update to " + connectedUser);
		}
	}
	
	
	/**
	 * Takes the login request from the client and passes a request on to the database.
	 * @param request
	 * @throws IOException
	 */
	public void login(LoginReq request) throws IOException {
		DatabaseRequest requestlogin = new DatabaseRequest(request, this);
		requestHandler.addRequest(requestlogin);
		
		Object returnValue = getInput();
	
		if (returnValue.getClass().equals(User.class)) {
				User user = (User) returnValue;
				connectedUser = user.getUsername();
		} 
		objectOutputStream.writeObject(returnValue);
		objectOutputStream.flush();
	}

	
	/**
	 * Logs off from the server.
	 * @param request
	 */
	public void logout(LogoutReq request) {
		DatabaseRequest requestLogout = new DatabaseRequest(request, this);
		requestHandler.addRequest(requestLogout);
		connectedUser = "unknown";
		isRunning = false;
	}
	
	
	/**
	 * Sends the request to the request handler and waits for an object to be
	 * sent back.
	 * @param request
	 * @throws IOException
	 */
	private void pushToRequestHandler(Request request) throws IOException {
		DatabaseRequest requestObject = new DatabaseRequest(request, this);
		requestHandler.addRequest(requestObject);
		
		
		
		Object returnValue = getInput();
		
		if (returnValue.getClass() != RequestComplete.class) {
			objectOutputStream.writeObject(returnValue);
			objectOutputStream.flush();
		}
	}
	
	
	/**
	 * Gets the username of the user that is conneted to the thread. If the user
	 * has not logged in yet unknown is returned.
	 * @return	The username of the user.
	 */
	public String getConnectedUser() {
		return this.connectedUser;
	}
	
}
