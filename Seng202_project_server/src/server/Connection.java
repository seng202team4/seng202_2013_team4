package server;

/**
 * Used exclusively by the request handler to store a service thread and the user that
 * is connected to it.
 * @author Danielz99
 */
public class Connection {
	public String user;
	public ServiceThread source;
	
	Connection (String user, ServiceThread source) {
		this.user = user;
		this.source = source;
	}
	
}
