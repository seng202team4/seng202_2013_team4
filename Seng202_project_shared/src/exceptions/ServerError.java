package exceptions;

import java.io.Serializable;

public class ServerError implements Serializable {

	public Exception exception;
	
	private static final long serialVersionUID = 8039635251511478730L;
	
	public ServerError(Exception exception) {
		this.exception = exception;
	}
}
