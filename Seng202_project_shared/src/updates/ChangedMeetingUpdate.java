package updates;


import shared.Meeting;

public class ChangedMeetingUpdate extends Update {

    private static final long serialVersionUID = 1L;
    public Meeting meeting;

    public ChangedMeetingUpdate(Meeting meeting) {
        super(CommandTypes.MEETING_UPDATE);
        this.meeting = meeting;
    }
}
