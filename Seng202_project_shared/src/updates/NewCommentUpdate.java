package updates;


import shared.Comment;

public class NewCommentUpdate extends Update {

    private static final long serialVersionUID = 1;
    public Comment comment;

    public NewCommentUpdate(Comment comment) {
        super(CommandTypes.NEW_COMMENT);
        this.comment = comment;
    }
}
