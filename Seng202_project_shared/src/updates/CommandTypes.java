package updates;

public enum CommandTypes {
    MEETING_UPDATE, 
    NEW_COMMENT, 
    NEW_MEETING, 
    REMOVE_MEETING;
}
