package updates;


import shared.Meeting;

public class NewMeetingUpdate extends Update {

    private static final long serialVersionUID = 1L;
    public Meeting meeting;

    public NewMeetingUpdate(Meeting meeting) {
        super(CommandTypes.NEW_MEETING);
        this.meeting = meeting;
    }
}
