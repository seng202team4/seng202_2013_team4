package updates;

import java.io.Serializable;

/**
 * The superclass for all client updates. Used when the server changes data that currently
 * logged in users have on their client sides. It contains a string that stores what type of
 * update the subclass is so it can be cast
 */
public class Update implements Serializable {

    private static final long serialVersionUID = 1L;
    public CommandTypes command;

    public Update (CommandTypes command) {
        this.command = command;
    }
}
