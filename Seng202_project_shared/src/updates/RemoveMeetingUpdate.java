package updates;

public class RemoveMeetingUpdate extends Update {

	public int meetingID;
	private static final long serialVersionUID = 7815393193962141779L;

	public RemoveMeetingUpdate(int meetingID) {
		super(CommandTypes.REMOVE_MEETING);
		this.meetingID = meetingID;
	}
}
