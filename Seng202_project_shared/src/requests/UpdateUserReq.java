package requests;

import shared.User;

/**
 * Used when a user wants to update their profile on the database.
 * @author Danielz99
 */
public class UpdateUserReq extends Request {

	private static final long serialVersionUID = 5308669732384475802L;
	public User user;
    public char[] password;
	
	public UpdateUserReq(User user, char[] password) {
		super("UpdateUserReq");
		this.user = user;
        this.password = password;
	}
}
