package requests;

public class RemoveAttendeeReq extends Request {
	
	public int meetingID;
	public String username;
	
	private static final long serialVersionUID = 5880512125376819332L;

	public RemoveAttendeeReq(int meetingID, String username) {
		super("RemoveAttendeeReq");
		this.meetingID = meetingID;
		this.username = username;
	}
}
