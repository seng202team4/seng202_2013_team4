package requests;

/**
 *	Used when requesting a user from the database.
 */
public class GetUserReq extends Request {

	private static final long serialVersionUID = 1L;
	public String username;
	
	public GetUserReq(String username) {
		super("GetUserReq");
		this.username = username;
	}
}
