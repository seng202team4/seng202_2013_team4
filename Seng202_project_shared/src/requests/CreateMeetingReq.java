package requests;

import shared.Meeting;

/**
 * Used when requesting the creation of a meeting on the database
 */
public class CreateMeetingReq extends Request {
	
	private static final long serialVersionUID = 1L;
	public Meeting meeting;

    public CreateMeetingReq(Meeting meeting) {
    	super("CreateMeetingReq");
        this.meeting = meeting;
    }
}
