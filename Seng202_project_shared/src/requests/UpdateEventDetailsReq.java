package requests;


/**
 * Used when requesting that the information stored on the database be changed
 */
public class UpdateEventDetailsReq extends Request {

	private static final long serialVersionUID = 1L;
	public int id;
    public String name;
    public String location;
    public String description;

	public UpdateEventDetailsReq(int id, String name, String location, String description) {
		super("UpdateMeetingReq");
		this.id = id;
        this.name = name;
        this.location = location;
        this.description = description;
	}
}
