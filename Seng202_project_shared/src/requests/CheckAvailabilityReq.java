package requests;

import java.util.ArrayList;
import java.util.Calendar;

public class CheckAvailabilityReq extends Request {

	
	public ArrayList<String> users;
	public Calendar startTime;
	public int duration;
	public int meetingID;
	
	private static final long serialVersionUID = 5102755113473514169L;

	public CheckAvailabilityReq(ArrayList<String> users, Calendar startTime, int duration, int meetingID) {
		super("CheckAvailabilityReq");
		this.users = users;
		this.startTime = startTime;
		this.duration = duration;
		this.meetingID = meetingID;
	}
	
}
