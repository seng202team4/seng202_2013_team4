package requests;

import shared.Meeting;

/**
 * Used when requesting that the information stored on the database be changed
 */
public class UpdateMeetingReq extends Request {

	private static final long serialVersionUID = 1L;
	public Meeting updatedMeeting;
	
	public UpdateMeetingReq(Meeting updatedMeeting) {
		super("UpdateMeetingReq");
		this.updatedMeeting = updatedMeeting;
	}
}
