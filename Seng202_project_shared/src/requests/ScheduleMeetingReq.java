package requests;


import java.util.ArrayList;
import java.util.Calendar;

/**
 * Used when requesting the meeting scheduler to give times when people are available
 */
public class ScheduleMeetingReq extends Request {

	private static final long serialVersionUID = 1L;
	public Calendar windowStart;
    public Calendar windowEnd;
    public int duration;
    public ArrayList<String> attendees;

    public ScheduleMeetingReq(Calendar windowStart, Calendar windowEnd, int duration, ArrayList<String> attendees) {
    	super("ScheduleMeetingReq");
        this.windowStart = windowStart;
        this.windowEnd = windowEnd;
        this.duration = duration;
        this.attendees = attendees;
    }
}
