package requests;

import java.io.Serializable;

/**
 *	The superclass for all client Requests. Used whenever a client requests 
 *	data from the server. Contians a string that stores what type of request 
 *	the subclass is so it can be cast
 */
public class Request implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public String command;
	
	public Request (String command) {
		this.command = command;
	}
}