package requests;

public class AddAttendeeReq extends Request {


	public int meetingID;
	public String username;
	private static final long serialVersionUID = 1893302059359047469L;

	public AddAttendeeReq(int meetingID, String username) {
		super("AddAttendeeReq");
		this.meetingID = meetingID;
		this.username = username;
	}

}
