package requests;

public class UpdateInitiatorReq extends Request{

	private static final long serialVersionUID = -3747827812764034200L;
	public int meetingID;
    public String oldInitiator;
	public String newInitiator;
	
	
	public UpdateInitiatorReq(int meetingID, String oldInitiator, String newInitiator) {
		super("UpdateInitiatorReq");
		this.meetingID = meetingID;
        this.oldInitiator = oldInitiator;
		this.newInitiator = newInitiator;
	}
}
