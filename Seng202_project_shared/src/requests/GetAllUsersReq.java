package requests;

public class GetAllUsersReq extends Request {

	private static final long serialVersionUID = 5877909876077671120L;

	public GetAllUsersReq() {
		super("GetAllUsersReq");
	}
}
