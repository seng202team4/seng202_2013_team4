package requests;

/**
 * Used when logging out of the server to keep track of who is connected to update.
 */
public class LogoutReq extends Request {

    private static final long serialVersionUID = 1L;
    public String username;

    public LogoutReq(String username) {
        super("LogoutReq");
        this.username = username;
    }
}
