package requests;

import shared.Comment;

/**
 * Used when requesitn a new comment be created in the database
 */
public class CreateCommentReq extends Request {

	private static final long serialVersionUID = -6651314674382229369L;
	public Comment comment;
	
	public CreateCommentReq(Comment comment) {
		super("CreateCommentReq");
		this.comment = comment;
	}
}
