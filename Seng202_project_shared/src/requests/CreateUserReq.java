package requests;

import shared.User;


/**
 * Used when requesting a new user account be created on the database
 */ 
public class CreateUserReq extends Request {
	
	private static final long serialVersionUID = 1L;
	public User user;
	public char[] password;
	
	public CreateUserReq(User user, char[] password) {
		super("CreateUserReq");
		this.user = user;
		this.password = password;
	}
}
