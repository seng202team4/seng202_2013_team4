package requests;

public class EditAcceptanceReq extends Request {
	
	public int meetingID;
	public String username;
	public Boolean isAccept;
	
	private static final long serialVersionUID = 4707188091116599461L;
	
	public EditAcceptanceReq(int meetingID, String username, Boolean isAccept) {
		super("EditAcceptanceReq");
		this.meetingID = meetingID;
		this.username = username;
		this.isAccept = isAccept;
	}
}
