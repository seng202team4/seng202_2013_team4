package requests;

/**
 * Used when a user wants to delete an event or meeting they initiated.
 * @author Monyolomery Swaggerson
 */
public class DeleteEventReq extends Request {

    private static final long serialVersionUID = 5308669732384475802L;
    public int eventID;
    public boolean isMeeting;

    public DeleteEventReq(int eventID, boolean isMeeting) {
        super("DeleteEventReq");
        this.eventID = eventID;
        this.isMeeting = isMeeting;
    }
}
