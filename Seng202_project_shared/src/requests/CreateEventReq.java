package requests;

import shared.Event;

/**
 *	Used when requesting a new personal event is to be created
 */
public class CreateEventReq extends Request {

	private static final long serialVersionUID = 1L;
	public Event event;

    public CreateEventReq(Event newEvent) {
    	super("CreateEventReq");
        this.event = newEvent;
    }
}
