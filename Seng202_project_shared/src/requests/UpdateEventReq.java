package requests;

import shared.Event;


/**
 * Used when requesting the information stored about an event be changed
 */
public class UpdateEventReq extends Request {


	private static final long serialVersionUID = 1L;
    public Event updatedEvent;

	
	public UpdateEventReq(Event updatedEvent) {
		super("UpdateEventReq");
        this.updatedEvent = updatedEvent;
	}
}
