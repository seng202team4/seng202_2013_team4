package requests;


/**
 * Used when requesting to log on to the server.
 */
public class LoginReq extends Request {
	
	private static final long serialVersionUID = 1L;
	public String username;
	public String password;
	
	public LoginReq(String username, char[] password) {
		super("LoginReq");
		this.username = username;
		this.password = new String(password);
	}
}
