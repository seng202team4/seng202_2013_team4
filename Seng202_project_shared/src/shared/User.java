package shared;

import java.io.Serializable;

/**
 * Holds all of the data about a user. Also contains a single Schedule instance for the events
 * that the user is participating in.
 * @Montgomery Anderson
 */
public class User implements Serializable {
	
	private static final long serialVersionUID = -2120948301446618226L;

    // the names are all pretty self - explanatory
	private String username;
    private String firstName;
    private String lastName;
	private String bio;
	private String position;
	private String room;
	private String email;
	private String phone;
    private Schedule schedule;


	/**
	 * Constructor for the user, takes in all attributes and stores them.
	 * @param username		The unique username that the user uses to login with
	 * @param firstName		The users first name
	 * @param lastName		The users last name
	 * @param bio			The users bio. Contains information about them that they set
	 * @param position		The position of the user within the business
	 * @param room			The room that the user is allocated within the business
	 * @param email			The user's email address
	 * @param phone 		The user's phone number  
     */
	public User(String username, String firstName, String lastName, String bio, String position, String room, String email, String phone, Schedule schedule) {
		this.username = username;
		this.firstName = firstName;
        this.lastName = lastName;
		this.bio = bio;
		this.position = position;
		this.room = room;
		this.email = email;
		this.phone = phone;
		this.schedule = schedule;
	}


    /**
     * Accessor method for the username of the user.
     * @return yhe username of the user
     */
    public String getUsername() {
        return this.username;
    }


    /**
     * Mutator method for the name of the user.
     * @param firstName the new first name
     * @param lastName the new last name
     */
	public void setName(String firstName, String lastName) {
		this.firstName = firstName;
        this.lastName = lastName;
	}
	
	
    /**
     * Accessor method for the full name of the user.
     * @return the concatenation of the first and last name with a seperating space
     */
	public String getName() {
		return firstName + " " + lastName;
	}
	
	
    /**
     * Mutator method for the first name of the user.
     * @param newName the new first name of the user
     */
	public void setFirstName(String newName) {
		firstName = newName;
	}
	
    /**
     * Accessor method for the first name of the user.
     * @return the users first name
     */
	public String getFirstname() {
		return this.firstName;
	}


    /**
     * Mutator method for the last name of the user.
     * @param newName the new last name of the user
     */
    public void setLastName(String newName) {
        lastName = newName;
    }

	
    /**
     * Accessor method for the last name of the user.
     * @return the users last name
     */
	public String getLastName() {
		return this.lastName;
	}

	
	
    /**
     * Mutator method for the bio of the user.
     * @param newBio the new bio of the user.
     */
	public void setBio(String newBio) {
		this.bio = newBio;
	}
	
	
    /**
     * Accessor method for the bio of the user.
     * @return the biography / description of the user
     */
	public String getBio() {
		return this.bio;
	}
	
	
    /**
     * Mutator method for the business position of the user.
     * @param newPosition the new position of the user
     */
	public void setPosition(String newPosition) {
		this.position = newPosition;
	}
	
	
    /**
     * Accessor method for business position of the user.
     * @return the position of the user within the company
     */
	public String getPosition() {
		return this.position;
	}

	
    /**
     * Accessor method for room that the user is based in.
     * @return the room the user can be found in within the company
     */
	public String getRoom() {
		return this.room;
	}
	
	
	/**
	 * Accessor method for the email pf the user
	 * @return the users email
	 */
	public String getEmail() {
		return this.email;
	}
	
	
	/**
	 * Accessor method for the phone number of the user
	 * @return the users phone number
	 */
	public String getPhone() {
		return this.phone;
	}
	
	
    /**
     * Accessor method for user's schedule.
     * @return the user's schedule containing all the event information
     */
    public Schedule getSchedule() {
        return this.schedule;
    }


    /**
     * To string method override.
     * @return a string representation of the user
     */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [username=");
		builder.append(username);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", bio=");
		builder.append(bio);
		builder.append(", position=");
		builder.append(position);
		builder.append(", room=");
		builder.append(room);
		builder.append(", email=");
		builder.append(email);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", schedule=");
		builder.append(schedule);
		builder.append("]");
		return builder.toString();
	}


    /**
     * Overridden hash code function to create a unique hash for each user.
     * @return the hash value
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bio == null) ? 0 : bio.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((room == null) ? 0 : room.hashCode());
		result = prime * result
				+ ((schedule == null) ? 0 : schedule.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}


    /**
     * Overridden equality method to check for field by field equality.
     * @param obj the event it is comparing with
     * @return true if they are 'equivalent' false otherwise
     */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (bio == null) {
			if (other.bio != null)
				return false;
		} else if (!bio.equals(other.bio))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (room == null) {
			if (other.room != null)
				return false;
		} else if (!room.equals(other.room))
			return false;
		if (schedule == null) {
			if (other.schedule != null)
				return false;
		} else if (!schedule.equals(other.schedule))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}
    
}
