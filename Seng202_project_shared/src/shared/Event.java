package shared;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *  The event object, stores all the details of a specific event. An event is a private meeting, where the only
 *  attendee is the initiator, it is designed to allow users to set times at which they are unavailable for other
 *  meetings.
 *  @author Montgomery Anderson
 */
public class Event implements Serializable {
	
	private static final long serialVersionUID = 1683693859823165375L;
	private int ID;
    private String name;
    private String location;        // or .. private Location location; alternative approach (will change to)
    private String description;
    private int duration;
    private Calendar startTime;
    private Calendar endTime;
    protected String initiator;
    private Boolean isMeeting;


    /**
     * Complete constructor taking in all possible information about the event, creating an instance of it.
     * @param ID the unique identifier for the event
     * @param isMeeting	true is the Event is a Meeting false otherwise
     * @param name the name of the event
     * @param location the location of the event
     * @param description the description of the event
     * @param duration the duration of the event
     * @param startTime the starting time of the event as a calendar object
     * @param initiator the user who initiated the meeting
     */
    public Event(int ID,
    			 Boolean isMeeting,
                 String name,
                 String location,
                 String description,
                 int duration,
                 Calendar startTime,
                 String initiator) {
        this.ID = ID;
        this.isMeeting = isMeeting;
        this.name = name;
        this.location = location;
        this.description = description;
        this.duration = duration;
        this.startTime = startTime;
        this.endTime = (Calendar) this.startTime.clone();
        this.endTime.add(Calendar.MINUTE, duration);
        this.initiator = initiator;
    }
    
    
    /**
     * Accessor method for the name of the Event.
     * @return the name of the event
     */
    public String getName() {
        return this.name;
    }


    /**
     * Setter method for the name of the event as a String.
     * @param name the name of the event.
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Setter method for the description of the event as a String.
     * @param description the description of the event.
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Setter method for the location of the event as a String.
     * @param location the location of the event.
     */
    public void setLocation(String location) {
        this.location = location;
    }


    /**
     * Accessor method for the unique event ID.
     * @return the unique event ID as an int.
     */
    public int getID() {
        return this.ID;
    }

    
    /**
     * Mutator method for the unique event ID.
     * @param ID the new ID of the meeting
     */
    public void setID(int ID) {
        this.ID = ID;
    }
    
    
    /**
     * Accessor method for the location of the Event.
     * @return the location of the event
     */
    public String getLocation() {
        return this.location;
    }


    /**
     * Accessor method for the description of the Event.
     * @return the description of the event
     */
    public String getDescription() {
        return this.description;
    }


    /**
     * Accessor method for the duration of the Event.
     * @return the duration of the event in minutes
     */
    public int getDuration() {
        return this.duration;
    }


    /**
     * Accessor method for the start time of the event.
     * @return the start time of the event
     */
    public Calendar getStartTime() {
        return this.startTime;
    }


    /**
     * Accessor method for the end time of the event.
     * @return the end time of the event
     */
    public Calendar getEndTime() {
        return this.endTime;
    }


    /**
     * Accessor method for the meeting initiator / creator of the meeting.
     * @return the meeting initiator
     */
    public String getInitiator() {
        return this.initiator;
    }
    
    
    public Boolean isMeeting() {
    	return isMeeting;
    }

    /**
     * Mutator method for the start time of the event.
     * @param startTime the new starting time of the event
     */
    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
        this.endTime = this.startTime;
        this.endTime.set(Calendar.MINUTE, this.startTime.get(Calendar.MINUTE) + duration);
    }
    
    
    /**
     * Compares this event to a time period and returns true if
     * this event intersects with the time period.
     * @param start the start of the time period
     * @param end the end of the time period
     * @return boolean, true if any part of this event overlaps
     * with the time period
     */
    public boolean intersects(Calendar start, Calendar end) {
    	return (endTime.after(start) && startTime.before(end));
    }


    /**
     * Overriding the toString method for Events, outdated, used in an older text based version of the scheduler, kept
     * in for testing purposes.
     * @return a string representation of the event
     */
    @Override
    public String toString() {
        return
            ">>>\n" +
            "EVENT       :  " + super.toString() + "\n" +
            "~~~~~\n" +
            "ID			 :  " + this.ID   + "\n" +		
            "Name        :  " + this.name + "\n" +
            "Location    :  " + this.location + "\n" +
            "Description :  " + this.description + "\n" +
            "Start time  :  " + new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss").format(
                    new Date(this.startTime.getTimeInMillis())) + "\n" +
            "End Time    :  " + new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss").format(
                    new Date(this.endTime.getTimeInMillis())) + "\n" +
            "Duration    :  " + Integer.toString(this.duration) + "\n" +
            "Initiator   :  " + initiator + "\n";
    }


    /**
     * Overridden hash code function to create a unique hash for each event.
     * @return the hash value
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + duration;
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result
				+ ((initiator == null) ? 0 : initiator.hashCode());
		result = prime * result
				+ ((isMeeting == null) ? 0 : isMeeting.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}


    /**
     * Overridden equality method to check for field by field equality.
     * @param obj the event it is comparing with
     * @return true if they are 'equivalent' false otherwise
     */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (ID != other.ID)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (duration != other.duration)
			return false;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (initiator == null) {
			if (other.initiator != null)
				return false;
		} else if (!initiator.equals(other.initiator))
			return false;
		if (isMeeting == null) {
			if (other.isMeeting != null)
				return false;
		} else if (!isMeeting.equals(other.isMeeting))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}
    
}
