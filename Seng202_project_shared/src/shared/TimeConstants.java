package shared;

public interface TimeConstants {
	public static final int MIN_PER_TIME_SLOT = 15;
	public static final int HOUR_PER_WORK_DAY = 8;
	public static final int MILLSEC_PER_SEC = 1000;
	public static final int SEC_PER_MIN = 60;
	public static final int MIN_PER_HOUR = 60;
	public static final int HOUR_PER_DAY = 24;
	public static final int DAYS_PER_WEEK = 7;
	public static final int MILLSEC_PER_TIME_SLOT = MILLSEC_PER_SEC * SEC_PER_MIN * MIN_PER_TIME_SLOT;
	public static final int MIN_PER_DAY = MIN_PER_HOUR * HOUR_PER_DAY;
	public static final int TIME_SLOTS_PER_HOUR = MIN_PER_HOUR / MIN_PER_TIME_SLOT;
	public static final int TIME_SLOTS_PER_DAY = MIN_PER_DAY / MIN_PER_TIME_SLOT;
	public static enum TimeFormat {TWELVE_HOUR, TWENTY_FOUR_HOUR};
	public static enum Format {MINUTES, DAYS, TIME_SLOTS};
}