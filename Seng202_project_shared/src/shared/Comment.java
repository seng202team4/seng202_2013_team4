package shared;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *	Is created when a user comments on a meeting. Has a unique id, the ID for the meeting it was created on,
 *	the id of the creater of the comment, and a timestamp of when the comment was created.
 */
public class Comment implements Serializable {

	private static final long serialVersionUID = 5499891580441711740L;


	private int ID;
	private int meetingID;
    private String owner;
    private Calendar timeStamp;
    private String comment;


    /**
     * Constructor method for comment, taking in the user who made the comment and the text they entered to be stored as
     * attributes of the comment.
     * @param owner         The user who made the comment.
     * @param comment       The string of text they commented.
     */
    public Comment(int ID, int meetingID, String owner,  Calendar timeStamp, String comment) {
    	this.ID = ID;
    	this.meetingID = meetingID;
        this.owner = owner;
        this.timeStamp = timeStamp;
        this.comment = comment;
    }


    @Override
    public String toString() {
        return // "    COMMENT    :  " + super.toString() + "\n" +
        		"    ID         :  " + ID + "\n" + 
                "    Owner      :  " + owner + "\n" +
                "    Timestamp  :  " + new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss").format(
                    new Date(this.timeStamp.getTimeInMillis())) + "\n" +
                comment + "\n";
    }

    
    /**
     * Accessor method for the ID of the meeting that the comment was made on.
     * @return
     */
    public int getMeetingID() {
        return meetingID;
    }
    
    /**
     * Accessor method for the unique ID of the comment.
     * @return
     */
    public int getID() {
    	return this.ID;
    }
    
    /**
     * Accessor method for the user that created the comment.
     * @return
     */
    public String getOwner() {
    	return this.owner;
    }
    
    /**
     * Accessor method for the time that the comment was made.
     * @return
     */
    public Calendar getTimeStamp() {
    	return this.timeStamp;
    }
    
    /**
     * Accessor method for the comments text.
     * @return
     */
    public String getComment() {
    	return this.comment;
    }
    
    
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ID;
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + meetingID;
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result
				+ ((timeStamp == null) ? 0 : timeStamp.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (ID != other.ID)
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (meetingID != other.meetingID)
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		return true;
	}
    
    
}
