package shared;

import exceptions.EventNotFoundException;
import exceptions.InvalidTimeslotException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A schedule class, associated with a single user and vice versa. Holds all meetings that the associated user is
 * invited to. Has a lot of schedule specific functionality related to the user specific model manipulations. Can be
 * thought of as the users calendar and all associated calendar-specific manipulations.
 * @author Montgomery Anderson
 */
public class Schedule implements Serializable {

	private static final long serialVersionUID = 3919901565022322149L;

	// an array list of all events the owning user is attending
	private ArrayList<Event> eventList = new ArrayList<>();
    /* an array list where each element is a boolean representing if the event in eventList at the same index has
     * been accepted yet or not */
    private ArrayList<Boolean> acceptanceList = new ArrayList<>();
    // the list of the event list
    private int eventListLength = 0;


    /**
     * Overridden hash code function to create a unique hash for each schedule.
     * @return the hash value
     */
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((acceptanceList == null) ? 0 : acceptanceList.hashCode());
		result = prime * result
				+ ((eventList == null) ? 0 : eventList.hashCode());
		result = prime * result + eventListLength;
		return result;
	}


    /**
     * Overridden equality method to check for field by field equality.
     * @param obj the schedule it is comparing with
     * @return true if they are 'equivalent' false otherwise
     */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Schedule other = (Schedule) obj;
		if (acceptanceList == null) {
			if (other.acceptanceList != null)
				return false;
		} else if (!acceptanceList.equals(other.acceptanceList))
			return false;
		if (eventList == null) {
			if (other.eventList != null)
				return false;
		} else if (!eventList.equals(other.eventList))
			return false;
		if (eventListLength != other.eventListLength)
			return false;
		return true;
	}


    /**
     * Constructor for the schedule, takes in the events and acceptance list as parameters, should have equal lengths.
     * @param events an array list of all events the user has in their schedule
     * @param acceptanceList an array list of booleans representing if the meeting at the equivalent index has been
     *                       accepted.
     */
	public Schedule(ArrayList<Event> events, ArrayList<Boolean> acceptanceList) {
		addListedEvents(events, acceptanceList);
    }
    
    
    /**
     * Accessor method for the event list of a specific user.
     * @return an ArrayList of Events that the user is participating in.
     */
    public ArrayList<Event> getEventList() {
        return this.eventList;
    }
    
    
    /**
     * Accessor method for the list of booleans representing whether the meeting has been accepted.
     * @return an arrayList of boolean representations whether the meeting has been accepted.
     */
    public ArrayList<Boolean> getAcceptanceList() {
        return this.acceptanceList;
    }
    
    
    /**
     * Gets the events from the eventList that the user has accepted.
     * @return an arrayList of events that have been confirmed by the user who owns this instance of schedule.
     */
    public ArrayList<Event> getAcceptedEvents() {
        ArrayList<Event> acceptedEvents = new ArrayList<>();
        for(Event event : eventList) {
            int eventIndex = eventList.indexOf(event);
            if(acceptanceList.get(eventIndex)) {
                acceptedEvents.add(event);
            }
        }
        return acceptedEvents;
    }


    /**
     * Accessor to fetch an event from the schedule with the given ID.
     * @param ID the unique ID of the event you want from the schedule
     * @return the event object with the given ID
     * @throws EventNotFoundException if the event could not be found
     */
    public Event getEvent(int ID) throws EventNotFoundException {
        for(Event event : eventList) {
            if(event.getID() == ID) {
                return event;
            }
        }
        throw new EventNotFoundException();
    }


    /**
     * Gets all the events occuring today at the day given.
     * @param startOfDay the start of the day to get all events of
     * @return an array list containing all the events occuring today
     */
	public ArrayList<Event> getDayEvents(Calendar startOfDay) {
		ArrayList<Event> dayEvents = new ArrayList<>();
		//
        Calendar endOfDay = CalendarHelper.getStartOfNextDay(startOfDay);
        
        for(Event event : eventList) {
        	if (event.intersects(startOfDay, endOfDay)) {
        		dayEvents.add(event);
        	}
        }
        return dayEvents;
    }

    
    /**
     * Gets the events from the eventList that the user hasn't accepted yet.
     * @return          An arrayList of events that have not been confirmed by
     *                  the user who owns this instance of schedule.
     */
    public ArrayList<Event> getUnacceptedEvents() {
        ArrayList<Event> acceptedEvents = new ArrayList<>();
        for(Event event : eventList) {
            int eventIndex = eventList.indexOf(event);
            if(! acceptanceList.get(eventIndex)) {
                acceptedEvents.add(event);
            }
        }
        return acceptedEvents;
    }


    /**
     * Gets an arrayList of events initiated by the user with the given username.
     * @param user the username of the user who's initiated events you want to find.
     * @return an ArrayList of events initiated by the user with username given.
     */
    public ArrayList<Event> getOwnedEvents(String user) {
        ArrayList<Event> ownedEvents = new ArrayList<>();
        for(Event event : eventList) {
            if(event.getInitiator().equals(user)) {
                ownedEvents.add(event);
            }
        }
        return ownedEvents;
    }


    /**
     * Accepts the meeting with the given unique ID as the user with the given username, in the schedule then calls the
     * meetings userAccepted() method.
     * @param meetingID the unique meeting ID
     * @param username the unique users username
     */
    public void acceptMeeting(int meetingID, String username) {
        int i = 0;
        for(Event event : eventList) {
            if(event.getID() == meetingID) {
                ((Meeting) event).userAccepted(username);
                acceptanceList.set(i, true);
                break;
            } else {
                i+= 1;
            }
        }
    }
    
    
    /**
     * Removes all trace of the given event with the unique ID passed in. If it is a meeting updates it meeting side
     * as well (not necessary).
     * @param eventID the unique ID of the event used to find it
     * @param isMeeting a boolean if it is a meeting or an event
     * @param username the username of who removed this event (not necessary, used to decouple it meeing-side)
     */
    public void removeEvent(int eventID, boolean isMeeting, String username) {
        for(int i = 0; i < eventListLength; i++) {
            Event event = eventList.get(i);
            if(event.getID() == eventID) {
                // if its a meeting, reject it meeting side
                if(isMeeting) {
                    ((Meeting)event).userRejected(username);
                }
                // then regardless of type, remove it
                eventList.remove(i);
                acceptanceList.remove(i);
                eventListLength -= 1;
                break;
            }
        }
    }


    /**
     * Adds the given event into the schedule.
     * @param newEvent the event to be added to the schedule
     * @param isAccepted whether or not the user has accepted it or not(1st call true if they initiate it, false
     *                   otherwise)
     */
    public void addEvent(Event newEvent, Boolean isAccepted) {
    	//if list is empty
    	if (eventListLength == 0) {
    		eventList.add(newEvent);
            acceptanceList.add(isAccepted);
            eventListLength += 1;
    		return;
    	}
    	
    	int index = 0;
    	 //while new event does not come before
    	while((index < eventListLength) && (!newEvent.getStartTime().after(eventList.get(index).getStartTime()))) {
    		index += 1;
    	}
    	eventList.add(index, newEvent);
    	acceptanceList.add(index, isAccepted);
    	eventListLength += 1;
    }


    /**
     * Updates the event by finding it and replacing it in the schedule.
     * @param updatedEvent the updated event
     */
    public void updateEvent(Event updatedEvent) {
        // find the event in the event list
        for(int i = 0; i < eventListLength; i++) {
            if(eventList.get(i).getID() == updatedEvent.getID()) {
                // replace the outdated event
                eventList.remove(i);
                acceptanceList.remove(i);
                eventListLength -= 1;
                addEvent(updatedEvent, true);
                break;
            }
        }
    }


    /**
     * Checks if the event with the given starting time and duration in minutes, clashes with any event in the schedule,
     * excluding itself, determined by the unique eventID passed in. Throws an invalid timeslot exception if there is a
     * clash.
     * @param eventID the unique event id to check if the clashing event is not itself
     * @param startTime the starting time of the event
     * @param duration the duration of the event in minutes
     * @throws InvalidTimeslotException if there is a clash
     */
    public void checkEventForClash(int eventID, Calendar startTime, int duration) throws InvalidTimeslotException {
        Calendar endTime = (Calendar) startTime.clone();
        endTime.add(Calendar.MINUTE, duration);
        for(Event event : eventList) {
            // if it clashes with an event that is not itself
            if(event.getID() != eventID && event.intersects(startTime, endTime)) {
                throw new InvalidTimeslotException();
            }
        }
    }


    /**
     * Finds and replaces the old meeting with the updated one.
     * @param updatedMeeting the updated meeting object
     */
    public void updateMeeting(Meeting updatedMeeting) {
        for(int i = 0; i < eventList.size(); i++) {
            Event event = eventList.get(i);
            if(event.getID() == updatedMeeting.getID()) {
                // remove out of date event
                eventList.remove(i);
                // store if it has been accepted or not
                boolean isAccepted = acceptanceList.get(i);
                acceptanceList.remove(i);
                eventListLength -= 1;
                /* re-insert event to be sorted chronologically by the addEvent call
                 * in-case of the dates being updated.*/
                addEvent(updatedMeeting, isAccepted);
            }
        }
    }


    /**
     * Finds the meeting to add the given comment to and calls the meeting to add it.
     * @param newComment the new comment
     */
    public void addComment(Comment newComment) {
        for(Event event : eventList) {
            if(event.getID() == newComment.getMeetingID()) {
                Meeting meeting = (Meeting) event;
                meeting.addComment(newComment);
                return;
            }
        }
    }


    /**
     * Adds multiple events to the eventList one by one using insertion sort.
     * @param eventList List of events to be added to the users calendars eventList.
     */
    public void addListedEvents(ArrayList<Event> eventList, ArrayList<Boolean> acceptedList) {
		int length = eventList.size();
		for (int i = 0; i < length; i++) {
			addEvent(eventList.get(i), acceptedList.get(i));
		}
    }


    /**
     * Takes an event and checks to see if it will fit into the users schedule
     * without clashing with events already in the schedule.
     * @param event		The event to be checked
     * @return			True if the event clashed, false if not.
     */
    public boolean isEventClashing(Event event) {
    	Calendar startTime = event.getStartTime();
    	Calendar endTime = event.getEndTime();
    	for (Event userEvent: eventList) {
    		if (userEvent.intersects(startTime, endTime)) {
    			return true;
    		}
    	}
    	
    	//no events in the users schedule clashed with the event passed into the function.
    	return false;
    }


    /**
     * Checks if the schedule contains a meeting that clashes with the given window.
     * @param start the start of the window
     * @param end the end of the window
     * @return true if there is an event intersecting the window, false otherwise
     */
    public boolean hasEventBetween(Calendar start, Calendar end) {
    	for (Event event: eventList) {
    		if (event.intersects(start, end)) {
    			return true;
    		}
    	}
    	return false;
    }

}
