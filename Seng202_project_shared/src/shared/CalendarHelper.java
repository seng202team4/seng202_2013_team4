package shared;

import java.util.Calendar;

public class CalendarHelper implements TimeConstants {
	public static String getDateTime(Calendar cal) {
		return getDate(cal) + ", " + getTime(cal);
	}
	
	
	public static String addLeadingZero(int number) {
		if (number < 10) {
			return "0" + Integer.toString(number);
		}
		else {
			return Integer.toString(number);
		}
	}
	
	
	public static String minutesToString(int totalMinutes, TimeFormat timeFormat) {
		totalMinutes = MIN_PER_TIME_SLOT * (totalMinutes / MIN_PER_TIME_SLOT); //Round to the nearest 5
        int hours = totalMinutes / MIN_PER_HOUR;
        int minutes = totalMinutes - hours * MIN_PER_HOUR;
        //return Integer.toString(hours) + "h " + addLeadingZero(minutes) + "m";
        if (timeFormat == TimeFormat.TWELVE_HOUR) {
        	String ampm = hours < 12 ? "am" : "pm";
        	hours = hours > 12 ? hours - 12 : hours;
        	hours = hours == 0 ? 12		 	: hours;
        	return addLeadingZero(hours) + ":" + addLeadingZero(minutes) + " " + ampm;
        }
        return addLeadingZero(hours) + "h " + addLeadingZero(minutes) + "m";
	}
	
	
	/**
	 * 
	 * @param cal
	 * @return
	 */
	public static String getTime(Calendar cal) {
		int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
		int minutes = cal.get(Calendar.MINUTE) + MIN_PER_HOUR * hourOfDay;
		return minutesToString(minutes, TimeFormat.TWELVE_HOUR);
	}
	
	
	/**
	 * 
	 * @param cal
	 * @return
	 */
	public static String getDate(Calendar cal) {
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		String date = addLeadingZero(dayOfMonth)
				+ "-" +	addLeadingZero(month)
				+ "-" + addLeadingZero(year);
		return date;
	}
	
	
	/**
	 * 
	 * @param day
	 * @return
	 */
	public static String getDayOfWeek(Calendar day) {
		switch (day.get(Calendar.DAY_OF_WEEK)){
			case (Calendar.SUNDAY) : return "Sun";
			case (Calendar.MONDAY) : return "Mon";
			case (Calendar.TUESDAY) : return "Tues";
			case (Calendar.WEDNESDAY) : return "Wed";
			case (Calendar.THURSDAY) : return "Thur";
			case (Calendar.FRIDAY) : return "Fri";
			case (Calendar.SATURDAY) : return "Sat";
			default : return null;
		}
	}
	
	
	/**
	 * Find difference between two calendar object.
	 * @param start the first of the two calendar objects.
	 * @param end the later of the two calendar objects.
	 * @param format the units of the return value.
	 * @return Difference between the two Calendar in format. 0 if start after end.
	 */
	public static int getDifference(Calendar start, Calendar end, Format format) {
		long startTimeMillis = start.getTimeInMillis();
		long endTimeMillis = end.getTimeInMillis();
		int diffMillis = (int) (endTimeMillis - startTimeMillis);//
		
		if (diffMillis <= 0) {
			return 0;
		}
		
	    int diffSeconds	= diffMillis 	/MILLSEC_PER_SEC;
	    int diffMinutes = diffSeconds	/SEC_PER_MIN;
	    int diffHours 	= diffMinutes	/MIN_PER_HOUR;
	    int diffDays 	= diffHours		/HOUR_PER_DAY;
	    int diffSlots 	= diffMinutes	/MIN_PER_TIME_SLOT;
	    
		if (format == Format.MINUTES)
			return diffMinutes;
		else if (format == Format.DAYS)
			return diffDays;
		else if (format == Format.TIME_SLOTS)
			return diffSlots;
		return 0;
	}
	
	
	/**
	 * 
	 * @param day
	 * @return
	 */
	public static Calendar getStartOfDay(Calendar day) {
        Calendar startOfDay = (Calendar) day.clone();
        startOfDay.set(Calendar.HOUR_OF_DAY, 0);
        startOfDay.set(Calendar.MINUTE, 0);
        startOfDay.set(Calendar.SECOND, 0);
        startOfDay.set(Calendar.MILLISECOND, 0);
        return startOfDay;
    }
	
	
	/**
	 * 
	 * @param day
	 * @return
	 */
	public static Calendar getStartOfNextDay(Calendar day) {
        Calendar endOfDay = getStartOfDay(day);
        endOfDay.add(Calendar.DATE, 1);
        return endOfDay;
    }
	
	
	/**
     * Takes in a Calendar object and finds the start of that objects week.
     * @return first day of the week
     */
    public static Calendar getFirstDayOfWeek(Calendar dayInWeek) {
        Calendar firstDayOfWeek = (Calendar) dayInWeek.clone();
        firstDayOfWeek.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        firstDayOfWeek = getStartOfDay(firstDayOfWeek);
        return getStartOfDay(firstDayOfWeek);
    }

    
	public static boolean isOnSameDay(Calendar day1, Calendar day2) {
		Calendar start = getStartOfDay(day1);
		Calendar end = getStartOfNextDay(day1);
		return (day2.after(start) && day2.before(end));
	}
	
	
	public static Calendar roundToNextTimeSlot(Calendar time) {
		double multiple = time.getTimeInMillis() / MILLSEC_PER_TIME_SLOT;
		System.out.println(getTime(time));
		long roundedtimeMs = (long) ((Math.ceil(multiple) + 1) * MILLSEC_PER_TIME_SLOT);
		time.setTimeInMillis(roundedtimeMs);
		System.out.println(getTime(time));
		return time;
	}
	
	
	/**
	 * Round duration up to the nearest timeslot.
	 * @param duration int length of time.
	 * @return rounded up duration.
	 */
	public static int roundUpToTimeSlot(int duration) {
		if (duration % MIN_PER_TIME_SLOT == 0) {
			return duration;
		}
		double multiple = duration / MIN_PER_TIME_SLOT;
		return (int) (Math.ceil(multiple) * MIN_PER_TIME_SLOT);
	}
}
