package shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * An extension to the class event to objectify a Meeting, extends event to have the ability to have attendees and
 * comments. Contains a multitude of methods associated with manipulating the object, closely linked with the schedule
 * of the logged in users.
 * @author Montgomery Anderson
 */
public class Meeting extends Event implements Serializable{

	private static final long serialVersionUID = 1L;

    // array list of all the attendees unique usernames
	private ArrayList<String> attendees = new ArrayList<>();
    /* array list representing which attendees have accepted, where each boolean represents the attendee at the same
     * index */
	private ArrayList<Boolean> accepted = new ArrayList<>();
    // an arrayList of comments stored in order starting at the most recent comment
    private ArrayList<Comment> comments = new ArrayList<>();


    /**
     * Meeting constructor used when meeting is initially created.
     * @param ID				The unique ID of the meeting.
     * @param name              The name of the meeting.
     * @param location          The location of the meeting.
     * @param description       The description of the meeting.
     * @param duration          The duration of the meeting in minutes.
     * @param startTime         The starting time of the meeting as a calendar object.
     * @param initiator         The meeting initiator/meeting creator.
     * @param attendees         An ArrayList of the users who are attending the meeting.
     */
    public Meeting(int ID,
    			   Boolean isMeeting,
                   String name,
                   String location,
                   String description,
                   int duration,
                   Calendar startTime,
                   String initiator,
                   ArrayList<String> attendees,
                   ArrayList<Boolean> accepted) {
         super(ID, isMeeting, name, location, description, duration, startTime, initiator);
         this.attendees = attendees;
         this.accepted = accepted;
    }


    /**
     * Meeting constructor when the meeting is read from the database, the main difference between this constructor
     * and the above one, is that this constructor takes in a list of comments also.
     * @param name              The name of the meeting.
     * @param location          The location of the meeting.
     * @param description       The description of the meeting.
     * @param duration          The duration of the meeting in minutes.
     * @param startTime         The starting time of the meeting as a calendar object.
     * @param initiator         The meeting initiator/meeting creator.
     * @param attendees         An ArrayList of the users who are attending the meeting.
     * @param comments          An ArrayList of the comments on the meeting.
     */
    public Meeting(int ID,
    			   Boolean isMeeting,
    			   String name,
                   String location,
                   String description,
                   int duration,
                   Calendar startTime,
                   String initiator,
                   ArrayList<String> attendees,
                   ArrayList<Boolean> accepted,
                   ArrayList<Comment> comments) {
        super(ID, isMeeting, name, location, description, duration, startTime, initiator);
        this.attendees = attendees;
        this.accepted = accepted;
        this.comments = comments;
    }


    /**
     * Accessor method for the accepted array
     * @return the accepted array of booleans
     */
    public ArrayList<Boolean> getAccepted() {
        return this.accepted;
    }


    /**
     * Sets the user with the given unique username to accepted in the accepted array. If it can't find the user it
     * does nothing, this only happens due to transmissions during propagation delay in networking, in which case it
     * will be fixed at the next update so doing nothing resolves the problem.
     * @param username the unique username of the user who accepted
     */
    public void userAccepted(String username) {
        int i = attendees.indexOf(username);
        if(i != -1) {
            accepted.set(i, true);
        }
    }


    /**
     * Removes the user from attendees and accepted, essentially rejecting the meeting on the meeting side, decoupling
     * the objects.
     * @param username the unique username of the user who rejected the meeting
     */
    public void userRejected(String username) {
        int i = attendees.indexOf(username);
        if (i != -1) {
        	accepted.remove(i);
        	attendees.remove(i);
        }
    }

    
    /**
     * Method for adding multiple meeting attendees to a meeting. Iterates through the users and adds them to the
     * attendees and accepted array.
     * @param invitedAttendees an ArrayList of Users to be added as attendees
     */
    public void addAttendees(ArrayList<User> invitedAttendees) {
        for (User invitee : invitedAttendees ) {
            this.attendees.add(invitee.getUsername());
            this.accepted.add(false);
            invitee.getSchedule().addEvent(this, false);
        }
    }


    /**
     * Performs a simple insertion sort when adding comments to ensure they are in the ordering where
     * the first element of the comments array (index 0) is the most recent comment. Should generally add the
     * comment to the front assuming the space time continuum is being reasonable.
     * @param comment the new comment to add.\
     */
    public void addComment(Comment comment) {
        if(comments.isEmpty()) {
            comments.add(comment);
        } else {
            Boolean added = false;
            for(int i = 0; i < comments.size(); i++) {
                if(comments.get(i).getTimeStamp().before(comment.getTimeStamp())) {
                    comments.add(i, comment);
                    added = true;
                    break;
                }
            }
            if(!added) {
                comments.add(comment);
            }
        }
    }


    /**
     * Method for removing multiple meeting attendees from a meeting.
     * @param unwantedAttendees an ArrayList of Users to be removed from the meeting
     */
    public void removeAttendees(ArrayList<User> unwantedAttendees) {
        for (User attendee : unwantedAttendees) {
            int location = attendees.indexOf(attendee.getUsername());
            this.attendees.remove(attendee.getUsername());
            this.accepted.remove(location);
            attendee.getSchedule().getEventList().remove(this);
        }
    }


    /**
     * Method for changing the meeting initiator. Sets given user to the meeting initiator and moves the meeting
     * initiator to the ArrayList of attendees.
     * @param newInitiator the new meeting initiator.
     */
    public void changeInitiator(String newInitiator) {
        // store the old initiator of the meeting
        String oldInitiator = super.initiator;
        // set the new initiator
        super.initiator = newInitiator;
        // get the index of the old initiator
        int newInitiatorIndex = attendees.indexOf(newInitiator);
        // remove the new initiator from attendees and accepted
        this.attendees.remove(newInitiatorIndex);
        this.accepted.remove(newInitiatorIndex);
        // add the old initiator to the attendees and accepted
        this.attendees.add(oldInitiator);
        this.accepted.add(true);

    }


    /**
     * Accessor method for the attendees of the meeting.
     * @return the attendees as an arrayList of their unique usernames.
     */
    public ArrayList<String> getAttendees() {
        return this.attendees;
    }


    /**
     * Accessor method for the comments on the meeting.
     * @return the comments on the meeting as an arrayList of comments.
     */
    public ArrayList<Comment> getComments() {
        return this.comments;
    }


    /**
     * Override of the toString() method for the meeting.
     * @return a  string representation of the meeting.
     */
    @Override
    public String toString() {

        // Creating a sting 'list' of attendees.
        String attendees = "";
        if( ! this.attendees.isEmpty() ) {
            for (String attendee : this.attendees) {
                attendees += "    " + attendee + "\n";
            }
        } else {
            attendees = " ~Empty~ ";
        }

        // Creating a string 'list' of comments.
        String comments = "";
        if( ! this.comments.isEmpty() ) {
            for (Comment comment : this.comments) {
                comments += comment.toString();
            }
        } else {
            comments = " ~Empty~ ";
        }

        return super.toString() +
        "Attendees   :  \n" + attendees +
        "Comments    :  \n" + comments;
    }
}
